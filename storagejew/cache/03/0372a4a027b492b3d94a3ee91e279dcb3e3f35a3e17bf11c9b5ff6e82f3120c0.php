<?php

/* so-claue/template/extension/module/so_listing_tabs/default.twig */
class __TwigTemplate_9e08d9192fb5e8d2a05081d8420a6112bb9aff1add1028411cde61e1a3afeab2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"module ";
        echo (isset($context["direction_class"]) ? $context["direction_class"] : null);
        echo " ";
        echo (isset($context["class_suffix"]) ? $context["class_suffix"] : null);
        echo "\">
\t";
        // line 2
        if ((isset($context["disp_title_module"]) ? $context["disp_title_module"] : null)) {
            // line 3
            echo "\t<h3 class=\"modtitle\">
\t\t
\t\t";
            // line 5
            if ((isset($context["disp_title_module"]) ? $context["disp_title_module"] : null)) {
                echo "<span>";
                echo (isset($context["head_name"]) ? $context["head_name"] : null);
                echo "</span>";
            }
            // line 6
            echo "\t</h3>
\t";
        }
        // line 8
        echo "\t";
        if (((isset($context["pre_text"]) ? $context["pre_text"] : null) != "")) {
            // line 9
            echo "\t\t<div class=\"form-group\">
\t\t\t";
            // line 10
            echo (isset($context["pre_text"]) ? $context["pre_text"] : null);
            echo "
\t\t</div>
\t";
        }
        // line 13
        echo "\t<div class=\"modcontent\">
\t\t<!--[if lt IE 9]>
\t\t<div id=\"";
        // line 15
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "\" class=\"so-listing-tabs msie lt-ie9 first-load module\"><![endif]-->
\t\t<!--[if IE 9]>
\t\t<div id=\"";
        // line 17
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "\" class=\"so-listing-tabs msie first-load module\"><![endif]-->
\t\t<!--[if gt IE 9]><!-->
\t\t<div id=\"";
        // line 19
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "\" class=\"so-listing-tabs first-load module\"><!--<![endif]-->
\t\t\t";
        // line 20
        if (twig_length_filter($this->env, (isset($context["list"]) ? $context["list"] : null))) {
            // line 21
            echo "\t\t\t\t<div class=\"ltabs-wrap \">
\t\t\t\t\t<div class=\"ltabs-tabs-container\" data-delay=\"";
            // line 22
            echo (isset($context["delay"]) ? $context["delay"] : null);
            echo "\"
\t\t\t\t\t \tdata-duration=\"";
            // line 23
            echo (isset($context["duration"]) ? $context["duration"] : null);
            echo "\"
\t\t\t\t\t \tdata-effect=\"";
            // line 24
            echo (isset($context["effect"]) ? $context["effect"] : null);
            echo "\"
\t\t\t\t\t \tdata-ajaxurl=\"";
            // line 25
            echo (isset($context["ajaxurl"]) ? $context["ajaxurl"] : null);
            echo "\" data-type_source=\"";
            echo (isset($context["type_source"]) ? $context["type_source"] : null);
            echo "\"
\t\t\t\t\t \tdata-type_show=\"";
            // line 26
            echo (isset($context["type_show"]) ? $context["type_show"] : null);
            echo "\" >
\t\t\t\t\t\t 
\t\t\t\t\t\t";
            // line 28
            echo twig_include($this->env, $context, ((isset($context["theme_config"]) ? $context["theme_config"] : null) . "/template/extension/module/so_listing_tabs/default/default_tabs.twig"));
            echo "
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"wap-listing-tabs products-list grid\">
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 33
            if (((isset($context["display_banner_image"]) ? $context["display_banner_image"] : null) == 1)) {
                // line 34
                echo "\t\t\t\t\t\t\t<div class=\"item-cat-image\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 35
                echo (isset($context["banner_image_url"]) ? $context["banner_image_url"] : null);
                echo "\" title=\"\" target=\"";
                echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                echo "\" >
\t\t\t\t\t\t\t\t\t<img class=\"categories-loadimage\" title=\"\" alt=\"\" src=\"";
                // line 36
                echo (isset($context["banner_image"]) ? $context["banner_image"] : null);
                echo "\"/>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 40
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"ltabs-items-container\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["items"]) {
                // line 44
                echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                // line 46
                $context["child_items"] = (($this->getAttribute($context["items"], "child", array(), "any", true, true)) ? ($this->getAttribute($context["items"], "child", array())) : (""));
                // line 47
                echo "\t\t\t\t\t\t\t\t";
                $context["cls"] = ((($this->getAttribute($context["items"], "sel", array(), "any", true, true) && ($this->getAttribute($context["items"], "sel", array()) == "sel"))) ? (" ltabs-items-selected ltabs-items-loaded") : (""));
                // line 48
                echo "\t\t\t\t\t\t\t\t";
                $context["cls"] = ((isset($context["cls"]) ? $context["cls"] : null) . ((($this->getAttribute($context["items"], "category_id", array()) == "*")) ? (" items-category-all") : ((" items-category-" . $this->getAttribute($context["items"], "category_id", array())))));
                // line 49
                echo "\t\t\t\t\t\t\t\t";
                $context["tab_id"] = (($this->getAttribute($this->getAttribute((isset($context["list"]) ? $context["list"] : null), $context["key"], array(), "array", false, true), "sel", array(), "array", true, true)) ? ($this->getAttribute($context["items"], "category_id", array())) : (""));
                // line 50
                echo "\t\t\t\t\t\t\t\t";
                $context["tab_id"] = ((((isset($context["tab_id"]) ? $context["tab_id"] : null) == "*")) ? ("all") : ((isset($context["tab_id"]) ? $context["tab_id"] : null)));
                // line 51
                echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"products-list ltabs-items ";
                // line 52
                echo (isset($context["cls"]) ? $context["cls"] : null);
                echo " \" data-total=\"";
                echo $this->getAttribute($context["items"], "count", array());
                echo "\" >
\t\t\t\t\t\t\t\t\t";
                // line 56
                echo "\t\t\t\t\t\t\t\t\t";
                if ((isset($context["child_items"]) ? $context["child_items"] : null)) {
                    // line 57
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    echo twig_include($this->env, $context, ((isset($context["theme_config"]) ? $context["theme_config"] : null) . "/template/extension/module/so_listing_tabs/default/default_items.twig"));
                    echo "
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 59
                    echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ltabs-loading\"></div>
\t\t\t\t\t\t\t\t\t";
                }
                // line 61
                echo "\t\t\t\t\t\t\t\t\t";
                $context["classloaded"] = (((((isset($context["source_limit"]) ? $context["source_limit"] : null) >= $this->getAttribute($context["items"], "count", array())) || ((isset($context["source_limit"]) ? $context["source_limit"] : null) == 0))) ? ("loaded") : (""));
                // line 62
                echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                // line 63
                if (((isset($context["type_show"]) ? $context["type_show"] : null) == "loadmore")) {
                    // line 64
                    echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ltabs-loadmore\"
\t\t\t\t\t\t\t\t\t\t\t data-active-content=\".items-category-";
                    // line 65
                    echo ((($this->getAttribute($context["items"], "category_id", array()) == "*")) ? ("all") : ($this->getAttribute($context["items"], "category_id", array())));
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t data-categoryid\t=\"";
                    // line 66
                    echo $this->getAttribute($context["items"], "category_id", array());
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t data-rl_start\t\t=\"";
                    // line 67
                    echo (isset($context["source_limit"]) ? $context["source_limit"] : null);
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t data-rl_total\t\t=\"";
                    // line 68
                    echo $this->getAttribute($context["items"], "count", array());
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t data-rl_allready\t=\"All ready\"
\t\t\t\t\t\t\t\t\t\t\t data-ajaxurl\t\t=\"";
                    // line 70
                    echo (isset($context["ajaxurl"]) ? $context["ajaxurl"] : null);
                    echo "\" 
\t\t\t\t\t\t\t\t\t\t\t data-rl_load\t\t=\"";
                    // line 71
                    echo (isset($context["source_limit"]) ? $context["source_limit"] : null);
                    echo "\" 
\t\t\t\t\t\t\t\t\t\t\t data-moduleid\t\t='";
                    // line 72
                    echo (isset($context["moduleid"]) ? $context["moduleid"] : null);
                    echo "'
\t\t\t\t\t\t\t\t\t\t\t >
\t\t\t\t\t\t\t\t\t\t\t<div class=\"ltabs-loadmore-btn ";
                    // line 74
                    echo (isset($context["classloaded"]) ? $context["classloaded"] : null);
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t data-label=\"";
                    // line 75
                    echo (((isset($context["classloaded"]) ? $context["classloaded"] : null)) ? ($this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "all_ready"), "method")) : ($this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "load_more"), "method")));
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ltabs-image-loading\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                }
                // line 81
                echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['items'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
            // line 86
            echo twig_include($this->env, $context, ((isset($context["theme_config"]) ? $context["theme_config"] : null) . "/template/extension/module/so_listing_tabs/default/default_js.twig"));
            echo "
\t\t\t";
        } else {
            // line 88
            echo "\t\t\t\t";
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_noproduct"), "method");
            echo "
\t\t\t";
        }
        // line 90
        echo "\t\t</div>
\t</div> <!-- /.modcontent-->
\t
\t";
        // line 93
        if (((isset($context["post_text"]) ? $context["post_text"] : null) != "")) {
            // line 94
            echo "\t\t<div class=\"form-group\">
\t\t\t";
            // line 95
            echo (isset($context["post_text"]) ? $context["post_text"] : null);
            echo "
\t\t</div>
\t";
        }
        // line 98
        echo "</div>\t";
    }

    public function getTemplateName()
    {
        return "so-claue/template/extension/module/so_listing_tabs/default.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 98,  283 => 95,  280 => 94,  278 => 93,  273 => 90,  267 => 88,  262 => 86,  257 => 83,  242 => 81,  233 => 75,  229 => 74,  224 => 72,  220 => 71,  216 => 70,  211 => 68,  207 => 67,  203 => 66,  199 => 65,  196 => 64,  194 => 63,  191 => 62,  188 => 61,  184 => 59,  178 => 57,  175 => 56,  169 => 52,  166 => 51,  163 => 50,  160 => 49,  157 => 48,  154 => 47,  152 => 46,  148 => 44,  131 => 43,  126 => 40,  119 => 36,  113 => 35,  110 => 34,  108 => 33,  100 => 28,  95 => 26,  89 => 25,  85 => 24,  81 => 23,  77 => 22,  74 => 21,  72 => 20,  68 => 19,  63 => 17,  58 => 15,  54 => 13,  48 => 10,  45 => 9,  42 => 8,  38 => 6,  32 => 5,  28 => 3,  26 => 2,  19 => 1,);
    }
}
/* <div class="module {{ direction_class }} {{ class_suffix }}">*/
/* 	{% if disp_title_module %}*/
/* 	<h3 class="modtitle">*/
/* 		*/
/* 		{% if disp_title_module %}<span>{{ head_name }}</span>{% endif %}*/
/* 	</h3>*/
/* 	{% endif %}*/
/* 	{% if pre_text != '' %}*/
/* 		<div class="form-group">*/
/* 			{{ pre_text }}*/
/* 		</div>*/
/* 	{% endif %}*/
/* 	<div class="modcontent">*/
/* 		<!--[if lt IE 9]>*/
/* 		<div id="{{ tag_id }}" class="so-listing-tabs msie lt-ie9 first-load module"><![endif]-->*/
/* 		<!--[if IE 9]>*/
/* 		<div id="{{ tag_id }}" class="so-listing-tabs msie first-load module"><![endif]-->*/
/* 		<!--[if gt IE 9]><!-->*/
/* 		<div id="{{ tag_id }}" class="so-listing-tabs first-load module"><!--<![endif]-->*/
/* 			{% if list|length %}*/
/* 				<div class="ltabs-wrap ">*/
/* 					<div class="ltabs-tabs-container" data-delay="{{ delay }}"*/
/* 					 	data-duration="{{ duration }}"*/
/* 					 	data-effect="{{ effect }}"*/
/* 					 	data-ajaxurl="{{ ajaxurl }}" data-type_source="{{ type_source }}"*/
/* 					 	data-type_show="{{ type_show }}" >*/
/* 						 */
/* 						{{ include (theme_config~"/template/extension/module/so_listing_tabs/default/default_tabs.twig") }}*/
/* 						*/
/* 					</div>*/
/* 					<div class="wap-listing-tabs products-list grid">*/
/* 						*/
/* 						{% if display_banner_image == 1 %}*/
/* 							<div class="item-cat-image">*/
/* 								<a href="{{ banner_image_url }}" title="" target="{{ item_link_target }}" >*/
/* 									<img class="categories-loadimage" title="" alt="" src="{{ banner_image }}"/>*/
/* 								</a>*/
/* 							</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						<div class="ltabs-items-container">*/
/* 						*/
/* 							{% for key, items in list %}*/
/* 								*/
/* 								*/
/* 								{% set child_items = items.child is defined ? items.child : '' %}*/
/* 								{% set cls = items.sel is defined and items.sel == "sel" ? ' ltabs-items-selected ltabs-items-loaded' : '' %}*/
/* 								{% set cls = cls~(items.category_id == "*" ? ' items-category-all' : ' items-category-' ~ items.category_id) %}*/
/* 								{% set tab_id = list[key]['sel'] is defined ? items.category_id : '' %}*/
/* 								{% set tab_id = tab_id == '*' ? 'all' : tab_id %}*/
/* 								*/
/* 								<div class="products-list ltabs-items {{ cls }} " data-total="{{ items.count }}" >*/
/* 									{# {% if allCategory[key] %}*/
/* 									<!-- <a class="items-category-seeall viewall" href="{{allCategory[key]}}" title="{{objlang.get('value_seeall')}}"> {{objlang.get('value_seeall')}} <i class="fa fa-caret-right" ></i></a> -->*/
/* 									{% endif %} #}*/
/* 									{% if child_items %}*/
/* 										{{ include (theme_config~"/template/extension/module/so_listing_tabs/default/default_items.twig") }}*/
/* 									{% else %}*/
/* 										<div class="ltabs-loading"></div>*/
/* 									{% endif %}*/
/* 									{% set classloaded = source_limit >= items.count or source_limit == 0 ? 'loaded' : '' %}*/
/* 									*/
/* 									{% if type_show == 'loadmore' %}*/
/* 										<div class="ltabs-loadmore"*/
/* 											 data-active-content=".items-category-{{ items.category_id == "*" ? 'all' : items.category_id }}"*/
/* 											 data-categoryid	="{{ items.category_id }}"*/
/* 											 data-rl_start		="{{ source_limit }}"*/
/* 											 data-rl_total		="{{ items.count }}"*/
/* 											 data-rl_allready	="All ready"*/
/* 											 data-ajaxurl		="{{ ajaxurl }}" */
/* 											 data-rl_load		="{{ source_limit }}" */
/* 											 data-moduleid		='{{ moduleid }}'*/
/* 											 >*/
/* 											<div class="ltabs-loadmore-btn {{ classloaded }}"*/
/* 												 data-label="{{ classloaded ? objlang.get('all_ready') : objlang.get('load_more') }}">*/
/* 												<span class="ltabs-image-loading"></span>*/
/* 												<i class="fa fa-plus"></i>*/
/* 											</div>*/
/* 										</div>*/
/* 									{% endif %}*/
/* 								</div>*/
/* 							{% endfor %}*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				{{ include (theme_config~"/template/extension/module/so_listing_tabs/default/default_js.twig") }}*/
/* 			{% else %}*/
/* 				{{ objlang.get('text_noproduct') }}*/
/* 			{% endif %}*/
/* 		</div>*/
/* 	</div> <!-- /.modcontent-->*/
/* 	*/
/* 	{% if post_text != '' %}*/
/* 		<div class="form-group">*/
/* 			{{ post_text }}*/
/* 		</div>*/
/* 	{% endif %}*/
/* </div>	*/
