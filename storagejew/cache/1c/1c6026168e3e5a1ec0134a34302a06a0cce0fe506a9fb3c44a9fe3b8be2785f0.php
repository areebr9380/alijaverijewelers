<?php

/* so-claue/template/header/header1.twig */
class __TwigTemplate_9fe0037d75843f1a51b345aed9465ef2f0b28dc5ae4d29403183737e5c5e983d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "
<header id=\"header\" class=\" variant typeheader-";
        // line 5
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">
\t";
        // line 6
        echo "  
\t<div class=\"header-top hidden-compact\">\t\t
\t\t<div class=\"row no-margin\">
\t\t\t<div class=\"header-top-left col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t\t\t\t
\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t";
        // line 12
        if ((isset($context["currency"]) ? $context["currency"] : null)) {
            echo "<li class=\"currency\"> ";
            echo (isset($context["currency"]) ? $context["currency"] : null);
            echo "  </li> ";
        }
        // line 13
        echo "\t\t\t\t\t";
        if ((isset($context["language"]) ? $context["language"] : null)) {
            echo " <li class=\"language\">";
            echo (isset($context["language"]) ? $context["language"] : null);
            echo " </li>\t";
        }
        echo "\t\t\t
\t\t\t\t</ul>\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"header-top-center collapsed-block col-lg-6 col-md-6 hidden-sm hidden-xs \">\t
\t\t\t\t";
        // line 19
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 20
            echo "\t\t\t\t\t<div class=\"welcome-msg\">
\t\t\t\t\t\t";
            // line 21
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 22
                echo "                            ";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method");
                echo "
                        ";
            }
            // line 23
            echo " 
\t\t\t\t\t</div>
\t\t\t\t";
        }
        // line 25
        echo "\t
\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"header-top-right col-lg-3 col-md-3 col-sm-6 hidden-xs\">
\t\t\t\t\t\t\t
\t\t\t\t<ul class=\"top-log list-inline\">
\t\t\t\t\t";
        // line 32
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 33
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
\t\t            ";
        } else {
            // line 35
            echo "\t\t\t            
\t\t\t            <li><a href=\"";
            // line 36
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a> &nbsp; ";
            echo (isset($context["text_or"]) ? $context["text_or"] : null);
            echo " </li>
\t\t\t            <li><a href=\"";
            // line 37
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
\t\t\t            
\t\t            ";
        }
        // line 40
        echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>\t\t
\t</div>
\t
\t
\t<div class=\"header-middle ";
        // line 46
        echo (isset($context["hidden_headercenter"]) ? $context["hidden_headercenter"] : null);
        echo "\">
\t\t
\t\t\t<div class=\"row no-margin\">\t\t\t
\t\t\t\t<div class=\"col-lg-2 hidden-xs hidden-sm hidden-md\">
\t\t\t\t\t";
        // line 50
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method")) {
            // line 51
            echo "\t\t\t\t\t<div class=\"telephone \" >
\t\t\t\t\t\t";
            // line 52
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 54
        echo "\t
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-8 col-md-10 col-sm-9 col-xs-12\">
\t\t\t\t\t<div class=\"main-menu-w\">
\t\t\t\t\t\t<div class=\"menu1\">\t
\t\t\t\t\t\t\t";
        // line 59
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"logo\">
\t\t\t\t\t   \t\t";
        // line 62
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo "
\t\t\t\t\t   \t</div>\t
\t\t\t\t\t   \t<div class=\"menu2\">\t
\t\t\t\t\t   \t\t";
        // line 65
        echo (isset($context["content_menu3"]) ? $context["content_menu3"] : null);
        echo "\t
\t\t\t\t\t   \t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"bottom-right col-lg-2 col-md-2 col-sm-3\">
\t\t\t\t\t<div class=\"inner\">
\t\t\t\t\t\t<div class=\"search-header-w\">
\t\t\t\t\t\t\t<div class=\"ico-search\"><i class=\"pe-7s-search\"></i></div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 73
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
\t\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"account\" id=\"my_account\"><a href=\"";
        // line 76
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\"  class=\"btn-xs dropdown-toggle\" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo " \" data-toggle=\"dropdown\"><i class=\"pe-7s-user\"></i></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu \">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 78
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\" title=\"";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
        // line 80
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 81
            echo "\t\t\t\t\t\t            <li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 82
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 83
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 84
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 85
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
\t\t\t\t\t            ";
        } else {
            // line 87
            echo "
\t\t\t\t\t\t            <li><a href=\"";
            // line 88
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 89
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t\t            ";
        }
        // line 90
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 95
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconWishlist_status"), "method")) {
            // line 96
            echo "\t\t\t\t\t\t<div class=\"wishlist hidden-md hidden-sm hidden-xs\"><a href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\" id=\"wishlist-total\" class=\"top-link-wishlist\" title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo " \"><i class=\"pe-7s-like\"></i></a></div>
\t\t\t\t\t\t";
        }
        // line 97
        echo " 
\t\t\t\t\t\t";
        // line 98
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconCompare_status"), "method")) {
            // line 99
            echo "\t\t\t\t\t\t<div class=\"compare hidden-sm hidden-xs\"><a href=\"";
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\"  class=\"top-link-compare\" title=\"";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo " \"><i class=\"pe-7s-graph3\"></i></a></div> 
\t\t\t\t\t\t";
        }
        // line 101
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
\t\t\t\t\t\t \t";
        // line 103
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t
\t</div>
\t\t
</header>";
    }

    public function getTemplateName()
    {
        return "so-claue/template/header/header1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 103,  272 => 101,  264 => 99,  262 => 98,  259 => 97,  251 => 96,  249 => 95,  242 => 90,  235 => 89,  229 => 88,  226 => 87,  219 => 85,  213 => 84,  207 => 83,  201 => 82,  194 => 81,  192 => 80,  183 => 78,  176 => 76,  170 => 73,  159 => 65,  153 => 62,  147 => 59,  140 => 54,  134 => 52,  131 => 51,  129 => 50,  122 => 46,  114 => 40,  106 => 37,  98 => 36,  95 => 35,  87 => 33,  85 => 32,  76 => 25,  71 => 23,  65 => 22,  63 => 21,  60 => 20,  58 => 19,  44 => 13,  38 => 12,  30 => 6,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* */
/* <header id="header" class=" variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/* 	{#======	HEADER TOP	=======#}  */
/* 	<div class="header-top hidden-compact">		*/
/* 		<div class="row no-margin">*/
/* 			<div class="header-top-left col-lg-3 col-md-3 col-sm-6 col-xs-12">*/
/* 				*/
/* 				<ul class="top-link list-inline lang-curr">*/
/* 					{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 					{% if language %} <li class="language">{{ language }} </li>	{% endif %}			*/
/* 				</ul>				*/
/* 				*/
/* 				*/
/* 			</div>*/
/* 			<div class="header-top-center collapsed-block col-lg-6 col-md-6 hidden-sm hidden-xs ">	*/
/* 				{% if soconfig.get_settings('welcome_message_status') %}*/
/* 					<div class="welcome-msg">*/
/* 						{% if soconfig.get_settings('welcome_message') is not empty %}*/
/*                             {{ soconfig.get_settings('welcome_message')}}*/
/*                         {% endif %} */
/* 					</div>*/
/* 				{% endif %}	*/
/* 				*/
/* 								*/
/* 			</div>*/
/* 			<div class="header-top-right col-lg-3 col-md-3 col-sm-6 hidden-xs">*/
/* 							*/
/* 				<ul class="top-log list-inline">*/
/* 					{% if logged %}*/
/* 						<li><a href="{{ logout }}">{{ text_logout }}</a></li>*/
/* 		            {% else %}*/
/* 			            */
/* 			            <li><a href="{{ login }}">{{ text_login }}</a> &nbsp; {{ text_or }} </li>*/
/* 			            <li><a href="{{ register }}">{{ text_register }}</a></li>*/
/* 			            */
/* 		            {% endif %}*/
/* 				</ul>*/
/* 			</div>*/
/* 		</div>		*/
/* 	</div>*/
/* 	*/
/* 	*/
/* 	<div class="header-middle {{hidden_headercenter}}">*/
/* 		*/
/* 			<div class="row no-margin">			*/
/* 				<div class="col-lg-2 hidden-xs hidden-sm hidden-md">*/
/* 					{% if soconfig.get_settings('phone_status') %}*/
/* 					<div class="telephone " >*/
/* 						{{ soconfig.get_settings('contact_number') }}*/
/* 					</div>*/
/* 					{% endif %}	*/
/* 				</div>*/
/* 				<div class="col-lg-8 col-md-10 col-sm-9 col-xs-12">*/
/* 					<div class="main-menu-w">*/
/* 						<div class="menu1">	*/
/* 							{{ content_menu1 }}	*/
/* 						</div>*/
/* 						<div class="logo">*/
/* 					   		{{soconfig.get_logo()}}*/
/* 					   	</div>	*/
/* 					   	<div class="menu2">	*/
/* 					   		{{ content_menu3 }}	*/
/* 					   	</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				<div class="bottom-right col-lg-2 col-md-2 col-sm-3">*/
/* 					<div class="inner">*/
/* 						<div class="search-header-w">*/
/* 							<div class="ico-search"><i class="pe-7s-search"></i></div>								*/
/* 							{{ search_block }}*/
/* 						</div>	*/
/* 					*/
/* 						<div class="account" id="my_account"><a href="{{ account }}"  class="btn-xs dropdown-toggle" title="{{ text_account }} " data-toggle="dropdown"><i class="pe-7s-user"></i></a>*/
/* 							<ul class="dropdown-menu ">*/
/* 									<li><a href="{{ wishlist }}" title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 									*/
/* 								{% if logged %}*/
/* 						            <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/* 						            <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/* 						            <li><a href="{{ transaction }}">{{ text_transaction }}</a></li>*/
/* 						            <li><a href="{{ download }}">{{ text_download }}</a></li>*/
/* 						            <li><a href="{{ logout }}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{ text_logout }}</a></li>*/
/* 					            {% else %}*/
/* */
/* 						            <li><a href="{{ register }}">{{ text_register }}</a></li>*/
/* 						            <li><a href="{{ login }}">{{ text_login }}</a></li>*/
/* 					            {% endif %}															*/
/* 							</ul>*/
/* 						</div>	*/
/* 						*/
/* 							*/
/* 						{% if soconfig.get_settings('iconWishlist_status')  %}*/
/* 						<div class="wishlist hidden-md hidden-sm hidden-xs"><a href="{{ wishlist }}" id="wishlist-total" class="top-link-wishlist" title="{{ text_wishlist }} "><i class="pe-7s-like"></i></a></div>*/
/* 						{% endif %} */
/* 						{% if soconfig.get_settings('iconCompare_status')  %}*/
/* 						<div class="compare hidden-sm hidden-xs"><a href="{{ compare }}"  class="top-link-compare" title="{{ text_compare }} "><i class="pe-7s-graph3"></i></a></div> */
/* 						{% endif %}*/
/* 						*/
/* 						<div class="shopping_cart">							*/
/* 						 	{{ cart }}*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>	*/
/* 				*/
/* 			</div>*/
/* 		*/
/* 	</div>*/
/* 		*/
/* </header>*/
