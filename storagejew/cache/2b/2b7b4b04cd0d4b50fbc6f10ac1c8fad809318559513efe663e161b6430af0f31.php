<?php

/* so-claue/template/extension/module/so_filter_shop_by/default_option.twig */
class __TwigTemplate_27ad36e22c6b475a11ebef00e1f258db91b978e8bbc3e2b678475ff3afb0ecad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li class=\"so-filter-options\" data-option=\"";
        echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), 0, array(), "array");
        echo "\">
\t<div class=\"so-filter-heading\">
\t\t<div class=\"so-filter-heading-text\">
\t\t\t<span>";
        // line 4
        echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), 0, array(), "array");
        echo "</span>
\t\t</div>
\t\t<i class=\"fa fa-chevron-down\"></i>
\t</div>

\t<div class=\"so-filter-content-opts\">
\t\t<div class=\"so-filter-content-opts-container\">
\t\t\t";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["values"]) ? $context["values"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
            // line 12
            echo "\t\t\t\t";
            if (((twig_length_filter($this->env, (isset($context["opt_id"]) ? $context["opt_id"] : null)) > 0) && ((isset($context["opt_id"]) ? $context["opt_id"] : null) != ""))) {
                // line 13
                echo "\t\t\t\t\t";
                $context["class"] = (((twig_in_filter($this->getAttribute($context["value"], "opt_value_id", array()), twig_split_filter($this->env, (isset($context["opt_id"]) ? $context["opt_id"] : null), ",")) == true)) ? ("opt_active") : (""));
                // line 14
                echo "\t\t\t\t";
            } else {
                // line 15
                echo "\t\t\t\t\t";
                $context["class"] = "";
                // line 16
                echo "\t\t\t\t";
            }
            // line 17
            echo "\t\t\t\t<div class=\"so-filter-option opt-select ";
            echo (isset($context["class"]) ? $context["class"] : null);
            echo " ";
            echo (((twig_length_filter($this->env, $this->getAttribute($context["value"], "opt_count_product", array())) > 0)) ? ("opt_enable") : ("opt_disable"));
            echo "\" data-type=\"option\" data-option_id=\"";
            echo $this->getAttribute($context["value"], "option_id", array());
            echo "\" data-option_value=\"";
            echo $this->getAttribute($context["value"], "opt_value_id", array());
            echo "\" data-count_product=\"";
            echo $this->getAttribute($context["value"], "opt_count_product", array());
            echo "\" data-list_product=\"";
            echo $this->getAttribute($context["value"], "opt_list_product", array());
            echo "\">
\t\t\t\t\t<div class=\"so-option-container\">
\t\t\t\t\t\t<div class=\"option-input\">
\t\t\t\t\t\t\t<span class=\"fa fa-square-o\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<label>";
            // line 22
            echo $this->getAttribute($context["value"], "opt_value_name", array());
            echo "</label>
\t\t\t\t\t\t<div class=\"option-count ";
            // line 23
            if (((twig_length_filter($this->env, (isset($context["opt_id"]) ? $context["opt_id"] : null)) > 0) && ((isset($context["opt_id"]) ? $context["opt_id"] : null) != ""))) {
                echo " ";
                echo (((twig_in_filter($this->getAttribute($context["value"], "opt_value_id", array()), twig_split_filter($this->env, (isset($context["opt_id"]) ? $context["opt_id"] : null), ",")) == true)) ? ("opt_close") : (""));
            }
            echo "\">
\t\t\t\t\t\t\t<span>";
            // line 24
            echo $this->getAttribute($context["value"], "opt_count_product", array());
            echo "</span>
\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "\t\t</div>
\t</div>
</li>
";
    }

    public function getTemplateName()
    {
        return "so-claue/template/extension/module/so_filter_shop_by/default_option.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 30,  85 => 24,  78 => 23,  74 => 22,  55 => 17,  52 => 16,  49 => 15,  46 => 14,  43 => 13,  40 => 12,  36 => 11,  26 => 4,  19 => 1,);
    }
}
/* <li class="so-filter-options" data-option="{{ item[0] }}">*/
/* 	<div class="so-filter-heading">*/
/* 		<div class="so-filter-heading-text">*/
/* 			<span>{{ item[0] }}</span>*/
/* 		</div>*/
/* 		<i class="fa fa-chevron-down"></i>*/
/* 	</div>*/
/* */
/* 	<div class="so-filter-content-opts">*/
/* 		<div class="so-filter-content-opts-container">*/
/* 			{% for value in values %}*/
/* 				{% if opt_id|length > 0 and opt_id != "" %}*/
/* 					{% set class = value.opt_value_id in opt_id|split(',') == true ? 'opt_active' : '' %}*/
/* 				{% else %}*/
/* 					{% set class = "" %}*/
/* 				{% endif %}*/
/* 				<div class="so-filter-option opt-select {{ class }} {{ value.opt_count_product|length > 0 ? 'opt_enable' : 'opt_disable' }}" data-type="option" data-option_id="{{ value.option_id }}" data-option_value="{{ value.opt_value_id }}" data-count_product="{{ value.opt_count_product }}" data-list_product="{{ value.opt_list_product }}">*/
/* 					<div class="so-option-container">*/
/* 						<div class="option-input">*/
/* 							<span class="fa fa-square-o"></i>*/
/* 						</div>*/
/* 						<label>{{ value.opt_value_name }}</label>*/
/* 						<div class="option-count {% if opt_id|length > 0 and opt_id != "" %} {{ value.opt_value_id in opt_id|split(',') == true ? 'opt_close' : '' }}{% endif %}">*/
/* 							<span>{{ value.opt_count_product }}</span>*/
/* 							<i class="fa fa-times"></i>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	</div>*/
/* </li>*/
/* */
