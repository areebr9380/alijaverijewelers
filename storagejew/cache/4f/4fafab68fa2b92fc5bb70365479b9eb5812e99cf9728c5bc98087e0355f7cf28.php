<?php

/* so-claue/template/header/header4.twig */
class __TwigTemplate_7797393c932ecf1ba324a4e0c437108c257dce0662baa4fa4efc74da626f4ae5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "
<header id=\"header\" class=\" variant typeheader-";
        // line 5
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">
\t";
        // line 6
        echo "  
\t<div class=\"header-top hidden-compact\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"header-top-left col-lg-3 col-md-6 col-sm-6 hidden-xs\">\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t";
        // line 11
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method")) {
            // line 12
            echo "\t\t\t\t\t<div class=\"telephone\" >
\t\t\t\t\t\t";
            // line 13
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 15
        echo "\t
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-center collapsed-block col-lg-6 hidden-md hidden-xs hidden-sm\">\t
\t\t\t\t\t";
        // line 19
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 20
            echo "\t\t\t\t\t\t<div class=\"hidden-sm hidden-xs welcome-msg\">
\t\t\t\t\t\t\t";
            // line 21
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 22
                echo "                                ";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method");
                echo "
                            ";
            }
            // line 23
            echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 25
        echo "\t
\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-right col-lg-3 col-md-6 col-sm-6 col-xs-12\">\t\t\t\t\t\t\t
\t\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t\t";
        // line 31
        if ((isset($context["currency"]) ? $context["currency"] : null)) {
            echo "<li class=\"currency\"> ";
            echo (isset($context["currency"]) ? $context["currency"] : null);
            echo "  </li> ";
        }
        // line 32
        echo "\t\t\t\t\t\t";
        if ((isset($context["language"]) ? $context["language"] : null)) {
            echo "<li class=\"language\">";
            echo (isset($context["language"]) ? $context["language"] : null);
            echo " </li>\t";
        }
        echo "\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t
\t<div class=\"header-middle ";
        // line 40
        echo (isset($context["hidden_headercenter"]) ? $context["hidden_headercenter"] : null);
        echo "\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">\t\t\t
\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-3 col-xs-4 logo-w\">
\t\t\t\t\t<div class=\"logo\">
\t\t\t\t   \t\t";
        // line 45
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo "
\t\t\t\t   \t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-8 col-md-8 col-sm-6 col-xs-2\">
\t\t\t\t\t<div class=\"main-menu-w\">\t\t\t\t
\t\t\t\t\t\t";
        // line 51
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"bottom-right col-lg-2 col-md-2 col-sm-3 col-xs-6\">
\t\t\t\t\t<div class=\"inner\">
\t\t\t\t\t\t<div class=\"search-header-w\">
\t\t\t\t\t\t\t<div class=\"ico-search\"><i class=\"pe-7s-search\"></i></div>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 58
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
\t\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"account\" id=\"my_account\"><a href=\"";
        // line 61
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\"  class=\"btn-xs dropdown-toggle\" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo " \" data-toggle=\"dropdown\"><i class=\"pe-7s-user\"></i></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu \">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 63
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\" title=\"";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t";
        // line 65
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 66
            echo "\t\t\t\t\t\t            <li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 67
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 68
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 69
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 70
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
\t\t\t\t\t            ";
        } else {
            // line 72
            echo "
\t\t\t\t\t\t            <li><a href=\"";
            // line 73
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
\t\t\t\t\t\t            <li><a href=\"";
            // line 74
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t\t            ";
        }
        // line 75
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 80
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconWishlist_status"), "method")) {
            // line 81
            echo "\t\t\t\t\t\t<div class=\"wishlist hidden-md hidden-sm hidden-xs\"><a href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\" id=\"wishlist-total\" class=\"top-link-wishlist\" title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo " \"><i class=\"pe-7s-like\"></i></a></div>
\t\t\t\t\t\t";
        }
        // line 82
        echo " 
\t\t\t\t\t\t";
        // line 83
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconCompare_status"), "method")) {
            // line 84
            echo "\t\t\t\t\t\t<div class=\"compare hidden-sm hidden-xs\"><a href=\"";
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\"  class=\"top-link-compare\" title=\"";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo " \"><i class=\"pe-7s-graph3\"></i></a></div> 
\t\t\t\t\t\t";
        }
        // line 86
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
\t\t\t\t\t\t \t";
        // line 88
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
\t\t
</header> ";
    }

    public function getTemplateName()
    {
        return "so-claue/template/header/header4.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 88,  231 => 86,  223 => 84,  221 => 83,  218 => 82,  210 => 81,  208 => 80,  201 => 75,  194 => 74,  188 => 73,  185 => 72,  178 => 70,  172 => 69,  166 => 68,  160 => 67,  153 => 66,  151 => 65,  142 => 63,  135 => 61,  129 => 58,  119 => 51,  110 => 45,  102 => 40,  86 => 32,  80 => 31,  72 => 25,  67 => 23,  61 => 22,  59 => 21,  56 => 20,  54 => 19,  48 => 15,  42 => 13,  39 => 12,  37 => 11,  30 => 6,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* */
/* <header id="header" class=" variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/* 	{#======	HEADER TOP	=======#}  */
/* 	<div class="header-top hidden-compact">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<div class="header-top-left col-lg-3 col-md-6 col-sm-6 hidden-xs">												*/
/* 					{% if soconfig.get_settings('phone_status') %}*/
/* 					<div class="telephone" >*/
/* 						{{ soconfig.get_settings('contact_number') }}*/
/* 					</div>*/
/* 					{% endif %}	*/
/* 					*/
/* 				</div>*/
/* 				<div class="header-top-center collapsed-block col-lg-6 hidden-md hidden-xs hidden-sm">	*/
/* 					{% if soconfig.get_settings('welcome_message_status') %}*/
/* 						<div class="hidden-sm hidden-xs welcome-msg">*/
/* 							{% if soconfig.get_settings('welcome_message') is not empty %}*/
/*                                 {{ soconfig.get_settings('welcome_message')}}*/
/*                             {% endif %} */
/* 						</div>*/
/* 					{% endif %}	*/
/* 					*/
/* 									*/
/* 				</div>*/
/* 				<div class="header-top-right col-lg-3 col-md-6 col-sm-6 col-xs-12">							*/
/* 					<ul class="top-link list-inline lang-curr">*/
/* 						{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 						{% if language %}<li class="language">{{ language }} </li>	{% endif %}			*/
/* 					</ul>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	*/
/* 	*/
/* 	<div class="header-middle {{hidden_headercenter}}">*/
/* 		<div class="container">*/
/* 			<div class="row">			*/
/* 				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 logo-w">*/
/* 					<div class="logo">*/
/* 				   		{{soconfig.get_logo()}}*/
/* 				   	</div>*/
/* 					*/
/* 				</div>*/
/* 				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-2">*/
/* 					<div class="main-menu-w">				*/
/* 						{{ content_menu1 }}																		 */
/* 					</div>*/
/* 				</div>*/
/* 				<div class="bottom-right col-lg-2 col-md-2 col-sm-3 col-xs-6">*/
/* 					<div class="inner">*/
/* 						<div class="search-header-w">*/
/* 							<div class="ico-search"><i class="pe-7s-search"></i></div>								*/
/* 							{{ search_block }}*/
/* 						</div>	*/
/* 					*/
/* 						<div class="account" id="my_account"><a href="{{ account }}"  class="btn-xs dropdown-toggle" title="{{ text_account }} " data-toggle="dropdown"><i class="pe-7s-user"></i></a>*/
/* 							<ul class="dropdown-menu ">*/
/* 									<li><a href="{{ wishlist }}" title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 									 */
/* 								{% if logged %}*/
/* 						            <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/* 						            <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/* 						            <li><a href="{{ transaction }}">{{ text_transaction }}</a></li>*/
/* 						            <li><a href="{{ download }}">{{ text_download }}</a></li>*/
/* 						            <li><a href="{{ logout }}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{ text_logout }}</a></li>*/
/* 					            {% else %}*/
/* */
/* 						            <li><a href="{{ register }}">{{ text_register }}</a></li>*/
/* 						            <li><a href="{{ login }}">{{ text_login }}</a></li>*/
/* 					            {% endif %}															*/
/* 							</ul>*/
/* 						</div>	*/
/* 						*/
/* 							*/
/* 						{% if soconfig.get_settings('iconWishlist_status')  %}*/
/* 						<div class="wishlist hidden-md hidden-sm hidden-xs"><a href="{{ wishlist }}" id="wishlist-total" class="top-link-wishlist" title="{{ text_wishlist }} "><i class="pe-7s-like"></i></a></div>*/
/* 						{% endif %} */
/* 						{% if soconfig.get_settings('iconCompare_status')  %}*/
/* 						<div class="compare hidden-sm hidden-xs"><a href="{{ compare }}"  class="top-link-compare" title="{{ text_compare }} "><i class="pe-7s-graph3"></i></a></div> */
/* 						{% endif %}*/
/* 						*/
/* 						<div class="shopping_cart">							*/
/* 						 	{{ cart }}*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>	*/
/* 				*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 		*/
/* </header> */
