<?php

/* so-claue/template/extension/module/so_deals/default.twig */
class __TwigTemplate_6270c1da57182ccd0e1ad8ee30c675c4bee8cc3e4b63fcc1e76d4378d0c7f1c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
//<![CDATA[
\tvar listdeal";
        // line 3
        echo (isset($context["module"]) ? $context["module"] : null);
        echo " = [];
//]]>
</script>
<div class=\"module ";
        // line 6
        echo (isset($context["direction_class"]) ? $context["direction_class"] : null);
        echo " ";
        echo (isset($context["class_suffix"]) ? $context["class_suffix"] : null);
        echo "\">
    ";
        // line 7
        if ((isset($context["disp_title_module"]) ? $context["disp_title_module"] : null)) {
            // line 8
            echo "\t\t<h3 class=\"modtitle\"><span>";
            echo (isset($context["head_name"]) ? $context["head_name"] : null);
            echo "</span>
\t\t\t
\t\t</h3>
\t";
        }
        // line 12
        echo "\t";
        if (((isset($context["pre_text"]) ? $context["pre_text"] : null) != "")) {
            // line 13
            echo "\t\t\t\t<div class=\"pretext\">
\t\t\t\t\t";
            // line 14
            echo (isset($context["pre_text"]) ? $context["pre_text"] : null);
            echo "
\t\t\t\t</div>
\t\t\t";
        }
        // line 17
        echo "\t
\t<div class=\"modcontent\">
\t\t";
        // line 19
        if ((array_key_exists("list", $context) && (isset($context["list"]) ? $context["list"] : null))) {
            // line 20
            echo "\t\t    ";
            $context["tag_id"] = (("so_deals_" . twig_random($this->env)) . twig_date_format_filter($this->env, "now", "mdYHis"));
            // line 21
            echo "\t\t    ";
            $context["class_respl"] = ((((((((("preset00-" . (isset($context["nb_column0"]) ? $context["nb_column0"] : null)) . " preset01-") . (isset($context["nb_column1"]) ? $context["nb_column1"] : null)) . " preset02-") . (isset($context["nb_column2"]) ? $context["nb_column2"] : null)) . " preset03-") . (isset($context["nb_column3"]) ? $context["nb_column3"] : null)) . " preset04-") . (isset($context["nb_column4"]) ? $context["nb_column4"] : null));
            // line 22
            echo "\t\t    ";
            $context["i"] = 0;
            // line 23
            echo "\t\t    ";
            $context["count_item"] = twig_length_filter($this->env, (isset($context["list"]) ? $context["list"] : null));
            // line 24
            echo "\t\t    ";
            if (((isset($context["include_js"]) ? $context["include_js"] : null) == "owlCarousel")) {
                // line 25
                echo "\t\t    \t";
                $this->loadTemplate(((((isset($context["config_theme"]) ? $context["config_theme"] : null) . "/template/extension/module/so_deals/") . (isset($context["store_layout"]) ? $context["store_layout"] : null)) . "_carousel.twig"), "so-claue/template/extension/module/so_deals/default.twig", 25)->display($context);
                // line 26
                echo "\t\t    ";
            } elseif (((isset($context["include_js"]) ? $context["include_js"] : null) == "slick")) {
                // line 27
                echo "\t\t    \t";
                echo twig_include($this->env, $context, ((((isset($context["config_theme"]) ? $context["config_theme"] : null) . "/template/extension/module/so_deals/") . (isset($context["store_layout"]) ? $context["store_layout"] : null)) . "_slick.twig"));
                echo "
\t\t    ";
            } else {
                // line 29
                echo "\t\t    \t";
                echo "";
                echo "
\t\t    ";
            }
            // line 31
            echo "\t\t";
        } else {
            // line 32
            echo "\t    \t";
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_noitem"), "method");
            echo "
\t    ";
        }
        // line 34
        echo "\t</div>
\t
\t";
        // line 36
        if (((isset($context["post_text"]) ? $context["post_text"] : null) != "")) {
            // line 37
            echo "\t\t<div class=\"form-group\">
\t\t\t";
            // line 38
            echo (isset($context["post_text"]) ? $context["post_text"] : null);
            echo "
\t\t</div>
\t";
        }
        // line 41
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "so-claue/template/extension/module/so_deals/default.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 41,  114 => 38,  111 => 37,  109 => 36,  105 => 34,  99 => 32,  96 => 31,  90 => 29,  84 => 27,  81 => 26,  78 => 25,  75 => 24,  72 => 23,  69 => 22,  66 => 21,  63 => 20,  61 => 19,  57 => 17,  51 => 14,  48 => 13,  45 => 12,  37 => 8,  35 => 7,  29 => 6,  23 => 3,  19 => 1,);
    }
}
/* <script>*/
/* //<![CDATA[*/
/* 	var listdeal{{ module }} = [];*/
/* //]]>*/
/* </script>*/
/* <div class="module {{ direction_class }} {{ class_suffix }}">*/
/*     {% if disp_title_module %}*/
/* 		<h3 class="modtitle"><span>{{ head_name }}</span>*/
/* 			*/
/* 		</h3>*/
/* 	{% endif %}*/
/* 	{% if pre_text != '' %}*/
/* 				<div class="pretext">*/
/* 					{{ pre_text }}*/
/* 				</div>*/
/* 			{% endif %}*/
/* 	*/
/* 	<div class="modcontent">*/
/* 		{% if list is defined and list %}*/
/* 		    {% set tag_id = 'so_deals_' ~ random() ~ "now"|date("mdYHis") %}*/
/* 		    {% set class_respl = 'preset00-'~nb_column0~' preset01-'~nb_column1~' preset02-'~nb_column2~' preset03-'~nb_column3~' preset04-'~nb_column4 %}*/
/* 		    {% set i = 0 %}*/
/* 		    {% set count_item = list|length %}*/
/* 		    {% if include_js == 'owlCarousel' %}*/
/* 		    	{% include (config_theme~'/template/extension/module/so_deals/'~store_layout~'_carousel.twig') %}*/
/* 		    {% elseif include_js == 'slick' %}*/
/* 		    	{{ include (config_theme~'/template/extension/module/so_deals/'~store_layout~'_slick.twig') }}*/
/* 		    {% else %}*/
/* 		    	{{ '' }}*/
/* 		    {% endif %}*/
/* 		{% else %}*/
/* 	    	{{ objlang.get('text_noitem') }}*/
/* 	    {% endif %}*/
/* 	</div>*/
/* 	*/
/* 	{% if post_text != '' %}*/
/* 		<div class="form-group">*/
/* 			{{ post_text }}*/
/* 		</div>*/
/* 	{% endif %}*/
/* </div>*/
/* */
