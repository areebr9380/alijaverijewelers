<?php

/* so-claue/template/common/header.twig */
class __TwigTemplate_baae057614a4c8daa2998de78fa7a0233b6ce471b01f71d2159c2001bc5e8296 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo "<!DOCTYPE html>
<html dir=\"";
        // line 10
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<head>
<meta charset=\"UTF-8\" />
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 14
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 15
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> 
";
        // line 17
        if ((isset($context["description"]) ? $context["description"] : null)) {
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />";
        }
        // line 18
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            echo "<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />";
        }
        // line 19
        echo "<!--[if IE]><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\"><![endif]-->
";
        // line 21
        if (((isset($context["direction"]) ? $context["direction"] : null) == "ltr")) {
            echo " ";
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/bootstrap/css/bootstrap.min.css"), "method");
            echo "
";
        } elseif ((        // line 22
(isset($context["direction"]) ? $context["direction"] : null) == "rtl")) {
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/soconfig/css/bootstrap/bootstrap.rtl.min.css"), "method");
            echo " 
";
        } else {
            // line 23
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/bootstrap/css/bootstrap.min.css"), "method");
            echo " ";
        }
        // line 24
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/font-awesome/css/font-awesome.min.css"), "method");
        echo "
";
        // line 25
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/soconfig/css/lib.css"), "method");
        echo "
";
        // line 26
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => (("catalog/view/theme/" . (isset($context["theme_directory"]) ? $context["theme_directory"] : null)) . "/css/ie9-and-up.css")), "method");
        echo "
";
        // line 27
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => (("catalog/view/theme/" . (isset($context["theme_directory"]) ? $context["theme_directory"] : null)) . "/css/custom.css")), "method");
        echo "
";
        // line 28
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/pe-icon-7-stroke/css/pe-icon-7-stroke.css"), "method");
        echo "
";
        // line 29
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => "catalog/view/javascript/pe-icon-7-stroke/css/helper.css"), "method");
        echo "
";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addCss", array(0 => $this->getAttribute($context["style"], "href", array())), "method");
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/jquery/jquery-2.1.1.min.js"), "method");
        echo "
";
        // line 33
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/bootstrap/js/bootstrap.min.js"), "method");
        echo "
";
        // line 34
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/libs.js"), "method");
        echo "
";
        // line 35
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/so.system.js"), "method");
        echo "
";
        // line 36
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/jquery.sticky-kit.min.js"), "method");
        echo "
";
        // line 37
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/lazysizes/lazysizes.min.js"), "method");
        echo "
";
        // line 38
        if (((isset($context["class"]) ? $context["class"] : null) == "information-information")) {
            echo " ";
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/typo/element.js"), "method");
            echo " ";
        }
        // line 39
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_status"), "method")) {
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/toppanel.js"), "method");
        }
        // line 40
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typelayout"), "method") == "7")) {
            // line 41
            echo "\t";
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/isotope.pkgd.min.js"), "method");
            echo "
\t";
            // line 42
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => "catalog/view/javascript/soconfig/js/imagesloaded.pkgd.min.js"), "method");
            echo "
";
        }
        // line 44
        echo "

";
        // line 46
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => (("catalog/view/theme/" . (isset($context["theme_directory"]) ? $context["theme_directory"] : null)) . "/js/so.custom.js")), "method");
        echo "
";
        // line 47
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => (("catalog/view/theme/" . (isset($context["theme_directory"]) ? $context["theme_directory"] : null)) . "/js/common.js")), "method");
        echo "

";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            echo " ";
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "addJs", array(0 => $context["script"]), "method");
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "scss_compass", array());
        echo "
";
        // line 52
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "css_out", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "cssExclude"), "method")), "method");
        echo "
";
        // line 53
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "js_out", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "jsExclude"), "method")), "method");
        echo "
";
        // line 55
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "cssinput_status"), "method") &&  !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "cssinput_content"), "method")))) {
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "cssinput_content"), "method");
            echo " ";
        }
        echo " 
";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            echo "<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            echo $context["analytic"];
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "
";
        // line 60
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "layoutstyle"), "method") == "boxed")) {
            echo " 
<style type=\"text/css\">
body {
    background-color:";
            // line 63
            echo (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "theme_bgcolor"), "method")) ? ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "theme_bgcolor"), "method")) : ("none"));
            echo " ;       
    ";
            // line 64
            if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contentbg"), "method") != "")) {
                // line 65
                echo "        background-image: url(\"image/";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contentbg"), "method");
                echo " \");
    ";
            }
            // line 67
            echo "    background-repeat:";
            echo (( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "content_bg_mode"), "method"))) ? ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "content_bg_mode"), "method")) : ("no-repeat"));
            echo " ;
    background-attachment:";
            // line 68
            echo (( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "content_attachment"), "method"))) ? ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "content_attachment"), "method")) : ("inherit"));
            echo " ;
    background-position:top center; 
}
</style>
";
        }
        // line 72
        echo " 
 
</head>
";
        // line 76
        $context["layoutbox"] = (( !twig_test_empty((isset($context["url_layoutbox"]) ? $context["url_layoutbox"] : null))) ? ((isset($context["url_layoutbox"]) ? $context["url_layoutbox"] : null)) : ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "layoutstyle"), "method")));
        // line 77
        $context["cls_body"] = ((isset($context["class"]) ? $context["class"] : null) . " ");
        // line 78
        $context["cls_body"] = ((isset($context["cls_body"]) ? $context["cls_body"] : null) . (isset($context["direction"]) ? $context["direction"] : null));
        // line 79
        $context["cls_body"] = (((isset($context["cls_body"]) ? $context["cls_body"] : null) . " layout-") . $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typelayout"), "method"));
        // line 80
        $context["cls_wrapper"] = ("wrapper-" . (isset($context["layoutbox"]) ? $context["layoutbox"] : null));
        // line 81
        $context["cls_wrapper"] = (((isset($context["cls_wrapper"]) ? $context["cls_wrapper"] : null) . " banners-effect-") . $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "type_banner"), "method"));
        // line 82
        echo "\t
<body class=\"";
        // line 83
        echo (isset($context["cls_body"]) ? $context["cls_body"] : null);
        echo "\">
<div id=\"wrapper\" class=\"";
        // line 84
        echo (isset($context["cls_wrapper"]) ? $context["cls_wrapper"] : null);
        echo "\">  
 
";
        // line 87
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "preloader"), "method")) {
            // line 88
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/preloader.twig"), "so-claue/template/common/header.twig", 88)->display(array_merge($context, array("preloader" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "preloader_animation"), "method"))));
        }
        // line 90
        echo "
";
        // line 92
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typeheader"), "method")) {
            // line 93
            echo "\t";
            $this->loadTemplate(((((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/header/header") . $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typeheader"), "method")) . ".twig"), "so-claue/template/common/header.twig", 93)->display(array_merge($context, array("typeheader" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typeheader"), "method"))));
            // line 94
            echo "
";
        } else {
            // line 95
            echo "\t
\t<style>
\t\t.alert-primary .alert-link {color: #002752;}
\t\t.alert-link {font-weight: 700;text-decoration: none;}
\t\t.alert-link:hover{text-decoration: underline;}
\t\t.alert {color: #004085;background-color: #cce5ff;padding: .75rem 1.25rem;margin-bottom: 1rem;border: 1px solid #b8daff;border-radius: .25rem;
\t\t}
\t</style>
\t<div class=\"alert alert-primary\">
\t\t\tGo to admin, find Extensions >>  <a class=\"alert-link\" href=\"admin/index.php?route=marketplace/modification\"  target=”_blank”> Modifications </a> and click 'Refresh' button. To apply the changes characterised by the uploaded modification file
\t</div>
";
        }
        // line 107
        echo "
<div id=\"socialLogin\"></div>
 
 ";
        // line 110
        if (((array_key_exists("setting", $context) && $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_enable", array())) && $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_popuplogin", array()))) {
            echo " 
 <div class=\"modal fade in\" id=\"so_sociallogin\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"> 
 <div class=\"modal-dialog block-popup-login\"> 
 <a href=\"javascript:void(0)\" title=\"Close\" class=\"close close-login fa fa-times-circle\" data-dismiss=\"modal\"></a> 
 <div class=\"tt_popup_login\"><strong>";
            // line 114
            echo (isset($context["text_title_popuplogin"]) ? $context["text_title_popuplogin"] : null);
            echo "</strong></div> 
 <div class=\"block-content\"> 
 <div class=\" col-reg registered-account\"> 
 <div class=\"block-content\"> 
 <form class=\"form form-login\" action=\"";
            // line 118
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\" method=\"post\" id=\"login-form\"> 
 <fieldset class=\"fieldset login\" data-hasrequired=\"* Required Fields\"> 
 <div class=\"field email required email-input\"> 
 <div class=\"control\"> 
 <input name=\"email\" value=\"\" autocomplete=\"off\" id=\"email\" type=\"email\" class=\"input-text\" title=\"Email\" placeholder=\"E-mail Address\" /> 
 </div> 
 </div> 
 <div class=\"field password required pass-input\"> 
 <div class=\"control\"> 
 <input name=\"password\" type=\"password\" autocomplete=\"off\" class=\"input-text\" id=\"pass\" title=\"Password\" placeholder=\"Password\" /> 
 </div> 
 </div> 
 ";
            // line 130
            if ($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_enable", array())) {
                echo " 
 <div class=\" form-group\"> 
 <label class=\"control-label\">";
                // line 132
                echo (isset($context["text_title_login_with_social"]) ? $context["text_title_login_with_social"] : null);
                echo "</label> 
 <div> 
 ";
                // line 134
                if ($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_googlestatus", array())) {
                    echo " 
 ";
                    // line 135
                    if (($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_button", array()) == "icon")) {
                        echo " 
 <a href=\"";
                        // line 136
                        echo (isset($context["googlelink"]) ? $context["googlelink"] : null);
                        echo "\" class=\"btn btn-social-icon btn-sm btn-google-plus\"><i class=\"fa fa-google fa-fw\" aria-hidden=\"true\"></i></a> 
 ";
                    } else {
                        // line 137
                        echo " 
 <a class=\"social-icon\" href=\"";
                        // line 138
                        echo (isset($context["googlelink"]) ? $context["googlelink"] : null);
                        echo "\"> 
 <img class=\"img-responsive\" src=\"";
                        // line 139
                        echo (isset($context["googleimage"]) ? $context["googleimage"] : null);
                        echo "\" 
 data-toggle=\"tooltip\" alt=\"";
                        // line 140
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_googletitle", array());
                        echo "\" 
 title=\"";
                        // line 141
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_googletitle", array());
                        echo "\" 
 /> 
 </a> 
 ";
                    }
                    // line 144
                    echo " 
 ";
                }
                // line 145
                echo " 
 ";
                // line 146
                if ($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_fbstatus", array())) {
                    echo " 
 ";
                    // line 147
                    if (($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_button", array()) == "icon")) {
                        echo " 
 <a href=\"";
                        // line 148
                        echo (isset($context["fblink"]) ? $context["fblink"] : null);
                        echo "\" class=\"btn btn-social-icon btn-sm btn-facebook\"><i class=\"fa fa-facebook fa-fw\" aria-hidden=\"true\"></i></a> 
 ";
                    } else {
                        // line 149
                        echo " 
 <a href=\"";
                        // line 150
                        echo (isset($context["fblink"]) ? $context["fblink"] : null);
                        echo "\" class=\"social-icon\"> 
 <img class=\"img-responsive\" src=\"";
                        // line 151
                        echo (isset($context["fbimage"]) ? $context["fbimage"] : null);
                        echo "\" 
 data-toggle=\"tooltip\" alt=\"";
                        // line 152
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_fbtitle", array());
                        echo "\" 
 title=\"";
                        // line 153
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_fbtitle", array());
                        echo "\" 
 /> 
 </a> 
 ";
                    }
                    // line 156
                    echo " 
 ";
                }
                // line 157
                echo " 
 ";
                // line 158
                if ($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_twitstatus", array())) {
                    echo " 
 ";
                    // line 159
                    if (($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_button", array()) == "icon")) {
                        echo " 
 <a href=\"";
                        // line 160
                        echo (isset($context["twitlink"]) ? $context["twitlink"] : null);
                        echo "\" class=\"btn btn-social-icon btn-sm btn-twitter\"><i class=\"fa fa-twitter fa-fw\" aria-hidden=\"true\"></i></a> 
 ";
                    } else {
                        // line 161
                        echo " 
 <a class=\"social-icon\" href=\"";
                        // line 162
                        echo (isset($context["twitlink"]) ? $context["twitlink"] : null);
                        echo "\"> 
 <img class=\"img-responsive\" src=\"";
                        // line 163
                        echo (isset($context["twitimage"]) ? $context["twitimage"] : null);
                        echo "\" 
 data-toggle=\"tooltip\" alt=\"";
                        // line 164
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_twittertitle", array());
                        echo "\" 
 title=\"";
                        // line 165
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_twittertitle", array());
                        echo "\" 
 /> 
 </a> 
 ";
                    }
                    // line 168
                    echo " 
 ";
                }
                // line 169
                echo " 
 ";
                // line 170
                if ($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_linkstatus", array())) {
                    echo " 
 ";
                    // line 171
                    if (($this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_button", array()) == "icon")) {
                        echo " 
 <a href=\"";
                        // line 172
                        echo (isset($context["linkdinlink"]) ? $context["linkdinlink"] : null);
                        echo "\" class=\"btn btn-social-icon btn-sm btn-linkdin\"><i class=\"fa fa-linkedin fa-fw\" aria-hidden=\"true\"></i></a> 
 ";
                    } else {
                        // line 173
                        echo " 
 <a class=\"social-icon\" href=\"";
                        // line 174
                        echo (isset($context["linkdinlink"]) ? $context["linkdinlink"] : null);
                        echo "\"> 
 <img class=\"img-responsive\" src=\"";
                        // line 175
                        echo (isset($context["linkdinimage"]) ? $context["linkdinimage"] : null);
                        echo "\" 
 data-toggle=\"tooltip\" alt=\"";
                        // line 176
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_linkedintitle", array());
                        echo "\" 
 title=\"";
                        // line 177
                        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "so_sociallogin_linkedintitle", array());
                        echo "\" 
 /> 
 </a> 
 ";
                    }
                    // line 180
                    echo " 
 ";
                }
                // line 181
                echo " 
 </div> 
 </div> 
 ";
            }
            // line 184
            echo " 
 <div class=\"secondary ft-link-p\"><a class=\"action remind\" href=\"";
            // line 185
            echo (isset($context["link_forgot_password"]) ? $context["link_forgot_password"] : null);
            echo "\"><span>";
            echo (isset($context["text_forgot_password"]) ? $context["text_forgot_password"] : null);
            echo "</span></a></div> 
 <div class=\"actions-toolbar\"> 
 <div class=\"primary\"><button type=\"submit\" class=\"action login primary\" name=\"send\" id=\"send2\"><span>";
            // line 187
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</span></button></div> 
 </div> 
 </fieldset> 
 </form> 
 </div> 
 </div> 
 <div class=\"col-reg login-customer\"> 
 ";
            // line 194
            echo (isset($context["text_colregister"]) ? $context["text_colregister"] : null);
            echo " 
 <a class=\"btn-reg-popup\" title=\"";
            // line 195
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "\" href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_create_account"]) ? $context["text_create_account"] : null);
            echo "</a> 
 </div> 
 <div style=\"clear:both;\"></div> 
 </div> 
 </div> 
 </div> 
 <script type=\"text/javascript\"> 
 jQuery(document).ready(function(\$) { 
 var \$window = \$(window); 
 function checkWidth() { 
 var windowsize = \$window.width(); 
 if (windowsize > 767) { 
 \$('a[href*=\"account/login\"]').click(function (e) { 
 e.preventDefault(); 
 \$(\"#so_sociallogin\").modal('show'); 
 }); 
 } 
 } 
 checkWidth(); 
 \$(window).resize(checkWidth); 
 }); 
 </script> 
 ";
        }
        // line 217
        echo " 
 
";
    }

    public function getTemplateName()
    {
        return "so-claue/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  579 => 217,  549 => 195,  545 => 194,  535 => 187,  528 => 185,  525 => 184,  519 => 181,  515 => 180,  508 => 177,  504 => 176,  500 => 175,  496 => 174,  493 => 173,  488 => 172,  484 => 171,  480 => 170,  477 => 169,  473 => 168,  466 => 165,  462 => 164,  458 => 163,  454 => 162,  451 => 161,  446 => 160,  442 => 159,  438 => 158,  435 => 157,  431 => 156,  424 => 153,  420 => 152,  416 => 151,  412 => 150,  409 => 149,  404 => 148,  400 => 147,  396 => 146,  393 => 145,  389 => 144,  382 => 141,  378 => 140,  374 => 139,  370 => 138,  367 => 137,  362 => 136,  358 => 135,  354 => 134,  349 => 132,  344 => 130,  329 => 118,  322 => 114,  315 => 110,  310 => 107,  296 => 95,  292 => 94,  289 => 93,  287 => 92,  284 => 90,  280 => 88,  278 => 87,  273 => 84,  269 => 83,  266 => 82,  264 => 81,  262 => 80,  260 => 79,  258 => 78,  256 => 77,  254 => 76,  249 => 72,  241 => 68,  236 => 67,  230 => 65,  228 => 64,  224 => 63,  218 => 60,  215 => 59,  206 => 58,  193 => 57,  186 => 55,  182 => 53,  178 => 52,  174 => 51,  163 => 49,  158 => 47,  154 => 46,  150 => 44,  145 => 42,  140 => 41,  138 => 40,  134 => 39,  128 => 38,  124 => 37,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  104 => 32,  95 => 30,  91 => 29,  87 => 28,  83 => 27,  79 => 26,  75 => 25,  71 => 24,  67 => 23,  61 => 22,  55 => 21,  52 => 19,  46 => 18,  40 => 17,  35 => 15,  31 => 14,  22 => 10,  19 => 9,);
    }
}
/* {#*/
/* ****************************************************** */
/*  * @package	SO Framework for Opencart 3.x*/
/*  * @author	http://www.opencartworks.com*/
/*  * @license	GNU General Public License*/
/*  * @copyright(C) 2008-2017 opencartworks.com. All rights reserved.*/
/*  *******************************************************/
/* #}*/
/* <!DOCTYPE html>*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1"> */
/* {% if description %}<meta name="description" content="{{ description }}" />{% endif %}*/
/* {% if keywords %}<meta name="keywords" content="{{ keywords }}" />{% endif %}*/
/* <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->*/
/* {# =========== Begin General CSS ==============#}*/
/* {% if direction=='ltr' %} {{soconfig.addCss('catalog/view/javascript/bootstrap/css/bootstrap.min.css')}}*/
/* {% elseif direction=='rtl' %}{{soconfig.addCss('catalog/view/javascript/soconfig/css/bootstrap/bootstrap.rtl.min.css')}} */
/* {% else %}{{soconfig.addCss('catalog/view/javascript/bootstrap/css/bootstrap.min.css')}} {% endif %}*/
/* {{soconfig.addCss('catalog/view/javascript/font-awesome/css/font-awesome.min.css')}}*/
/* {{soconfig.addCss('catalog/view/javascript/soconfig/css/lib.css')}}*/
/* {{soconfig.addCss('catalog/view/theme/'~theme_directory~'/css/ie9-and-up.css')}}*/
/* {{soconfig.addCss('catalog/view/theme/'~theme_directory~'/css/custom.css')}}*/
/* {{soconfig.addCss('catalog/view/javascript/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}*/
/* {{soconfig.addCss('catalog/view/javascript/pe-icon-7-stroke/css/helper.css')}}*/
/* {% for style in styles %}{{ soconfig.addCss(style.href)}}{% endfor %}*/
/* {# =========== Begin General Scripts ==============#}*/
/* {{soconfig.addJs('catalog/view/javascript/jquery/jquery-2.1.1.min.js')}}*/
/* {{soconfig.addJs('catalog/view/javascript/bootstrap/js/bootstrap.min.js')}}*/
/* {{soconfig.addJs('catalog/view/javascript/soconfig/js/libs.js')}}*/
/* {{soconfig.addJs('catalog/view/javascript/soconfig/js/so.system.js')}}*/
/* {{soconfig.addJs('catalog/view/javascript/soconfig/js/jquery.sticky-kit.min.js')}}*/
/* {{soconfig.addJs('catalog/view/javascript/lazysizes/lazysizes.min.js')}}*/
/* {% if class=='information-information' %} {{soconfig.addJs('catalog/view/javascript/soconfig/js/typo/element.js')}} {% endif %}*/
/* {% if soconfig.get_settings('toppanel_status') %}{{soconfig.addJs('catalog/view/javascript/soconfig/js/toppanel.js')}}{% endif %}*/
/* {% if soconfig.get_settings('typelayout') =='7' %}*/
/* 	{{soconfig.addJs('catalog/view/javascript/soconfig/js/isotope.pkgd.min.js')}}*/
/* 	{{soconfig.addJs('catalog/view/javascript/soconfig/js/imagesloaded.pkgd.min.js')}}*/
/* {% endif %}*/
/* */
/* */
/* {{soconfig.addJs('catalog/view/theme/'~theme_directory~'/js/so.custom.js')}}*/
/* {{soconfig.addJs('catalog/view/theme/'~theme_directory~'/js/common.js')}}*/
/* */
/* {% for script in scripts %} {{soconfig.addJs(script)}} {% endfor %}*/
/* {# =========== Begin Other CSS & JS  ==============#}*/
/* {{soconfig.scss_compass}}*/
/* {{soconfig.css_out(soconfig.get_settings('cssExclude'))}}*/
/* {{soconfig.js_out(soconfig.get_settings('jsExclude'))}}*/
/* {# =========== Custom Code Editor ==============#}*/
/* {% if soconfig.get_settings('cssinput_status') and (soconfig.get_settings('cssinput_content')) is not empty %}{{ soconfig.get_settings('cssinput_content')  }} {% endif %} */
/* {# =========== Begin Google Analytics ==============#}*/
/* {% for link in links %}<link href="{{ link.href }}" rel="{{ link.rel }}" />{% endfor %}*/
/* {% for analytic in analytics %}{{ analytic }}{% endfor %}*/
/* */
/* {% if soconfig.get_settings('layoutstyle') == 'boxed'  %} */
/* <style type="text/css">*/
/* body {*/
/*     background-color:{{ soconfig.get_settings('theme_bgcolor') ? soconfig.get_settings('theme_bgcolor') : 'none' }} ;       */
/*     {% if soconfig.get_settings('contentbg') !='' %}*/
/*         background-image: url("image/{{soconfig.get_settings('contentbg') }} ");*/
/*     {% endif %}*/
/*     background-repeat:{{ soconfig.get_settings('content_bg_mode') is not empty ? soconfig.get_settings('content_bg_mode') : 'no-repeat' }} ;*/
/*     background-attachment:{{ soconfig.get_settings('content_attachment') is not empty ? soconfig.get_settings('content_attachment') : 'inherit' }} ;*/
/*     background-position:top center; */
/* }*/
/* </style>*/
/* {% endif %} */
/*  */
/* </head>*/
/* {# =========== Add class Body ==============#}*/
/* {% set layoutbox =  url_layoutbox is not empty  ? url_layoutbox : soconfig.get_settings('layoutstyle') %}*/
/* {% set cls_body = class ~ ' ' %}*/
/* {% set cls_body = cls_body ~ direction %}*/
/* {% set cls_body = cls_body ~ ' layout-'~soconfig.get_settings('typelayout')%}*/
/* {% set cls_wrapper = 'wrapper-'~layoutbox%}*/
/* {% set cls_wrapper = cls_wrapper ~ ' banners-effect-'~soconfig.get_settings('type_banner')%}*/
/* 	*/
/* <body class="{{cls_body}}">*/
/* <div id="wrapper" class="{{cls_wrapper}}">  */
/*  */
/* {# =========== Show Preloader ==============#}*/
/* {% if soconfig.get_settings('preloader')%}*/
/* 	{% include theme_directory~'/template/soconfig/preloader.twig' with {preloader: soconfig.get_settings('preloader_animation')} %}*/
/* {% endif %}*/
/* */
/* {# =========== Show Header==============#}*/
/* {% if soconfig.get_settings('typeheader') %}*/
/* 	{% include theme_directory~'/template/header/header'~ soconfig.get_settings('typeheader') ~'.twig' with {typeheader: soconfig.get_settings('typeheader')} %}*/
/* */
/* {% else%}	*/
/* 	<style>*/
/* 		.alert-primary .alert-link {color: #002752;}*/
/* 		.alert-link {font-weight: 700;text-decoration: none;}*/
/* 		.alert-link:hover{text-decoration: underline;}*/
/* 		.alert {color: #004085;background-color: #cce5ff;padding: .75rem 1.25rem;margin-bottom: 1rem;border: 1px solid #b8daff;border-radius: .25rem;*/
/* 		}*/
/* 	</style>*/
/* 	<div class="alert alert-primary">*/
/* 			Go to admin, find Extensions >>  <a class="alert-link" href="admin/index.php?route=marketplace/modification"  target=”_blank”> Modifications </a> and click 'Refresh' button. To apply the changes characterised by the uploaded modification file*/
/* 	</div>*/
/* {% endif %}*/
/* */
/* <div id="socialLogin"></div>*/
/*  */
/*  {% if setting is defined and setting.so_sociallogin_enable and setting.so_sociallogin_popuplogin %} */
/*  <div class="modal fade in" id="so_sociallogin" tabindex="-1" role="dialog" aria-hidden="true"> */
/*  <div class="modal-dialog block-popup-login"> */
/*  <a href="javascript:void(0)" title="Close" class="close close-login fa fa-times-circle" data-dismiss="modal"></a> */
/*  <div class="tt_popup_login"><strong>{{ text_title_popuplogin }}</strong></div> */
/*  <div class="block-content"> */
/*  <div class=" col-reg registered-account"> */
/*  <div class="block-content"> */
/*  <form class="form form-login" action="{{ login }}" method="post" id="login-form"> */
/*  <fieldset class="fieldset login" data-hasrequired="* Required Fields"> */
/*  <div class="field email required email-input"> */
/*  <div class="control"> */
/*  <input name="email" value="" autocomplete="off" id="email" type="email" class="input-text" title="Email" placeholder="E-mail Address" /> */
/*  </div> */
/*  </div> */
/*  <div class="field password required pass-input"> */
/*  <div class="control"> */
/*  <input name="password" type="password" autocomplete="off" class="input-text" id="pass" title="Password" placeholder="Password" /> */
/*  </div> */
/*  </div> */
/*  {% if setting.so_sociallogin_enable %} */
/*  <div class=" form-group"> */
/*  <label class="control-label">{{ text_title_login_with_social }}</label> */
/*  <div> */
/*  {% if setting.so_sociallogin_googlestatus %} */
/*  {% if setting.so_sociallogin_button == 'icon' %} */
/*  <a href="{{ googlelink }}" class="btn btn-social-icon btn-sm btn-google-plus"><i class="fa fa-google fa-fw" aria-hidden="true"></i></a> */
/*  {% else %} */
/*  <a class="social-icon" href="{{ googlelink }}"> */
/*  <img class="img-responsive" src="{{ googleimage }}" */
/*  data-toggle="tooltip" alt="{{ setting.so_sociallogin_googletitle }}" */
/*  title="{{ setting.so_sociallogin_googletitle }}" */
/*  /> */
/*  </a> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% if setting.so_sociallogin_fbstatus %} */
/*  {% if setting.so_sociallogin_button == 'icon' %} */
/*  <a href="{{ fblink }}" class="btn btn-social-icon btn-sm btn-facebook"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a> */
/*  {% else %} */
/*  <a href="{{ fblink }}" class="social-icon"> */
/*  <img class="img-responsive" src="{{ fbimage }}" */
/*  data-toggle="tooltip" alt="{{ setting.so_sociallogin_fbtitle }}" */
/*  title="{{ setting.so_sociallogin_fbtitle }}" */
/*  /> */
/*  </a> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% if setting.so_sociallogin_twitstatus %} */
/*  {% if setting.so_sociallogin_button == 'icon' %} */
/*  <a href="{{ twitlink }}" class="btn btn-social-icon btn-sm btn-twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a> */
/*  {% else %} */
/*  <a class="social-icon" href="{{ twitlink }}"> */
/*  <img class="img-responsive" src="{{ twitimage }}" */
/*  data-toggle="tooltip" alt="{{ setting.so_sociallogin_twittertitle }}" */
/*  title="{{ setting.so_sociallogin_twittertitle }}" */
/*  /> */
/*  </a> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% if setting.so_sociallogin_linkstatus %} */
/*  {% if setting.so_sociallogin_button == 'icon' %} */
/*  <a href="{{ linkdinlink }}" class="btn btn-social-icon btn-sm btn-linkdin"><i class="fa fa-linkedin fa-fw" aria-hidden="true"></i></a> */
/*  {% else %} */
/*  <a class="social-icon" href="{{ linkdinlink }}"> */
/*  <img class="img-responsive" src="{{ linkdinimage }}" */
/*  data-toggle="tooltip" alt="{{ setting.so_sociallogin_linkedintitle }}" */
/*  title="{{ setting.so_sociallogin_linkedintitle }}" */
/*  /> */
/*  </a> */
/*  {% endif %} */
/*  {% endif %} */
/*  </div> */
/*  </div> */
/*  {% endif %} */
/*  <div class="secondary ft-link-p"><a class="action remind" href="{{ link_forgot_password }}"><span>{{ text_forgot_password }}</span></a></div> */
/*  <div class="actions-toolbar"> */
/*  <div class="primary"><button type="submit" class="action login primary" name="send" id="send2"><span>{{ text_login }}</span></button></div> */
/*  </div> */
/*  </fieldset> */
/*  </form> */
/*  </div> */
/*  </div> */
/*  <div class="col-reg login-customer"> */
/*  {{ text_colregister }} */
/*  <a class="btn-reg-popup" title="{{ text_register }}" href="{{ register }}">{{ text_create_account }}</a> */
/*  </div> */
/*  <div style="clear:both;"></div> */
/*  </div> */
/*  </div> */
/*  </div> */
/*  <script type="text/javascript"> */
/*  jQuery(document).ready(function($) { */
/*  var $window = $(window); */
/*  function checkWidth() { */
/*  var windowsize = $window.width(); */
/*  if (windowsize > 767) { */
/*  $('a[href*="account/login"]').click(function (e) { */
/*  e.preventDefault(); */
/*  $("#so_sociallogin").modal('show'); */
/*  }); */
/*  } */
/*  } */
/*  checkWidth(); */
/*  $(window).resize(checkWidth); */
/*  }); */
/*  </script> */
/*  {% endif %} */
/*  */
/* */
