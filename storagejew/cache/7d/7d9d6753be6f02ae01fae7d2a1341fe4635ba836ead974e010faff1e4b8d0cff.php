<?php

/* so-claue/template/extension/module/so_megamenu/default.twig */
class __TwigTemplate_7474171ccd1cf319c84ccfdb2acf8037dac115de5701685dd867d39e656dad8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["suffix_id"] = ("so_megamenu_" . twig_random($this->env, 100));
        // line 2
        $context["show_verticalmenu_id"] = ("show-verticalmenu-" . twig_random($this->env, 100));
        // line 3
        $context["remove_verticalmenu_id"] = ("remove-verticalmenu-" . twig_random($this->env, 100));
        // line 4
        $context["show_megamenu_id"] = ("show-megamenu-" . twig_random($this->env, 100));
        // line 5
        $context["remove_megamenu_id"] = ("remove-megamenu-" . twig_random($this->env, 100));
        // line 6
        echo "

<div id=\"";
        // line 8
        echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
        echo "\" class=\"responsive megamenu-style-dev\">
\t";
        // line 9
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 10
            echo "\t<div class=\"so-vertical-menu no-gutter\">
\t";
        }
        // line 12
        echo "\t
\t";
        // line 13
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "disp_title_module", array()) && (isset($context["head_name"]) ? $context["head_name"] : null))) {
            // line 14
            echo "\t\t<h3>";
            echo (isset($context["head_name"]) ? $context["head_name"] : null);
            echo "</h3>
\t";
        }
        // line 16
        echo "\t<nav class=\"navbar-default\">
\t\t<div class=\" container-megamenu ";
        // line 17
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "full_width", array()) != 1)) {
            echo " ";
            echo "container";
            echo " ";
        }
        echo " ";
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            echo " ";
            echo "vertical ";
            echo " ";
        } else {
            echo " ";
            echo "horizontal";
            echo " ";
        }
        echo "\">
\t\t";
        // line 18
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 19
            echo "\t\t\t<div id=\"menuHeading\">
\t\t\t\t<div class=\"megamenuToogle-wrapper\">
\t\t\t\t\t<div class=\"megamenuToogle-pattern\">
\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t<div><span></span><span></span><span></span></div>
\t\t\t\t\t\t\t<b>";
            // line 24
            echo (isset($context["navigation_text"]) ? $context["navigation_text"] : null);
            echo "</b>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"navbar-header\">
\t\t\t\t<button type=\"button\" id=\"";
            // line 30
            echo (isset($context["show_verticalmenu_id"]) ? $context["show_verticalmenu_id"] : null);
            echo "\" data-toggle=\"collapse\"  class=\"navbar-toggle\">
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>

\t\t\t\t</button>
\t\t\t</div>
\t\t";
        } else {
            // line 38
            echo "\t\t\t<div class=\"navbar-header\">
\t\t\t\t<button type=\"button\" id=\"";
            // line 39
            echo (isset($context["show_megamenu_id"]) ? $context["show_megamenu_id"] : null);
            echo "\" data-toggle=\"collapse\"  class=\"navbar-toggle\">
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t</button>
\t\t\t</div>
\t\t";
        }
        // line 46
        echo "
\t\t";
        // line 47
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 48
            echo "\t\t\t<div class=\"vertical-wrapper\">
\t\t";
        } else {
            // line 50
            echo "\t\t\t<div class=\"megamenu-wrapper\">
\t\t";
        }
        // line 52
        echo "
\t\t";
        // line 53
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 54
            echo "\t\t\t<span id=\"";
            echo (isset($context["remove_verticalmenu_id"]) ? $context["remove_verticalmenu_id"] : null);
            echo "\" class=\"remove-verticalmenu pe-7s-close icon-close\"></span>
\t\t";
        } else {
            // line 56
            echo "\t\t\t<span id=\"";
            echo (isset($context["remove_megamenu_id"]) ? $context["remove_megamenu_id"] : null);
            echo "\" class=\"pe-7s-close icon-close\"></span>
\t\t";
        }
        // line 58
        echo "
\t\t\t<div class=\"megamenu-pattern\">\t\t\t
\t\t\t\t<ul class=\"megamenu\"
\t\t\t\tdata-transition=\"";
        // line 61
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "animation", array()) != "")) {
            echo $this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "animation", array());
        } else {
            echo "none";
        }
        echo "\" data-animationtime=\"";
        if ((($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "animation_time", array()) > 0) && ($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "animation_time", array()) < 5000))) {
            echo $this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "animation_time", array());
        } else {
            echo 500;
        }
        echo "\">
\t\t\t\t\t";
        // line 62
        if ((($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "home_item", array()) == "icon") || ($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "home_item", array()) == "text"))) {
            // line 63
            echo "\t\t\t\t\t\t<li class=\"home\">
\t\t\t\t\t\t\t<a href=\"";
            // line 64
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">
\t\t\t\t\t\t\t";
            // line 65
            if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "home_item", array()) == "icon")) {
                // line 66
                echo "\t\t\t\t\t\t\t\t<i class=\"fa fa-home\"></i>
\t\t\t\t\t\t\t";
            } else {
                // line 68
                echo "\t\t\t\t\t\t\t\t<span><strong>";
                echo (isset($context["home_text"]) ? $context["home_text"] : null);
                echo "</strong></span>
\t\t\t\t\t\t\t";
            }
            // line 70
            echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
        }
        // line 73
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu"]) ? $context["menu"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["row"]) {
            // line 75
            echo "\t\t\t\t\t\t";
            $context["class"] = "";
            // line 76
            echo "\t\t\t\t\t\t";
            $context["icon_font"] = "";
            // line 77
            echo "\t\t\t\t\t\t";
            $context["class_link"] = "clearfix";
            // line 78
            echo "\t\t\t\t\t\t";
            $context["label_item"] = "";
            // line 79
            echo "\t\t\t\t\t\t";
            $context["title"] = false;
            // line 80
            echo "\t\t\t\t\t\t";
            $context["target"] = false;
            // line 81
            echo "
\t\t\t\t\t\t";
            // line 82
            if (twig_in_filter("no_image", $this->getAttribute($context["row"], "icon", array()))) {
                // line 83
                echo "\t\t\t\t\t\t\t";
                $context["icon"] = "";
                // line 84
                echo "\t\t\t\t\t\t";
            } else {
                // line 85
                echo "\t\t\t\t\t\t\t";
                $context["icon"] = $this->getAttribute($context["row"], "icon", array());
                // line 86
                echo "\t\t\t\t\t\t";
            }
            // line 87
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 88
            if (($this->getAttribute($context["row"], "description", array()) != "")) {
                // line 89
                echo "\t\t\t\t\t\t\t";
                $context["class_link"] = ((isset($context["class_link"]) ? $context["class_link"] : null) . " description");
                // line 90
                echo "\t\t\t\t\t\t";
            }
            // line 91
            echo "
\t\t\t\t\t\t";
            // line 92
            if (((((isset($context["route"]) ? $context["route"] : null) && ((isset($context["route"]) ? $context["route"] : null) == $this->getAttribute($context["row"], "route", array()))) && (isset($context["path"]) ? $context["path"] : null)) && ((isset($context["path"]) ? $context["path"] : null) == $this->getAttribute($context["row"], "path", array())))) {
                // line 93
                echo "\t\t\t\t\t\t\t";
                $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . " active_menu");
                // line 94
                echo "\t\t\t\t\t\t";
            }
            // line 95
            echo "
\t\t\t\t\t\t";
            // line 96
            if ($this->getAttribute($context["row"], "class_menu", array())) {
                // line 97
                echo "\t\t\t\t\t\t\t";
                $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . $this->getAttribute($context["row"], "class_menu", array()));
                // line 98
                echo "\t\t\t\t\t\t";
            }
            // line 99
            echo "
\t\t\t\t\t\t";
            // line 100
            if ($this->getAttribute($context["row"], "icon_font", array())) {
                // line 101
                echo "\t\t\t\t\t\t\t";
                $context["icon_font"] = ((((isset($context["icon_font"]) ? $context["icon_font"] : null) . "<i class=\"") . $this->getAttribute($context["row"], "icon_font", array())) . "\"></i>");
                // line 102
                echo "\t\t\t\t\t\t";
            }
            // line 103
            echo "
\t\t\t\t\t\t";
            // line 104
            if ($this->getAttribute($context["row"], "label_item", array())) {
                // line 105
                echo "\t\t\t\t\t\t\t";
                $context["label_item"] = ((((isset($context["label_item"]) ? $context["label_item"] : null) . "<span class=\"label") . $this->getAttribute($context["row"], "label_item", array())) . "\"></span>");
                // line 106
                echo "\t\t\t\t\t\t";
            }
            // line 107
            echo "
\t\t\t\t\t\t";
            // line 108
            if ((twig_test_iterable($this->getAttribute($context["row"], "submenu", array())) && $this->getAttribute($context["row"], "submenu", array()))) {
                // line 109
                echo "\t\t\t\t\t\t\t";
                $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . " with-sub-menu");
                // line 110
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["row"], "submenu_type", array()) == 1)) {
                    // line 111
                    echo "\t\t\t\t\t\t\t\t";
                    $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . " click");
                    // line 112
                    echo "\t\t\t\t\t\t\t";
                } else {
                    // line 113
                    echo "\t\t\t\t\t\t\t\t";
                    $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . " hover");
                    // line 114
                    echo "\t\t\t\t\t\t\t";
                }
                // line 115
                echo "\t\t\t\t\t\t";
            }
            // line 116
            echo "
\t\t\t\t\t\t";
            // line 117
            if (($this->getAttribute($context["row"], "position", array()) == 1)) {
                // line 118
                echo "\t\t\t\t\t\t\t";
                $context["class"] = ((isset($context["class"]) ? $context["class"] : null) . " pull-right");
                // line 119
                echo "\t\t\t\t\t\t";
            }
            // line 120
            echo "
\t\t\t\t\t\t";
            // line 121
            if (($this->getAttribute($context["row"], "submenu_type", array()) == 2)) {
                // line 122
                echo "\t\t\t\t\t\t\t";
                $context["title"] = "title=\"hover-intent\"";
                // line 123
                echo "\t\t\t\t\t\t";
            }
            // line 124
            echo "
\t\t\t\t\t\t";
            // line 125
            if (($this->getAttribute($context["row"], "new_window", array()) == 1)) {
                // line 126
                echo "\t\t\t\t\t\t\t";
                $context["target"] = "target=\"_blank\"";
                // line 127
                echo "\t\t\t\t\t\t";
            }
            // line 128
            echo "
\t\t\t\t\t\t";
            // line 129
            if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
                // line 130
                echo "\t\t\t\t\t\t\t<li class=\"item-vertical ";
                echo (isset($context["class"]) ? $context["class"] : null);
                echo "\" ";
                echo (isset($context["title"]) ? $context["title"] : null);
                echo ">
\t\t\t\t\t\t\t\t<p class='close-menu'></p>
\t\t\t\t\t\t\t\t";
                // line 132
                if ((twig_test_iterable($this->getAttribute($context["row"], "submenu", array())) && $this->getAttribute($context["row"], "submenu", array()))) {
                    // line 133
                    echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["row"], "link", array());
                    echo "\" class=\"";
                    echo (isset($context["class_link"]) ? $context["class_link"] : null);
                    echo "\" ";
                    echo (isset($context["target"]) ? $context["target"] : null);
                    echo ">
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t<strong>";
                    // line 135
                    echo ((((isset($context["icon_font"]) ? $context["icon_font"] : null) . (isset($context["icon"]) ? $context["icon"] : null)) . $this->getAttribute($this->getAttribute($context["row"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) . $this->getAttribute($context["row"], "description", array()));
                    echo "</strong>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                    // line 137
                    echo (isset($context["label_item"]) ? $context["label_item"] : null);
                    echo "
\t\t\t\t\t\t\t\t\t\t<b class='fa fa-angle-right' ></b>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 141
                    echo "\t\t\t\t\t\t\t \t\t<a href=\"";
                    echo $this->getAttribute($context["row"], "link", array());
                    echo "\" class=\"";
                    echo (isset($context["class_link"]) ? $context["class_link"] : null);
                    echo "\" ";
                    echo (isset($context["target"]) ? $context["target"] : null);
                    echo ">
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t<strong>";
                    // line 143
                    echo ((((isset($context["icon_font"]) ? $context["icon_font"] : null) . (isset($context["icon"]) ? $context["icon"] : null)) . $this->getAttribute($this->getAttribute($context["row"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) . $this->getAttribute($context["row"], "description", array()));
                    echo "</strong>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                    // line 145
                    echo (isset($context["label_item"]) ? $context["label_item"] : null);
                    echo "
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                }
                // line 148
                echo "
\t\t\t\t\t\t\t\t";
                // line 149
                if ((twig_test_iterable($this->getAttribute($context["row"], "submenu", array())) && $this->getAttribute($context["row"], "submenu", array()))) {
                    // line 150
                    echo "\t\t\t\t\t\t\t\t\t";
                    if (twig_in_filter("%", $this->getAttribute($context["row"], "submenu_width", array()))) {
                        // line 151
                        echo "\t\t\t\t\t\t\t\t\t\t<div class=\"sub-menu\" data-subwidth =\"";
                        echo twig_replace_filter($this->getAttribute($context["row"], "submenu_width", array()), array("%" => ""));
                        echo "\">
\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 153
                        echo "\t\t\t\t\t\t\t\t\t\t<div class=\"sub-menu\" style=\"width:";
                        echo $this->getAttribute($context["row"], "submenu_width", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 155
                    echo "
\t\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 158
                    $context["row_fluid"] = 0;
                    // line 159
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["row"], "submenu", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                        // line 160
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((((isset($context["row_fluid"]) ? $context["row_fluid"] : null) + $this->getAttribute($context["submenu"], "content_width", array())) > 12)) {
                            // line 161
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["row_fluid"] = $this->getAttribute($context["submenu"], "content_width", array());
                            // line 162
                            echo "\t\t\t\t\t\t\t\t\t \t\t\t\t</div><div class=\"border\"></div><div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 164
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["row_fluid"] = ((isset($context["row_fluid"]) ? $context["row_fluid"] : null) + $this->getAttribute($context["submenu"], "content_width", array()));
                            // line 165
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 166
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-";
                        echo $this->getAttribute($context["submenu"], "content_width", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 167
                        if (($this->getAttribute($context["submenu"], "content_type", array()) == 0)) {
                            // line 168
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"html ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">";
                            echo $this->getAttribute($context["submenu"], "html", array());
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 169
$context["submenu"], "content_type", array()) == 1)) {
                            // line 170
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable($this->getAttribute($context["submenu"], "product", array()))) {
                                // line 171
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"product ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\"><a href=\"";
                                // line 172
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "\" onclick=\"window.location = '";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "'\"><img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "image", array());
                                echo "\" alt=\"\"></a></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"name\"><a href=\"";
                                // line 173
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "\" onclick=\"window.location = '";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "'\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "name", array());
                                echo "</a></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 175
                                if ( !$this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "special", array())) {
                                    // line 176
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "price", array());
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 178
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "special", array());
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 180
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 183
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 2)) {
                            // line 184
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"categories ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">";
                            echo $this->getAttribute($context["submenu"], "categories", array());
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 185
$context["submenu"], "content_type", array()) == 3)) {
                            // line 186
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable($this->getAttribute($context["submenu"], "manufactures", array()))) {
                                // line 187
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"manufacturer ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 188
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["submenu"], "manufactures", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                                    // line 189
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                                    echo $this->getAttribute($context["manufacturer"], "link", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["manufacturer"], "name", array());
                                    echo "</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 191
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 193
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 4)) {
                            // line 194
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($this->getAttribute($context["submenu"], "images", array()), "show_title", array()) == 1)) {
                                // line 195
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title-submenu\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 197
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"link ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 198
                            echo $this->getAttribute($this->getAttribute($context["submenu"], "images", array()), "link", array());
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 200
$context["submenu"], "content_type", array()) == 5)) {
                            // line 201
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "categories", array())) {
                                // line 202
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"subcategory ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 203
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "categories", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["categories"]) {
                                    // line 204
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 205
                                    if (($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "show_title", array()) == 1)) {
                                        // line 206
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                                        echo $this->getAttribute($context["categories"], "href", array());
                                        echo "\" class=\"title-submenu ";
                                        echo $this->getAttribute($context["submenu"], "class_menu", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["categories"], "name", array());
                                        echo "</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 208
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if ($this->getAttribute($context["categories"], "categories", array())) {
                                        // line 209
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        echo $this->getAttribute($context["categories"], "categories", array());
                                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 211
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if (($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "show_image", array()) == 1)) {
                                        // line 212
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                        echo $this->getAttribute($context["categories"], "thumb", array());
                                        echo "\" alt=\"\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 214
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categories'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 216
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 218
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 6)) {
                            // line 219
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "show_title", array()) == 1)) {
                                // line 220
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title-submenu\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 222
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "products", array())) {
                                // line 223
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "products", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                                    // line 224
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    $context["itempage"] = (($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "col", array())) ? ((12 / $this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "col", array()))) : (4));
                                    // line 225
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-xs-";
                                    echo (isset($context["itempage"]) ? $context["itempage"] : null);
                                    echo " ";
                                    echo $this->getAttribute($context["submenu"], "class_menu", array());
                                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-thumb\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                                    // line 228
                                    echo $this->getAttribute($context["product"], "href", array());
                                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                    // line 229
                                    echo $this->getAttribute($context["product"], "thumb", array());
                                    echo "\" alt=\"";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "\" title=\"";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "\"  />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4><a href=\"";
                                    // line 234
                                    echo $this->getAttribute($context["product"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "</a></h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>";
                                    // line 235
                                    echo $this->getAttribute($context["product"], "description", array());
                                    echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 236
                                    if ($this->getAttribute($context["product"], "rating", array())) {
                                        // line 237
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 238
                                        $context['_parent'] = $context;
                                        $context['_seq'] = twig_ensure_traversable(range(1, 5));
                                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                            // line 239
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                                                // line 240
                                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            } else {
                                                // line 242
                                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            }
                                            // line 244
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        }
                                        $_parent = $context['_parent'];
                                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                                        $context = array_intersect_key($context, $_parent) + $_parent;
                                        // line 245
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 247
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 248
                                    if ($this->getAttribute($context["product"], "price", array())) {
                                        // line 249
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 250
                                        if ( !$this->getAttribute($context["product"], "special", array())) {
                                            // line 251
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            echo $this->getAttribute($context["product"], "price", array());
                                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t\t";
                                        } else {
                                            // line 253
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                                            echo $this->getAttribute($context["product"], "special", array());
                                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                                            // line 254
                                            echo $this->getAttribute($context["product"], "price", array());
                                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t\t";
                                        }
                                        // line 256
                                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 260
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  \t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  \t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t \t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 265
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 266
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 269
                    echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t";
                }
                // line 273
                echo "\t\t\t\t\t\t\t</li>\t\t\t\t\t\t\t
\t\t\t\t\t\t";
            } else {
                // line 274
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t\t<li class=\"";
                // line 275
                echo (isset($context["class"]) ? $context["class"] : null);
                echo "\" ";
                echo (isset($context["title"]) ? $context["title"] : null);
                echo ">
\t\t\t\t\t\t\t\t<p class='close-menu'></p>
\t\t\t\t\t\t\t\t";
                // line 277
                if ((twig_test_iterable($this->getAttribute($context["row"], "submenu", array())) && $this->getAttribute($context["row"], "submenu", array()))) {
                    // line 278
                    echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["row"], "link", array());
                    echo "\" class=\"";
                    echo (isset($context["class_link"]) ? $context["class_link"] : null);
                    echo "\" ";
                    echo (isset($context["target"]) ? $context["target"] : null);
                    echo ">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 281
                    echo ((((isset($context["icon_font"]) ? $context["icon_font"] : null) . (isset($context["icon"]) ? $context["icon"] : null)) . $this->getAttribute($this->getAttribute($context["row"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) . $this->getAttribute($context["row"], "description", array()));
                    echo "
\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t";
                    // line 283
                    echo (isset($context["label_item"]) ? $context["label_item"] : null);
                    echo "
\t\t\t\t\t\t\t\t\t\t<b class='caret'></b>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 287
                    echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["row"], "link", array());
                    echo "\" class=\"";
                    echo (isset($context["class_link"]) ? $context["class_link"] : null);
                    echo "\" ";
                    echo (isset($context["target"]) ? $context["target"] : null);
                    echo ">
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 289
                    echo ((((isset($context["icon_font"]) ? $context["icon_font"] : null) . (isset($context["icon"]) ? $context["icon"] : null)) . $this->getAttribute($this->getAttribute($context["row"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) . $this->getAttribute($context["row"], "description", array()));
                    echo "
\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t";
                    // line 291
                    echo (isset($context["label_item"]) ? $context["label_item"] : null);
                    echo "
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                }
                // line 294
                echo "
\t\t\t\t\t\t\t\t";
                // line 295
                if ((twig_test_iterable($this->getAttribute($context["row"], "submenu", array())) && $this->getAttribute($context["row"], "submenu", array()))) {
                    // line 296
                    echo "\t\t\t\t\t\t\t\t\t<div class=\"sub-menu\" style=\"width: ";
                    echo $this->getAttribute($context["row"], "submenu_width", array());
                    echo "\">
\t\t\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 299
                    $context["row_fluid"] = 0;
                    // line 300
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["row"], "submenu", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                        // line 301
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((((isset($context["row_fluid"]) ? $context["row_fluid"] : null) + $this->getAttribute($context["submenu"], "content_width", array())) > 12)) {
                            // line 302
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["row_fluid"] = $this->getAttribute($context["submenu"], "content_width", array());
                            // line 303
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div><div class=\"border\"></div><div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 305
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["row_fluid"] = ((isset($context["row_fluid"]) ? $context["row_fluid"] : null) + $this->getAttribute($context["submenu"], "content_width", array()));
                            // line 306
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 307
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-";
                        echo $this->getAttribute($context["submenu"], "content_width", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 308
                        if (($this->getAttribute($context["submenu"], "content_type", array()) == 0)) {
                            // line 309
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"html ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 310
                            echo $this->getAttribute($context["submenu"], "html", array());
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 312
$context["submenu"], "content_type", array()) == 1)) {
                            // line 313
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable($this->getAttribute($context["submenu"], "product", array()))) {
                                // line 314
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"product ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\"><a href=\"";
                                // line 315
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "\" onclick=\"window.location = '";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "'\"><img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "image", array());
                                echo "\" alt=\"\"></a></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"name\"><a href=\"";
                                // line 316
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "\" onclick=\"window.location = '";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "link", array());
                                echo "'\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "name", array());
                                echo "</a></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 318
                                if ( !$this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "special", array())) {
                                    // line 319
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "price", array());
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 321
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    echo $this->getAttribute($this->getAttribute($context["submenu"], "product", array()), "special", array());
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 323
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 326
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 2)) {
                            // line 327
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"categories ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 328
                            echo $this->getAttribute($context["submenu"], "categories", array());
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 330
$context["submenu"], "content_type", array()) == 3)) {
                            // line 331
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (twig_test_iterable($this->getAttribute($context["submenu"], "manufactures", array()))) {
                                // line 332
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"manufacturer ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 333
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["submenu"], "manufactures", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                                    // line 334
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                                    echo $this->getAttribute($context["manufacturer"], "link", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["manufacturer"], "name", array());
                                    echo "</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 336
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 338
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 4)) {
                            // line 339
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($this->getAttribute($context["submenu"], "images", array()), "show_title", array()) == 1)) {
                                // line 340
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title-submenu\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 342
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"link ";
                            echo $this->getAttribute($context["submenu"], "class_menu", array());
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 343
                            echo $this->getAttribute($this->getAttribute($context["submenu"], "images", array()), "link", array());
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute(                        // line 345
$context["submenu"], "content_type", array()) == 5)) {
                            // line 346
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "categories", array())) {
                                // line 347
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"subcategory ";
                                echo $this->getAttribute($context["submenu"], "class_menu", array());
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 348
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "categories", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["categories"]) {
                                    // line 349
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 350
                                    if (($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "show_title", array()) == 1)) {
                                        // line 351
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                                        echo $this->getAttribute($context["categories"], "href", array());
                                        echo "\" class=\"title-submenu ";
                                        echo $this->getAttribute($context["submenu"], "class_menu", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["categories"], "name", array());
                                        echo "</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 353
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if ($this->getAttribute($context["categories"], "categories", array())) {
                                        // line 354
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        echo $this->getAttribute($context["categories"], "categories", array());
                                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 356
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if (($this->getAttribute($this->getAttribute($context["submenu"], "subcategory", array()), "show_image", array()) == 1)) {
                                        // line 357
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                        echo $this->getAttribute($context["categories"], "thumb", array());
                                        echo "\" alt=\"\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 359
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categories'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 361
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 363
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } elseif (($this->getAttribute($context["submenu"], "content_type", array()) == 6)) {
                            // line 364
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "show_title", array()) == 1)) {
                                // line 365
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title-submenu\">";
                                echo $this->getAttribute($this->getAttribute($context["submenu"], "name", array()), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 367
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "products", array())) {
                                // line 368
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "products", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                                    // line 369
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    $context["itempage"] = (($this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "col", array())) ? ((12 / $this->getAttribute($this->getAttribute($context["submenu"], "productlist", array()), "col", array()))) : (4));
                                    // line 370
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-";
                                    echo (isset($context["itempage"]) ? $context["itempage"] : null);
                                    echo " ";
                                    echo $this->getAttribute($context["submenu"], "class_menu", array());
                                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-thumb\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                                    // line 373
                                    echo $this->getAttribute($context["product"], "href", array());
                                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                                    // line 374
                                    echo $this->getAttribute($context["product"], "thumb", array());
                                    echo "\" alt=\"";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "\" title=\"";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4><a href=\"";
                                    // line 379
                                    echo $this->getAttribute($context["product"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["product"], "name", array());
                                    echo "</a></h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>";
                                    // line 380
                                    echo $this->getAttribute($context["product"], "description", array());
                                    echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 381
                                    if ($this->getAttribute($context["product"], "rating", array())) {
                                        // line 382
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 383
                                        $context['_parent'] = $context;
                                        $context['_seq'] = twig_ensure_traversable(range(1, 5));
                                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                            // line 384
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                                                // line 385
                                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            } else {
                                                // line 387
                                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            }
                                            // line 389
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        }
                                        $_parent = $context['_parent'];
                                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                                        $context = array_intersect_key($context, $_parent) + $_parent;
                                        // line 390
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 392
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 393
                                    if ($this->getAttribute($context["product"], "price", array())) {
                                        // line 394
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        // line 395
                                        if ( !$this->getAttribute($context["product"], "special", array())) {
                                            // line 396
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                            echo $this->getAttribute($context["product"], "price", array());
                                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t";
                                        } else {
                                            // line 398
                                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                                            echo $this->getAttribute($context["product"], "special", array());
                                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                                            // line 399
                                            echo $this->getAttribute($context["product"], "price", array());
                                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t";
                                        }
                                        // line 401
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   \t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 405
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  \t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t  \t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t \t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 410
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 411
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 412
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 414
                    echo "\t\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                }
                // line 418
                echo "\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
            }
            // line 420
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 421
        echo "\t\t\t\t</ul>\t\t
\t\t\t</div>
\t\t</div>
\t\t</div>
\t</nav>
\t";
        // line 426
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 427
            echo "\t\t</div>
\t";
        }
        // line 429
        echo "</div>

";
        // line 431
        if (($this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "orientation", array()) == 1)) {
            // line 432
            echo "<script type=\"text/javascript\">
\t\$(document).ready(function() {

\t\t(function (element) {
\t\t\tvar \$element = \$(element);
\t\t\tvar itemver =  ";
            // line 437
            echo $this->getAttribute((isset($context["ustawienia"]) ? $context["ustawienia"] : null), "show_itemver", array());
            echo ";
\t\t\t
\t\t\t\$(\"ul.megamenu  li.item-vertical\", \$element ).addClass('demo');
\t\t\t
\t\t\tif(itemver <= \$( \".vertical ul.megamenu >li\", \$element ).length){
\t\t\t\t\$('.vertical ul.megamenu', \$element).append('<li class=\"loadmore\"><i class=\"fa fa-plus-square-o\"></i><span class=\"more-view\"> ";
            // line 442
            echo (isset($context["text_more_category"]) ? $context["text_more_category"] : null);
            echo "</span></li>');
\t\t\t\t\$('.horizontal ul.megamenu li.loadmore', \$element).remove();
\t\t\t}
\t\t\t
\t\t\tvar show_itemver = itemver-1 ;
\t\t\t\$('ul.megamenu > li.item-vertical', \$element).each(function(i){
\t\t\t\tif(i>show_itemver){
\t\t\t\t\t\t\$(this).css('display', 'none');
\t\t\t\t} 
\t\t\t});
\t\t\t
\t\t\t\$(\".megamenu .loadmore\", \$element).click(function(){
\t\t\t\tif(\$(this).hasClass('open')){
\t\t\t\t\t\$('ul.megamenu li.item-vertical', \$element).each(function(i){
\t\t\t\t\t\t\tif(i>show_itemver){
\t\t\t\t\t\t\t\t\t\$(this).slideUp(200);
\t\t\t\t\t\t\t\t\t\$(this).css('display', 'none');
\t\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t\t\$(this).removeClass('open');
\t\t\t\t\t\$('.loadmore', \$element).html('<i class=\"fa fa-plus\"></i><span class=\"more-view\">";
            // line 463
            echo (isset($context["text_more_category"]) ? $context["text_more_category"] : null);
            echo "</span>');
\t\t\t\t}else{
\t\t\t\t\t\$('ul.megamenu li.item-vertical', \$element).each(function(i){
\t\t\t\t\t\t\tif(i>show_itemver){
\t\t\t\t\t\t\t\t\t\$(this).slideDown(200);
\t\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t\t\$(this).addClass('open');
\t\t\t\t\t\$('.loadmore', \$element).html('<i class=\"fa fa-minus\"></i><span class=\"more-view\">";
            // line 471
            echo (isset($context["text_more_category"]) ? $context["text_more_category"] : null);
            echo "</span>');
\t\t\t\t}
\t\t\t});
\t\t})(\"#";
            // line 474
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo "\");


\t

\t    \$(\"#";
            // line 479
            echo (isset($context["show_verticalmenu_id"]) ? $context["show_verticalmenu_id"] : null);
            echo "\").click(function () {
\t\t\tif(\$('#";
            // line 480
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .vertical-wrapper').hasClass('so-vertical-active'))
\t\t\t\t\$('#";
            // line 481
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .vertical-wrapper').removeClass('so-vertical-active');
\t\t\telse
\t\t\t\t\$('#";
            // line 483
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .vertical-wrapper').addClass('so-vertical-active');
\t\t}); 
\t\t
\t\t\$('#";
            // line 486
            echo (isset($context["remove_verticalmenu_id"]) ? $context["remove_verticalmenu_id"] : null);
            echo "').click(function() {
\t        \$('#";
            // line 487
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .vertical-wrapper').removeClass('so-vertical-active');
\t        return false;
\t    });\t
\t});
</script>
";
        } else {
            // line 493
            echo "<script>
\$(document).ready(function(){
\t\$(\"#";
            // line 495
            echo (isset($context["show_megamenu_id"]) ? $context["show_megamenu_id"] : null);
            echo "\").click(function () {
\t\tif(\$('#";
            // line 496
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .megamenu-wrapper',).hasClass('so-megamenu-active'))
\t\t\t\$('#";
            // line 497
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .megamenu-wrapper').removeClass('so-megamenu-active');
\t\telse
\t\t\t\$('#";
            // line 499
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .megamenu-wrapper').addClass('so-megamenu-active');
\t}); 
\t\$(\"#";
            // line 501
            echo (isset($context["remove_megamenu_id"]) ? $context["remove_megamenu_id"] : null);
            echo "\").click(function() {
        \$('#";
            // line 502
            echo (isset($context["suffix_id"]) ? $context["suffix_id"] : null);
            echo " .megamenu-wrapper').removeClass('so-megamenu-active');
        return false;
    });\t\t
\t
});
</script>

";
        }
        // line 510
        echo "<script>
\$(document).ready(function(){
\t\$('a[href=\"";
        // line 512
        echo (isset($context["actual_link"]) ? $context["actual_link"] : null);
        echo "\"]').each(function() {
\t\t\$(this).parents('.with-sub-menu').addClass('sub-active');
\t});  
});
</script>";
    }

    public function getTemplateName()
    {
        return "so-claue/template/extension/module/so_megamenu/default.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1377 => 512,  1373 => 510,  1362 => 502,  1358 => 501,  1353 => 499,  1348 => 497,  1344 => 496,  1340 => 495,  1336 => 493,  1327 => 487,  1323 => 486,  1317 => 483,  1312 => 481,  1308 => 480,  1304 => 479,  1296 => 474,  1290 => 471,  1279 => 463,  1255 => 442,  1247 => 437,  1240 => 432,  1238 => 431,  1234 => 429,  1230 => 427,  1228 => 426,  1221 => 421,  1215 => 420,  1211 => 418,  1205 => 414,  1198 => 412,  1195 => 411,  1192 => 410,  1182 => 405,  1176 => 401,  1171 => 399,  1166 => 398,  1160 => 396,  1158 => 395,  1155 => 394,  1153 => 393,  1150 => 392,  1146 => 390,  1140 => 389,  1136 => 387,  1132 => 385,  1129 => 384,  1125 => 383,  1122 => 382,  1120 => 381,  1116 => 380,  1110 => 379,  1098 => 374,  1094 => 373,  1085 => 370,  1082 => 369,  1077 => 368,  1074 => 367,  1068 => 365,  1065 => 364,  1062 => 363,  1058 => 361,  1051 => 359,  1045 => 357,  1042 => 356,  1036 => 354,  1033 => 353,  1023 => 351,  1021 => 350,  1018 => 349,  1014 => 348,  1009 => 347,  1006 => 346,  1004 => 345,  999 => 343,  994 => 342,  988 => 340,  985 => 339,  982 => 338,  978 => 336,  967 => 334,  963 => 333,  958 => 332,  955 => 331,  953 => 330,  948 => 328,  943 => 327,  940 => 326,  935 => 323,  929 => 321,  923 => 319,  921 => 318,  912 => 316,  904 => 315,  899 => 314,  896 => 313,  894 => 312,  889 => 310,  884 => 309,  882 => 308,  877 => 307,  874 => 306,  871 => 305,  867 => 303,  864 => 302,  861 => 301,  856 => 300,  854 => 299,  847 => 296,  845 => 295,  842 => 294,  836 => 291,  831 => 289,  821 => 287,  814 => 283,  809 => 281,  798 => 278,  796 => 277,  789 => 275,  786 => 274,  782 => 273,  776 => 269,  766 => 266,  763 => 265,  753 => 260,  747 => 256,  742 => 254,  737 => 253,  731 => 251,  729 => 250,  726 => 249,  724 => 248,  721 => 247,  717 => 245,  711 => 244,  707 => 242,  703 => 240,  700 => 239,  696 => 238,  693 => 237,  691 => 236,  687 => 235,  681 => 234,  669 => 229,  665 => 228,  656 => 225,  653 => 224,  648 => 223,  645 => 222,  639 => 220,  636 => 219,  633 => 218,  629 => 216,  622 => 214,  616 => 212,  613 => 211,  607 => 209,  604 => 208,  594 => 206,  592 => 205,  589 => 204,  585 => 203,  580 => 202,  577 => 201,  575 => 200,  570 => 198,  565 => 197,  559 => 195,  556 => 194,  553 => 193,  549 => 191,  538 => 189,  534 => 188,  529 => 187,  526 => 186,  524 => 185,  517 => 184,  514 => 183,  509 => 180,  503 => 178,  497 => 176,  495 => 175,  486 => 173,  478 => 172,  473 => 171,  470 => 170,  468 => 169,  461 => 168,  459 => 167,  454 => 166,  451 => 165,  448 => 164,  444 => 162,  441 => 161,  438 => 160,  433 => 159,  431 => 158,  426 => 155,  420 => 153,  414 => 151,  411 => 150,  409 => 149,  406 => 148,  400 => 145,  395 => 143,  385 => 141,  378 => 137,  373 => 135,  363 => 133,  361 => 132,  353 => 130,  351 => 129,  348 => 128,  345 => 127,  342 => 126,  340 => 125,  337 => 124,  334 => 123,  331 => 122,  329 => 121,  326 => 120,  323 => 119,  320 => 118,  318 => 117,  315 => 116,  312 => 115,  309 => 114,  306 => 113,  303 => 112,  300 => 111,  297 => 110,  294 => 109,  292 => 108,  289 => 107,  286 => 106,  283 => 105,  281 => 104,  278 => 103,  275 => 102,  272 => 101,  270 => 100,  267 => 99,  264 => 98,  261 => 97,  259 => 96,  256 => 95,  253 => 94,  250 => 93,  248 => 92,  245 => 91,  242 => 90,  239 => 89,  237 => 88,  234 => 87,  231 => 86,  228 => 85,  225 => 84,  222 => 83,  220 => 82,  217 => 81,  214 => 80,  211 => 79,  208 => 78,  205 => 77,  202 => 76,  199 => 75,  195 => 74,  192 => 73,  187 => 70,  181 => 68,  177 => 66,  175 => 65,  171 => 64,  168 => 63,  166 => 62,  152 => 61,  147 => 58,  141 => 56,  135 => 54,  133 => 53,  130 => 52,  126 => 50,  122 => 48,  120 => 47,  117 => 46,  107 => 39,  104 => 38,  93 => 30,  84 => 24,  77 => 19,  75 => 18,  57 => 17,  54 => 16,  48 => 14,  46 => 13,  43 => 12,  39 => 10,  37 => 9,  33 => 8,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% set suffix_id = 'so_megamenu_' ~ random(100) %}*/
/* {% set show_verticalmenu_id = 'show-verticalmenu-' ~ random(100) %}*/
/* {% set remove_verticalmenu_id = 'remove-verticalmenu-' ~ random(100) %}*/
/* {% set show_megamenu_id = 'show-megamenu-' ~ random(100) %}*/
/* {% set remove_megamenu_id = 'remove-megamenu-' ~ random(100) %}*/
/* */
/* */
/* <div id="{{suffix_id}}" class="responsive megamenu-style-dev">*/
/* 	{% if ustawienia.orientation == 1 %}*/
/* 	<div class="so-vertical-menu no-gutter">*/
/* 	{% endif %}*/
/* 	*/
/* 	{% if ustawienia.disp_title_module and head_name %}*/
/* 		<h3>{{ head_name }}</h3>*/
/* 	{% endif %}*/
/* 	<nav class="navbar-default">*/
/* 		<div class=" container-megamenu {% if ustawienia.full_width != 1 %} {{ 'container' }} {% endif %} {% if ustawienia.orientation == 1 %} {{ 'vertical ' }} {% else %} {{ 'horizontal' }} {% endif %}">*/
/* 		{% if ustawienia.orientation == 1 %}*/
/* 			<div id="menuHeading">*/
/* 				<div class="megamenuToogle-wrapper">*/
/* 					<div class="megamenuToogle-pattern">*/
/* 						<div class="container">*/
/* 							<div><span></span><span></span><span></span></div>*/
/* 							<b>{{ navigation_text }}</b>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="navbar-header">*/
/* 				<button type="button" id="{{show_verticalmenu_id}}" data-toggle="collapse"  class="navbar-toggle">*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* */
/* 				</button>*/
/* 			</div>*/
/* 		{% else %}*/
/* 			<div class="navbar-header">*/
/* 				<button type="button" id="{{show_megamenu_id}}" data-toggle="collapse"  class="navbar-toggle">*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* 				</button>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 		{% if ustawienia.orientation == 1 %}*/
/* 			<div class="vertical-wrapper">*/
/* 		{% else %}*/
/* 			<div class="megamenu-wrapper">*/
/* 		{% endif %}*/
/* */
/* 		{% if ustawienia.orientation == 1 %}*/
/* 			<span id="{{remove_verticalmenu_id}}" class="remove-verticalmenu pe-7s-close icon-close"></span>*/
/* 		{% else %}*/
/* 			<span id="{{remove_megamenu_id}}" class="pe-7s-close icon-close"></span>*/
/* 		{% endif %}*/
/* */
/* 			<div class="megamenu-pattern">			*/
/* 				<ul class="megamenu"*/
/* 				data-transition="{% if ustawienia.animation != '' %}{{ ustawienia.animation }}{% else %}{{ 'none' }}{% endif %}" data-animationtime="{% if ustawienia.animation_time > 0 and ustawienia.animation_time < 5000 %}{{ ustawienia.animation_time }}{% else %}{{ 500 }}{% endif %}">*/
/* 					{% if ustawienia.home_item == 'icon' or ustawienia.home_item == 'text' %}*/
/* 						<li class="home">*/
/* 							<a href="{{ home }}">*/
/* 							{% if ustawienia.home_item == 'icon' %}*/
/* 								<i class="fa fa-home"></i>*/
/* 							{% else %}*/
/* 								<span><strong>{{ home_text }}</strong></span>*/
/* 							{% endif %}*/
/* 							</a>*/
/* 						</li>*/
/* 					{% endif %}*/
/* 					*/
/* 					{% for key, row in menu %}*/
/* 						{% set class 		= '' %}*/
/* 						{% set icon_font 	= '' %}*/
/* 						{% set class_link 	= 'clearfix' %}*/
/* 						{% set label_item 	= '' %}*/
/* 						{% set title 		= false %}*/
/* 						{% set target 		= false %}*/
/* */
/* 						{% if 'no_image' in row.icon %}*/
/* 							{% set icon = '' %}*/
/* 						{% else %}*/
/* 							{% set icon = row.icon %}*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if row.description != '' %}*/
/* 							{% set class_link = class_link~' description' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if route and route == row.route and path and path == row.path %}*/
/* 							{% set class = class~' active_menu' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.class_menu %}*/
/* 							{% set class = class~row.class_menu %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.icon_font %}*/
/* 							{% set icon_font = icon_font~'<i class="'~row.icon_font~'"></i>' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.label_item %}*/
/* 							{% set label_item = label_item~'<span class="label'~row.label_item~'"></span>' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.submenu is iterable and row.submenu %}*/
/* 							{% set class = class~' with-sub-menu' %}*/
/* 							{% if row.submenu_type == 1 %}*/
/* 								{% set class = class~' click' %}*/
/* 							{% else %}*/
/* 								{% set class = class~' hover' %}*/
/* 							{% endif %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.position == 1 %}*/
/* 							{% set class = class~' pull-right' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.submenu_type == 2 %}*/
/* 							{% set title = 'title="hover-intent"' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if row.new_window == 1 %}*/
/* 							{% set target = 'target="_blank"' %}*/
/* 						{% endif %}*/
/* */
/* 						{% if ustawienia.orientation == 1 %}*/
/* 							<li class="item-vertical {{ class }}" {{ title }}>*/
/* 								<p class='close-menu'></p>*/
/* 								{% if row.submenu is iterable and row.submenu %}*/
/* 									<a href="{{ row.link }}" class="{{ class_link }}" {{ target }}>*/
/* 										<span>*/
/* 											<strong>{{ icon_font~icon~row.name[lang_id]~row.description }}</strong>*/
/* 										</span>*/
/* 										{{ label_item }}*/
/* 										<b class='fa fa-angle-right' ></b>*/
/* 									</a>*/
/* 								{% else %}*/
/* 							 		<a href="{{ row.link }}" class="{{ class_link }}" {{ target }}>*/
/* 										<span>*/
/* 											<strong>{{ icon_font~icon~row.name[lang_id]~row.description }}</strong>*/
/* 										</span>*/
/* 										{{ label_item }}*/
/* 									</a>*/
/* 								{% endif %}*/
/* */
/* 								{% if row.submenu is iterable and row.submenu %}*/
/* 									{% if '%' in row.submenu_width %}*/
/* 										<div class="sub-menu" data-subwidth ="{{ row.submenu_width|replace({'%': ''}) }}">*/
/* 									{% else %}*/
/* 										<div class="sub-menu" style="width:{{ row.submenu_width }}">*/
/* 									{% endif %}*/
/* */
/* 									<div class="content">*/
/* 										<div class="row">*/
/* 											{% set row_fluid = 0 %}*/
/* 											{% for submenu in row.submenu %}*/
/* 												{% if row_fluid+submenu.content_width > 12 %}*/
/* 													{% set row_fluid = submenu.content_width %}*/
/* 									 				</div><div class="border"></div><div class="row">*/
/* 												{% else %}*/
/* 													{% set row_fluid = row_fluid+submenu.content_width %}*/
/* 												{% endif %}*/
/* 												<div class="col-sm-{{ submenu.content_width }}">*/
/* 													{% if submenu.content_type == 0 %}*/
/* 														<div class="html {{submenu.class_menu }}">{{ submenu.html }}</div>*/
/* 													{% elseif submenu.content_type == 1 %}*/
/* 														{% if submenu.product is iterable %}*/
/* 															<div class="product {{ submenu.class_menu }}">*/
/* 																<div class="image"><a href="{{ submenu.product.link }}" onclick="window.location = '{{ submenu.product.link }}'"><img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ submenu.product.image }}" alt=""></a></div>*/
/* 																<div class="name"><a href="{{ submenu.product.link }}" onclick="window.location = '{{ submenu.product.link }}'">{{ submenu.product.name }}</a></div>*/
/* 																<div class="price">*/
/* 																	{% if not submenu.product.special %}*/
/* 																		{{ submenu.product.price }}*/
/* 																	{% else %}*/
/* 																		{{ submenu.product.special }}*/
/* 																	{% endif %}*/
/* 																</div>*/
/* 															</div>*/
/* 														{% endif %}*/
/* 													{% elseif submenu.content_type == 2 %}*/
/* 														<div class="categories {{ submenu.class_menu }}">{{ submenu.categories }}</div>*/
/* 													{% elseif submenu.content_type == 3 %}*/
/* 														{% if submenu.manufactures is iterable %}*/
/* 															<ul class="manufacturer {{ submenu.class_menu }}">*/
/* 																{% for manufacturer in submenu.manufactures %}*/
/* 																	<li><a href="{{ manufacturer.link }}">{{ manufacturer.name }}</a></li>*/
/* 																{% endfor %}*/
/* 															</ul>*/
/* 														{% endif %}*/
/* 													{% elseif submenu.content_type == 4 %}*/
/* 														{% if submenu.images.show_title == 1 %}*/
/* 															<span class="title-submenu">{{ submenu.name[lang_id] }}</span>*/
/* 														{% endif %}*/
/* 														<div class="link {{ submenu.class_menu }}">*/
/* 															{{ submenu.images.link }}*/
/* 														</div>*/
/* 													{% elseif submenu.content_type == 5 %}*/
/* 														{% if submenu.subcategory.categories %}*/
/* 															<ul class="subcategory {{ submenu.class_menu }}">*/
/* 																{% for categories in submenu.subcategory.categories %}*/
/* 																	<li>*/
/* 																		{% if submenu.subcategory.show_title == 1 %}*/
/* 																			<a href="{{ categories.href }}" class="title-submenu {{ submenu.class_menu }}">{{ categories.name }}</a>*/
/* 																		{% endif %}*/
/* 																		{% if categories.categories %}*/
/* 																			{{ categories.categories }}*/
/* 																		{% endif %}*/
/* 																		{% if submenu.subcategory.show_image == 1 %}*/
/* 																			<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ categories.thumb }}" alt="" >*/
/* 																		{% endif %}*/
/* 																	</li>*/
/* 																{% endfor %}*/
/* 															</ul>*/
/* 														{% endif %}*/
/* 													{% elseif submenu.content_type == 6 %}*/
/* 														{% if submenu.productlist.show_title == 1 %}*/
/* 															<span class="title-submenu">{{ submenu.name[lang_id] }}</span>*/
/* 														{% endif %}*/
/* 														{% if submenu.productlist.products %}*/
/* 															{% for product in submenu.productlist.products %}*/
/* 																{% set itempage = submenu.productlist.col ? 12/submenu.productlist.col : 4 %}*/
/* 																<div class="col-xs-{{ itempage }} {{ submenu.class_menu }}">*/
/* 																	<div class="product-thumb">*/
/* 																		<div class="image">*/
/* 																			<a href="{{ product.href }}">*/
/* 																				<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}"  />*/
/* 																			</a>*/
/* 																		</div>*/
/* 																		<div>*/
/* 																			<div class="caption">*/
/* 																				<h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 																				<p>{{ product.description }}</p>*/
/* 																				{% if product.rating %}*/
/* 																					<div class="rating">*/
/* 																						{% for i in 1..5 %}*/
/* 																							{% if product.rating < i %}*/
/* 																								<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 																							{% else %}*/
/* 																								<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 																							{% endif %}*/
/* 																						{% endfor %}*/
/* 																					</div>*/
/* 																				{% endif %}*/
/* 																		*/
/* 																				{% if product.price %}*/
/* 																					<p class="price">*/
/* 																						{% if not product.special %}*/
/* 																							{{ product.price }}*/
/* 																				   		{% else %}*/
/* 																							<span class="price-new">{{ product.special }}</span>*/
/* 																							<span class="price-old">{{ product.price }}</span>*/
/* 																				   		{% endif %}*/
/* */
/* 																				   		*/
/* 																					</p>*/
/* 																				{% endif %}*/
/* 																	  		</div>*/
/* 																		</div>*/
/* 																  	</div>*/
/* 																</div>*/
/* 														 	{% endfor %}*/
/* 														{% endif %}*/
/* 													{% endif %}													*/
/* 												</div>*/
/* 											{% endfor %}*/
/* 										</div>*/
/* 									</div>				*/
/* 									</div>			*/
/* 								{% endif %}*/
/* 							</li>							*/
/* 						{% else %}						*/
/* 							<li class="{{ class }}" {{ title }}>*/
/* 								<p class='close-menu'></p>*/
/* 								{% if row.submenu is iterable and row.submenu %}*/
/* 									<a href="{{ row.link }}" class="{{ class_link }}" {{ target }}>*/
/* 										*/
/* 										<strong>*/
/* 											{{ icon_font~icon~row.name[lang_id]~row.description }}*/
/* 										</strong>*/
/* 										{{ label_item }}*/
/* 										<b class='caret'></b>*/
/* 									</a>*/
/* 								{% else %}*/
/* 									<a href="{{ row.link }}" class="{{ class_link }}" {{ target }}>*/
/* 										<strong>*/
/* 											{{ icon_font~icon~row.name[lang_id]~row.description }}*/
/* 										</strong>*/
/* 										{{ label_item }}*/
/* 									</a>*/
/* 								{% endif %}*/
/* */
/* 								{% if row.submenu is iterable and row.submenu %}*/
/* 									<div class="sub-menu" style="width: {{ row.submenu_width }}">*/
/* 										<div class="content">*/
/* 											<div class="row">*/
/* 												{% set row_fluid = 0 %}*/
/* 												{% for submenu in row.submenu %}*/
/* 													{% if row_fluid+submenu.content_width > 12 %}*/
/* 														{% set row_fluid = submenu.content_width %}*/
/* 														</div><div class="border"></div><div class="row">*/
/* 													{% else %}*/
/* 														{% set row_fluid = row_fluid+submenu.content_width %}*/
/* 													{% endif %}*/
/* 													<div class="col-sm-{{ submenu.content_width }}">*/
/* 														{% if submenu.content_type == 0 %}*/
/* 															<div class="html {{ submenu.class_menu }}">*/
/* 																{{ submenu.html }}*/
/* 															</div>*/
/* 														{% elseif submenu.content_type == 1 %}*/
/* 															{% if submenu.product is iterable %}*/
/* 																<div class="product {{ submenu.class_menu }}">*/
/* 																	<div class="image"><a href="{{ submenu.product.link }}" onclick="window.location = '{{ submenu.product.link }}'"><img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ submenu.product.image }}" alt=""></a></div>*/
/* 																	<div class="name"><a href="{{ submenu.product.link }}" onclick="window.location = '{{ submenu.product.link }}'">{{ submenu.product.name }}</a></div>*/
/* 																	<div class="price">*/
/* 																		{% if not submenu.product.special %}*/
/* 																			{{ submenu.product.price }}*/
/* 																		{% else %}*/
/* 																			{{ submenu.product.special }}*/
/* 																		{% endif %}*/
/* 																	</div>*/
/* 																</div>*/
/* 															{% endif %}*/
/* 														{% elseif submenu.content_type == 2 %}*/
/* 															<div class="categories {{ submenu.class_menu }}">*/
/* 																{{ submenu.categories }}*/
/* 															</div>															*/
/* 														{% elseif submenu.content_type == 3 %}*/
/* 															{% if submenu.manufactures is iterable %}*/
/* 																<ul class="manufacturer {{ submenu.class_menu }}">*/
/* 																	{% for manufacturer in submenu.manufactures %}*/
/* 																		<li><a href="{{ manufacturer.link }}">{{ manufacturer.name }}</a></li>*/
/* 																	{% endfor %}*/
/* 																</ul>*/
/* 															{% endif %}*/
/* 														{% elseif submenu.content_type == 4 %}*/
/* 															{% if submenu.images.show_title == 1 %}*/
/* 																<span class="title-submenu">{{ submenu.name[lang_id] }}</span>*/
/* 															{% endif %}*/
/* 															<div class="link {{ submenu.class_menu }}">*/
/* 																{{ submenu.images.link }}*/
/* 															</div>*/
/* 														{% elseif submenu.content_type == 5 %}*/
/* 															{% if submenu.subcategory.categories %}*/
/* 																<ul class="subcategory {{ submenu.class_menu }}">*/
/* 																	{% for categories in submenu.subcategory.categories %}*/
/* 																		<li>*/
/* 																			{% if submenu.subcategory.show_title == 1 %}*/
/* 																				<a href="{{ categories.href }}" class="title-submenu {{ submenu.class_menu }}">{{ categories.name }}</a>*/
/* 																			{% endif %}*/
/* 																			{% if categories.categories %}*/
/* 																				{{ categories.categories }}*/
/* 																			{% endif %}*/
/* 																			{% if submenu.subcategory.show_image == 1 %}*/
/* 																				<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ categories.thumb }}" alt="" />*/
/* 																			{% endif %}*/
/* 																		</li>		*/
/* 																	{% endfor %}*/
/* 																</ul>*/
/* 															{% endif %}*/
/* 														{% elseif submenu.content_type == 6 %}*/
/* 															{% if submenu.productlist.show_title == 1 %}*/
/* 																<span class="title-submenu">{{ submenu.name[lang_id] }}</span>*/
/* 															{% endif %}*/
/* 															{% if submenu.productlist.products %}*/
/* 																{% for product in submenu.productlist.products %}*/
/* 																	{% set itempage = submenu.productlist.col ? 12/submenu.productlist.col : 4 %}*/
/* 																	<div class="col-sm-{{ itempage }} {{ submenu.class_menu }}">*/
/* 																		<div class="product-thumb">*/
/* 																			<div class="image">*/
/* 																				<a href="{{ product.href }}">*/
/* 																					<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}"/>*/
/* 																				</a>*/
/* 																			</div>*/
/* 																			<div>*/
/* 																				<div class="caption">*/
/* 																					<h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 																					<p>{{ product.description }}</p>*/
/* 																					{% if product.rating %}*/
/* 																						<div class="rating">*/
/* 																							{% for i in 1..5 %}*/
/* 																								{% if product.rating < i %}*/
/* 																									<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 																								{% else %}*/
/* 																									<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 																								{% endif %}*/
/* 																							{% endfor %}*/
/* 																						</div>*/
/* 																					{% endif %}*/
/* */
/* 																					{% if product.price %}*/
/* 																						<p class="price">*/
/* 																							{% if not product.special %}*/
/* 																								{{ product.price }}*/
/* 																						   	{% else %}*/
/* 																								<span class="price-new">{{ product.special }}</span>*/
/* 																								<span class="price-old">{{ product.price }}</span>*/
/* 																						   	{% endif %}*/
/* 																						   	*/
/* 																						   */
/* 																						</p>*/
/* 																					{% endif %}*/
/* 																		  		</div>*/
/* 																			</div>*/
/* 													  					</div>*/
/* 																	</div>*/
/* 												 				{% endfor %}*/
/* 															{% endif %}*/
/* 														{% endif %}*/
/* 													</div>*/
/* 												{% endfor %}*/
/* 											</div>												*/
/* 										</div>*/
/* 									</div>										*/
/* 								{% endif %}*/
/* 							</li>*/
/* 						{% endif %}*/
/* 					{% endfor %}*/
/* 				</ul>		*/
/* 			</div>*/
/* 		</div>*/
/* 		</div>*/
/* 	</nav>*/
/* 	{% if ustawienia.orientation == 1 %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* </div>*/
/* */
/* {% if ustawienia.orientation == 1 %}*/
/* <script type="text/javascript">*/
/* 	$(document).ready(function() {*/
/* */
/* 		(function (element) {*/
/* 			var $element = $(element);*/
/* 			var itemver =  {{ ustawienia.show_itemver }};*/
/* 			*/
/* 			$("ul.megamenu  li.item-vertical", $element ).addClass('demo');*/
/* 			*/
/* 			if(itemver <= $( ".vertical ul.megamenu >li", $element ).length){*/
/* 				$('.vertical ul.megamenu', $element).append('<li class="loadmore"><i class="fa fa-plus-square-o"></i><span class="more-view"> {{text_more_category}}</span></li>');*/
/* 				$('.horizontal ul.megamenu li.loadmore', $element).remove();*/
/* 			}*/
/* 			*/
/* 			var show_itemver = itemver-1 ;*/
/* 			$('ul.megamenu > li.item-vertical', $element).each(function(i){*/
/* 				if(i>show_itemver){*/
/* 						$(this).css('display', 'none');*/
/* 				} */
/* 			});*/
/* 			*/
/* 			$(".megamenu .loadmore", $element).click(function(){*/
/* 				if($(this).hasClass('open')){*/
/* 					$('ul.megamenu li.item-vertical', $element).each(function(i){*/
/* 							if(i>show_itemver){*/
/* 									$(this).slideUp(200);*/
/* 									$(this).css('display', 'none');*/
/* 							}*/
/* 					});*/
/* 					*/
/* 					$(this).removeClass('open');*/
/* 					$('.loadmore', $element).html('<i class="fa fa-plus"></i><span class="more-view">{{text_more_category}}</span>');*/
/* 				}else{*/
/* 					$('ul.megamenu li.item-vertical', $element).each(function(i){*/
/* 							if(i>show_itemver){*/
/* 									$(this).slideDown(200);*/
/* 							}*/
/* 					});*/
/* 					$(this).addClass('open');*/
/* 					$('.loadmore', $element).html('<i class="fa fa-minus"></i><span class="more-view">{{text_more_category}}</span>');*/
/* 				}*/
/* 			});*/
/* 		})("#{{suffix_id}}");*/
/* */
/* */
/* 	*/
/* */
/* 	    $("#{{show_verticalmenu_id}}").click(function () {*/
/* 			if($('#{{suffix_id}} .vertical-wrapper').hasClass('so-vertical-active'))*/
/* 				$('#{{suffix_id}} .vertical-wrapper').removeClass('so-vertical-active');*/
/* 			else*/
/* 				$('#{{suffix_id}} .vertical-wrapper').addClass('so-vertical-active');*/
/* 		}); */
/* 		*/
/* 		$('#{{remove_verticalmenu_id}}').click(function() {*/
/* 	        $('#{{suffix_id}} .vertical-wrapper').removeClass('so-vertical-active');*/
/* 	        return false;*/
/* 	    });	*/
/* 	});*/
/* </script>*/
/* {% else %}*/
/* <script>*/
/* $(document).ready(function(){*/
/* 	$("#{{show_megamenu_id}}").click(function () {*/
/* 		if($('#{{suffix_id}} .megamenu-wrapper',).hasClass('so-megamenu-active'))*/
/* 			$('#{{suffix_id}} .megamenu-wrapper').removeClass('so-megamenu-active');*/
/* 		else*/
/* 			$('#{{suffix_id}} .megamenu-wrapper').addClass('so-megamenu-active');*/
/* 	}); */
/* 	$("#{{remove_megamenu_id}}").click(function() {*/
/*         $('#{{suffix_id}} .megamenu-wrapper').removeClass('so-megamenu-active');*/
/*         return false;*/
/*     });		*/
/* 	*/
/* });*/
/* </script>*/
/* */
/* {% endif %}*/
/* <script>*/
/* $(document).ready(function(){*/
/* 	$('a[href="{{ actual_link }}"]').each(function() {*/
/* 		$(this).parents('.with-sub-menu').addClass('sub-active');*/
/* 	});  */
/* });*/
/* </script>*/
