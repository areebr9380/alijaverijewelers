<?php

/* so-claue/template/product/category.twig */
class __TwigTemplate_6187a64cb73418870795f9c3f5e0b1bfa53f142e90b17adb8138f7cc707511ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 4
        $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/breadcrumbs.twig"), "so-claue/template/product/category.twig", 4)->display($context);
        // line 5
        echo "

";
        // line 8
        if ((isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null)) {
            $context["col_position"] = (isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null);
        } else {
            // line 9
            $context["col_position"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_position"), "method");
            echo " ";
        }
        // line 10
        echo "
";
        // line 11
        if ((isset($context["url_asideType"]) ? $context["url_asideType"] : null)) {
            echo " ";
            $context["col_canvas"] = (isset($context["url_asideType"]) ? $context["url_asideType"] : null);
        } else {
            // line 12
            $context["col_canvas"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_type"), "method");
        }
        // line 13
        $context["desktop_canvas"] = ((((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) ? ("desktop-offcanvas") : (""));
        // line 14
        $context["displaySidebar"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_display_sidebar"), "method");
        // line 15
        echo "
";
        // line 16
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "inside")) {
            // line 17
            echo "<div class=\"container\">
\t";
            // line 18
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/subcategory.twig"), "so-claue/template/product/category.twig", 18)->display($context);
            // line 19
            echo "\t
</div>
";
        }
        // line 22
        echo "
<div class=\"container product-listing content-main ";
        // line 23
        echo (isset($context["desktop_canvas"]) ? $context["desktop_canvas"] : null);
        echo "\">
  
  <div class=\"row\">
\t";
        // line 26
        if ((((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "left") || ((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "all"))) {
            // line 27
            echo "\t\t";
            echo (isset($context["column_left"]) ? $context["column_left"] : null);
            echo "
\t";
        }
        // line 29
        echo "\t 
    ";
        // line 30
        if (((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) {
            // line 31
            echo "    \t";
            $context["class"] = "col-sm-12";
            // line 32
            echo "    ";
        } elseif ((((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "left") && ((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "right"))) {
            // line 33
            echo "    \t";
            $context["class"] = "col-md-6 col-sm-12 col-xs-12 fluid-allsidebar";
            // line 34
            echo "    ";
        } elseif ((((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "left") || ((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "right"))) {
            // line 35
            echo "    \t";
            $context["class"] = "col-md-9 col-sm-12 col-xs-12 fluid-sidebar";
            // line 36
            echo "\t";
        } elseif (((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "all")) {
            // line 37
            echo "    \t";
            $context["class"] = "col-md-8 col-sm-12 col-xs-12 fluid-sidebar";
            // line 38
            echo "    ";
        } else {
            // line 39
            echo "    \t";
            $context["class"] = "col-sm-12";
            // line 40
            echo "    ";
        }
        // line 41
        echo "
    <div id=\"content\" class=\"";
        // line 42
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
\t\t
    \t";
        // line 44
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "featured_cate_status"), "method")) {
            echo " ";
            echo (isset($context["content_top"]) ? $context["content_top"] : null);
        }
        // line 45
        echo "\t\t
\t\t<div class=\"products-category clearfix\">

\t\t\t";
        // line 48
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "outside")) {
            // line 49
            echo "\t\t\t\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/subcategory.twig"), "so-claue/template/product/category.twig", 49)->display($context);
            // line 50
            echo "\t\t\t";
        }
        // line 51
        echo "\t  
\t\t\t";
        // line 52
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 53
            echo "\t\t\t\t";
            // line 54
            echo "\t\t\t\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/listing.twig"), "so-claue/template/product/category.twig", 54)->display(array_merge($context, array("listingType" => (isset($context["listingType"]) ? $context["listingType"] : null))));
            // line 55
            echo "\t\t\t\t
\t\t\t";
        }
        // line 57
        echo "\t\t  
\t\t\t";
        // line 58
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 59
            echo "\t\t\t  <p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t\t\t  <div class=\"buttons\">
\t\t\t\t<div class=\"pull-right\"><a href=\"";
            // line 61
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
\t\t\t  </div>
\t\t\t";
        }
        // line 64
        echo "
\t      \t";
        // line 65
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "

\t  \t</div>
\t </div>
\t
\t";
        // line 70
        if ((((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "right") || ((isset($context["displaySidebar"]) ? $context["displaySidebar"] : null) == "all"))) {
            // line 71
            echo "\t\t";
            echo (isset($context["column_right"]) ? $context["column_right"] : null);
            echo "
\t";
        }
        // line 73
        echo "    

    ";
        // line 75
        if ((isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null)) {
            echo " ";
            $context["sidebar_sticky"] = (isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null);
            // line 76
            echo "\t";
        } else {
            echo " ";
            $context["sidebar_sticky"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_sidebar_sticky"), "method");
        }
        // line 77
        echo "    <script type=\"text/javascript\"><!--
\t\t\$(window).load(sidebar_sticky_update());
\t\t\$(window).resize(sidebar_sticky_update());

    \tfunction sidebar_sticky_update(){
    \t\t var viewportWidth = \$(window).width();
    \t\t if (viewportWidth > 1200) {
\t    \t\t// Initialize the sticky scrolling on an item 
\t\t\t\tsidebar_sticky = '";
        // line 85
        echo (isset($context["sidebar_sticky"]) ? $context["sidebar_sticky"] : null);
        echo "';
\t\t\t\t
\t\t\t\tif(sidebar_sticky=='left'){
\t\t\t\t\t\$(\".left_column\").stick_in_parent({
\t\t\t\t\t    offset_top: 10,
\t\t\t\t\t    bottoming   : true
\t\t\t\t\t});
\t\t\t\t}else if (sidebar_sticky=='right'){
\t\t\t\t\t\$(\".right_column\").stick_in_parent({
\t\t\t\t\t    offset_top: 10,
\t\t\t\t\t    bottoming   : true
\t\t\t\t\t});
\t\t\t\t}else if (sidebar_sticky=='all'){
\t\t\t\t\t\$(\".content-aside\").stick_in_parent({
\t\t\t\t\t    offset_top: 10,
\t\t\t\t\t    bottoming   : true
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
    \t}
\t\t
\t\t
\t//--></script> 

\t</div>
</div>
";
        // line 111
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "so-claue/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 111,  224 => 85,  214 => 77,  208 => 76,  204 => 75,  200 => 73,  194 => 71,  192 => 70,  184 => 65,  181 => 64,  173 => 61,  167 => 59,  165 => 58,  162 => 57,  158 => 55,  155 => 54,  153 => 53,  151 => 52,  148 => 51,  145 => 50,  142 => 49,  140 => 48,  135 => 45,  130 => 44,  125 => 42,  122 => 41,  119 => 40,  116 => 39,  113 => 38,  110 => 37,  107 => 36,  104 => 35,  101 => 34,  98 => 33,  95 => 32,  92 => 31,  90 => 30,  87 => 29,  81 => 27,  79 => 26,  73 => 23,  70 => 22,  65 => 19,  63 => 18,  60 => 17,  58 => 16,  55 => 15,  53 => 14,  51 => 13,  48 => 12,  43 => 11,  40 => 10,  36 => 9,  32 => 8,  28 => 5,  26 => 4,  22 => 2,  19 => 1,);
    }
}
/* */
/* {{ header }}*/
/* {#====  Loader breadcrumbs ==== #}*/
/* {% include theme_directory~'/template/soconfig/breadcrumbs.twig' %}*/
/* */
/* */
/* {#====  Variables url parameter ==== #}*/
/* {% if url_asidePosition %}{% set col_position = url_asidePosition %}*/
/* {% else %}{% set col_position = soconfig.get_settings('catalog_col_position') %} {% endif %}*/
/* */
/* {% if url_asideType %} {% set col_canvas = url_asideType %}*/
/* {% else %}{% set col_canvas = soconfig.get_settings('catalog_col_type') %}{% endif %}*/
/* {% set desktop_canvas = col_canvas =='off_canvas' ? 'desktop-offcanvas' : '' %}*/
/* {% set displaySidebar = soconfig.get_settings('catalog_display_sidebar')%}*/
/* */
/* {% if col_position == 'inside' %}*/
/* <div class="container">*/
/* 	{% include theme_directory~'/template/soconfig/subcategory.twig' %}*/
/* 	*/
/* </div>*/
/* {% endif %}*/
/* */
/* <div class="container product-listing content-main {{desktop_canvas}}">*/
/*   */
/*   <div class="row">*/
/* 	{% if displaySidebar =='left' or displaySidebar =='all'%}*/
/* 		{{ column_left }}*/
/* 	{% endif %}*/
/* 	 */
/*     {% if col_canvas =='off_canvas' %}*/
/*     	{% set class = 'col-sm-12' %}*/
/*     {% elseif displaySidebar =='left' and displaySidebar =='right' %}*/
/*     	{% set class = 'col-md-6 col-sm-12 col-xs-12 fluid-allsidebar' %}*/
/*     {% elseif displaySidebar =='left' or displaySidebar =='right' %}*/
/*     	{% set class = 'col-md-9 col-sm-12 col-xs-12 fluid-sidebar' %}*/
/* 	{% elseif displaySidebar =='all' %}*/
/*     	{% set class = 'col-md-8 col-sm-12 col-xs-12 fluid-sidebar' %}*/
/*     {% else %}*/
/*     	{% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/* */
/*     <div id="content" class="{{ class }}">*/
/* 		*/
/*     	{% if soconfig.get_settings('featured_cate_status') %} {{ content_top }}{% endif %}*/
/* 		*/
/* 		<div class="products-category clearfix">*/
/* */
/* 			{% if col_position== 'outside' %}*/
/* 				{% include theme_directory~'/template/soconfig/subcategory.twig' %}*/
/* 			{% endif %}*/
/* 	  */
/* 			{% if products %}*/
/* 				{#==== Product Listing ==== #}*/
/* 				{% include theme_directory~'/template/soconfig/listing.twig' with {listingType: listingType} %}*/
/* 				*/
/* 			{% endif %}*/
/* 		  */
/* 			{% if not categories and not products %}*/
/* 			  <p>{{ text_empty }}</p>*/
/* 			  <div class="buttons">*/
/* 				<div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>*/
/* 			  </div>*/
/* 			{% endif %}*/
/* */
/* 	      	{{ content_bottom }}*/
/* */
/* 	  	</div>*/
/* 	 </div>*/
/* 	*/
/* 	{% if displaySidebar =='right' or displaySidebar =='all'%}*/
/* 		{{ column_right }}*/
/* 	{% endif %}*/
/*     */
/* */
/*     {% if url_sidebarsticky %} {% set sidebar_sticky = url_sidebarsticky %}*/
/* 	{% else %} {% set sidebar_sticky = soconfig.get_settings('catalog_sidebar_sticky') %}{% endif %}*/
/*     <script type="text/javascript"><!--*/
/* 		$(window).load(sidebar_sticky_update());*/
/* 		$(window).resize(sidebar_sticky_update());*/
/* */
/*     	function sidebar_sticky_update(){*/
/*     		 var viewportWidth = $(window).width();*/
/*     		 if (viewportWidth > 1200) {*/
/* 	    		// Initialize the sticky scrolling on an item */
/* 				sidebar_sticky = '{{sidebar_sticky}}';*/
/* 				*/
/* 				if(sidebar_sticky=='left'){*/
/* 					$(".left_column").stick_in_parent({*/
/* 					    offset_top: 10,*/
/* 					    bottoming   : true*/
/* 					});*/
/* 				}else if (sidebar_sticky=='right'){*/
/* 					$(".right_column").stick_in_parent({*/
/* 					    offset_top: 10,*/
/* 					    bottoming   : true*/
/* 					});*/
/* 				}else if (sidebar_sticky=='all'){*/
/* 					$(".content-aside").stick_in_parent({*/
/* 					    offset_top: 10,*/
/* 					    bottoming   : true*/
/* 					});*/
/* 				}*/
/* 			}*/
/*     	}*/
/* 		*/
/* 		*/
/* 	//--></script> */
/* */
/* 	</div>*/
/* </div>*/
/* {{ footer }} */
/* */
