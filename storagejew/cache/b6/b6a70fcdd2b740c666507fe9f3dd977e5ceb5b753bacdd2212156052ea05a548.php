<?php

/* so-claue/template/soconfig/quickview.twig */
class __TwigTemplate_9b74460970a3350747f8ba960939c3e19ca3d4b8d14980b254dc7c7b4eafe27d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

<div class=\"product-detail\">
\t<div id=\"product-quick\" class=\"product-info\">
\t\t<div class=\"product-view row\">
\t\t\t<div class=\"left-content-product \">
\t\t\t\t";
        // line 16
        echo "\t\t\t\t<div class=\"content-product-left class-honizol  col-sm-5\">
\t\t\t\t\t<div class=\"large-image \">
\t\t\t\t\t\t<img class=\"product-image-zoom\" src=\"";
        // line 18
        echo (isset($context["popup"]) ? $context["popup"] : null);
        echo "\" data-zoom-image=\"";
        echo (isset($context["popup"]) ? $context["popup"] : null);
        echo "\" title=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" alt=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t";
        // line 22
        echo "\t\t\t\t\t<div id=\"thumb-slider\" class=\"full_slider contentslider\" data-rtl=\"";
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" data-autoplay=\"no\"  data-pagination=\"no\" data-delay=\"4\" data-speed=\"0.6\" data-margin=\"10\"  data-items_column0=\"4\" data-items_column1=\"3\" data-items_column2=\"4\" data-items_column3=\"3\" data-items_column4=\"2\" data-arrows=\"yes\" data-lazyload=\"yes\" data-loop=\"no\" data-hoverpause=\"yes\">
\t\t\t\t\t\t";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["image"]) {
            // line 24
            echo "\t\t\t\t\t\t\t<div class=\"image-additional\">
\t\t\t\t\t\t\t<a data-index=\"";
            // line 25
            echo $context["key"];
            echo "\" class=\"img thumbnail \" data-image=\"";
            echo $this->getAttribute($context["image"], "popup", array());
            echo "\" title=\"";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "\">
\t\t\t\t\t\t\t\t<img src=\"";
            // line 26
            echo $this->getAttribute($context["image"], "thumb", array());
            echo "\" title=\"";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "\" alt=\"";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "\" />
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 35
        echo "\t\t\t\t<div class=\"content-product-right col-sm-7\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-7 col-xs-12\">
\t\t\t\t\t\t\t<div class=\"title-product\">
\t\t\t\t\t\t\t\t<h1>";
        // line 40
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo " </h1>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        // line 42
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 43
            echo "\t\t\t\t\t\t\t";
            // line 44
            echo "\t\t\t\t\t\t\t<div class=\"box-review\">
\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 48
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                } else {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                }
                // line 49
                echo "\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<a class=\"reviews_button\" href=\"#\" >";
            // line 52
            echo (isset($context["reviews"]) ? $context["reviews"] : null);
            echo "</a> 
\t\t\t\t\t\t\t\t";
            // line 53
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_order"), "method")) {
                // line 54
                echo "\t\t\t\t\t\t\t\t\t<span class=\"order-num\">";
                echo (isset($context["orders"]) ? $context["orders"] : null);
                echo "</span>
\t\t\t\t\t\t\t\t";
            }
            // line 56
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        // line 58
        echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 61
        echo "\t\t\t\t\t\t\t";
        if ((isset($context["price"]) ? $context["price"] : null)) {
            echo " 
\t\t\t\t\t\t\t<div class=\"product_page_price price\" itemprop=\"offerDetails\" itemscope itemtype=\"http://data-vocabulary.org/Offer\">
\t\t\t\t\t\t\t\t";
            // line 63
            if ( !(isset($context["special"]) ? $context["special"] : null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<span class=\"price-new\"><span itemprop=\"price\" id=\"price-old\">";
                // line 64
                echo (isset($context["price"]) ? $context["price"] : null);
                echo " </span></span>
\t\t\t\t\t\t\t\t";
            } else {
                // line 65
                echo "   
\t\t\t\t\t\t\t\t\t<span class=\"price-new\"><span itemprop=\"price\" id=\"price-special\">";
                // line 66
                echo (isset($context["special"]) ? $context["special"] : null);
                echo " </span></span> <span class=\"price-old\" id=\"price-old\">";
                echo (isset($context["price"]) ? $context["price"] : null);
                echo " </span>
\t\t\t\t\t\t\t\t";
            }
            // line 67
            echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 69
            if ((isset($context["tax"]) ? $context["tax"] : null)) {
                echo " 
\t\t\t\t\t\t\t\t\t<div class=\"price-tax\"><span>";
                // line 70
                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                echo " </span> ";
                echo (isset($context["tax"]) ? $context["tax"] : null);
                echo " </div>
\t\t\t\t\t\t\t\t";
            }
            // line 71
            echo " 
\t\t\t\t\t\t\t\t";
            // line 73
            echo "\t\t\t\t\t\t\t\t";
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "discount_status"), "method")) {
                echo " 
\t\t\t\t\t\t\t\t\t";
                // line 74
                if (((isset($context["price"]) ? $context["price"] : null) && (isset($context["special"]) ? $context["special"] : null))) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t<span class=\"label-product label-sale\"> ";
                    // line 75
                    echo (isset($context["discount"]) ? $context["discount"] : null);
                    echo "</span>
\t\t\t\t\t\t\t\t\t";
                }
                // line 76
                echo " 
\t\t\t\t\t\t\t\t";
            }
            // line 77
            echo " 

\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        // line 81
        echo " 
\t\t\t\t\t\t \t";
        // line 82
        if ((isset($context["discounts"]) ? $context["discounts"] : null)) {
            echo " 
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled text-success\">
\t\t\t\t\t\t\t\t";
            // line 84
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["discounts"]) ? $context["discounts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                echo " 
\t\t\t\t\t\t\t\t\t<li><strong>";
                // line 85
                echo $this->getAttribute($context["discount"], "quantity", array());
                echo " ";
                echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
                echo " ";
                echo $this->getAttribute($context["discount"], "price", array());
                echo "</strong> </li>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t";
        }
        // line 88
        echo " \t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-sm-5 col-xs-12\">
\t\t\t\t\t\t\t<div class=\"product-box-desc\">
\t\t\t\t\t\t\t\t";
        // line 92
        if ((isset($context["manufacturer"]) ? $context["manufacturer"] : null)) {
            echo " 
\t\t\t\t\t\t\t\t\t\t<div class=\"brand\"><span>";
            // line 93
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo " </span><a href=\"";
            echo (isset($context["manufacturers"]) ? $context["manufacturers"] : null);
            echo " \">";
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo " </a></div>
\t\t\t\t\t\t\t\t";
        }
        // line 94
        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
        // line 96
        if ((isset($context["model"]) ? $context["model"] : null)) {
            echo " 
\t\t\t\t\t\t\t\t\t<div class=\"model\"><span>";
            // line 97
            echo (isset($context["text_model"]) ? $context["text_model"] : null);
            echo " </span> ";
            echo (isset($context["model"]) ? $context["model"] : null);
            echo " </div>
\t\t\t\t\t\t\t\t";
        }
        // line 98
        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
        // line 100
        if ((isset($context["points"]) ? $context["points"] : null)) {
            echo " 
\t\t\t\t\t\t\t\t\t<div class=\"reward hidden\"><span>";
            // line 101
            echo (isset($context["text_points"]) ? $context["text_points"] : null);
            echo " </span> ";
            echo (isset($context["points"]) ? $context["points"] : null);
            echo " </div>
\t\t\t\t\t\t\t\t";
        }
        // line 102
        echo " 
\t\t\t\t\t\t\t\t<div class=\"stock\"><span>";
        // line 103
        echo (isset($context["text_stock"]) ? $context["text_stock"] : null);
        echo " </span> <i class=\"fa fa-check-square-o\"></i> ";
        echo (isset($context["stock"]) ? $context["stock"] : null);
        echo " </div>\t
\t\t\t\t\t\t\t\t<div class=\"inner-box-viewed \">
\t\t\t\t\t\t\t\t\t<span>";
        // line 105
        echo (isset($context["text_viewed"]) ? $context["text_viewed"] : null);
        echo "</span> <i class=\"fa fa-eye\" ></i> ";
        echo (isset($context["viewed"]) ? $context["viewed"] : null);
        echo "
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"short_description form-group\">
\t\t\t\t\t\t<h3>";
        // line 112
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_overview"), "method");
        echo "</h3>
\t\t\t\t\t\t<div class=\"form-group\">";
        // line 113
        echo (isset($context["description_short"]) ? $context["description_short"] : null);
        echo "</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t";
        // line 116
        if ((isset($context["options"]) ? $context["options"] : null)) {
            echo " 
\t\t\t\t\t<div id=\"product\">\t
\t\t\t\t\t\t<h3>";
            // line 118
            echo (isset($context["text_option"]) ? $context["text_option"] : null);
            echo " </h3>
\t\t\t\t\t\t";
            // line 119
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 120
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["option"], "type", array()) == "select")) {
                    // line 121
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 122
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t\t<select name=\"option[";
                    // line 123
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"\">";
                    // line 124
                    echo (isset($context["text_select"]) ? $context["text_select"] : null);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                    // line 125
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 126
                        echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo "
\t\t\t\t\t\t\t\t\t";
                        // line 127
                        if ($this->getAttribute($context["option_value"], "price", array())) {
                            // line 128
                            echo "\t\t\t\t\t\t\t\t\t\t(";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo ")
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 130
                        echo "\t\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 132
                    echo "\t\t\t\t\t\t\t  </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 135
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 136
                if (($this->getAttribute($context["option"], "type", array()) == "radio")) {
                    // line 137
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 138
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t\t<div id=\"input-option";
                    // line 139
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t\t";
                    // line 140
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 141
                    echo "\t\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 142
                    echo "
\t\t\t\t\t\t\t\t\t";
                    // line 143
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 144
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 145
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"radio ";
                        // line 147
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"option[";
                        // line 149
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 150
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 151
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 152
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 153
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 154
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 155
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " (";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )";
                        }
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 160
                    echo "\t
\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t";
                    // line 162
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 165
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t\t   \$('#input-option";
                        // line 166
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "  span').removeClass(\"active\");
\t\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 171
                    echo " 

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 176
                echo "
\t\t\t\t\t\t\t";
                // line 177
                if (($this->getAttribute($context["option"], "type", array()) == "checkbox")) {
                    // line 178
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 179
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  \t<div id=\"input-option";
                    // line 180
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t\t";
                    // line 181
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 182
                    echo "\t\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 183
                    echo "
\t\t\t\t\t\t\t\t\t";
                    // line 184
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 185
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 186
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox  ";
                        // line 188
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t <input type=\"checkbox\" name=\"option[";
                        // line 191
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "][]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 192
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 193
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 194
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 195
                        echo " 

\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 197
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 198
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t(";
                            // line 199
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 200
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 205
                    echo "\t
\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t\t";
                    // line 207
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 210
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 215
                    echo " 

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 220
                echo "
\t\t\t\t\t\t\t";
                // line 221
                if (($this->getAttribute($context["option"], "type", array()) == "text")) {
                    // line 222
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 223
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  <input type=\"text\" name=\"option[";
                    // line 224
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 227
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["option"], "type", array()) == "textarea")) {
                    // line 228
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 229
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  <textarea name=\"option[";
                    // line 230
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\">";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 233
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["option"], "type", array()) == "file")) {
                    // line 234
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  <label class=\"control-label\">";
                    // line 235
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  <button type=\"button\" id=\"button-upload";
                    // line 236
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                    echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                    echo "</button>
\t\t\t\t\t\t\t  <input type=\"hidden\" name=\"option[";
                    // line 237
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 240
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["option"], "type", array()) == "date")) {
                    // line 241
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 242
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  <div class=\"input-group date\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 244
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 250
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["option"], "type", array()) == "datetime")) {
                    // line 251
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 252
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t  <div class=\"input-group datetime\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 254
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 260
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 261
                if (($this->getAttribute($context["option"], "type", array()) == "time")) {
                    // line 262
                    echo "\t\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 263
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t\t<div class=\"input-group time\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 265
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 271
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 274
            echo "\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 276
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 277
        if ((isset($context["recurrings"]) ? $context["recurrings"] : null)) {
            // line 278
            echo "\t\t\t\t\t<hr>
\t\t\t\t\t<h3>";
            // line 279
            echo (isset($context["text_payment_recurring"]) ? $context["text_payment_recurring"] : null);
            echo "</h3>
\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t  <select name=\"recurring_id\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">";
            // line 282
            echo (isset($context["text_select"]) ? $context["text_select"] : null);
            echo "</option>
\t\t\t\t\t\t";
            // line 283
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["recurrings"]) ? $context["recurrings"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 284
                echo "\t\t\t\t\t\t<option value=\"";
                echo $this->getAttribute($context["recurring"], "recurring_id", array());
                echo "\">";
                echo $this->getAttribute($context["recurring"], "name", array());
                echo "</option>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 286
            echo "\t\t\t\t\t  </select>
\t\t\t\t\t  <div class=\"help-block\" id=\"recurring-description\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 290
        echo "\t\t\t\t  
\t\t\t\t\t<div class=\"form-group box-info-product\">
\t\t\t\t\t\t<div class=\"option quantity\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-quantity\">";
        // line 293
        echo (isset($context["entry_qty"]) ? $context["entry_qty"] : null);
        echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group quantity-control\">
\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_down fa fa-minus\"></span>
\t\t\t\t\t\t\t\t  <input class=\"form-control\" type=\"text\" name=\"quantity\" value=\"";
        // line 296
        echo (isset($context["minimum"]) ? $context["minimum"] : null);
        echo "\" />
\t\t\t\t\t\t\t\t  <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 297
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "\" />\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_up fa fa-plus\"></span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"detail-action\">
\t\t\t\t\t\t\t";
        // line 303
        echo "\t\t\t\t\t\t\t<div class=\"cart\">
\t\t\t\t\t\t\t\t<input type=\"button\" value=\"";
        // line 304
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-cart\" class=\"btn btn-mega\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<input type=\"button\"  value=\"";
        // line 306
        echo (isset($context["text_buynow"]) ? $context["text_buynow"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-checkout\" class=\"btn btn-checkout \" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"add-to-links wish_comp\">
\t\t\t\t\t\t\t\t<a onclick=\"wishlist.add(";
        // line 309
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-heart\"></i> ";
        echo (isset($context["text_addwishlist"]) ? $context["text_addwishlist"] : null);
        echo "</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t";
        // line 314
        if (((isset($context["minimum"]) ? $context["minimum"] : null) > 1)) {
            // line 315
            echo "\t\t\t\t\t\t<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["text_minimum"]) ? $context["text_minimum"] : null);
            echo "</div>
\t\t\t\t\t";
        }
        // line 317
        echo "\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t\t
\t</div>
</div>

<style type=\"text/css\">
\tbody{background:none;}
\t#wrapper{box-shadow:none;background:#fff;}
\t#wrapper > *:not(.product-detail){display: none;}
\t#wrapper .product-view{margin:0;}
</style>


<script type=\"text/javascript\">
\t\$(document).ready(function() {
\t\t\$('.product-options li').click(function(){
\t\t\t\$(this).addClass(function() {
\t\t\t\tif(\$(this).hasClass(\"active\")) return \"\";
\t\t\t\treturn \"active\";
\t\t\t});
\t\t\t
\t\t\t\$(this).siblings(\"li\").removeClass(\"active\");
\t\t\t\$('.product-options .selected-option').html('<span class=\"label label-success\">'+ \$(this).find('img').data('original-title') +'</span>');
\t\t})
\t\t
\t});
\t\t\t
</script>

<script type=\"text/javascript\"><!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert, .text-danger').remove();
\t\t\t
\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script> 


<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=extension/soconfig/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product-quick input[type=\\'text\\'], #product-quick input[type=\\'hidden\\'], #product-quick input[type=\\'radio\\']:checked, #product-quick input[type=\\'checkbox\\']:checked, #product-quick select, #product-quick textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t
\t\t\t\$('.text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');
\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));
\t\t\t\t\t\t
\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t
\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}
\t\t\t\t
\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}
\t\t\t
\t\t\tif (json['success']) {
\t\t\t\tparent.\$('#previewModal').modal('show'); 
\t\t\t\tparent.\$('#previewModal .modal-body').load('index.php?route=extension/soconfig/cart/info&product_id='+ ";
        // line 412
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");
\t\t\t\tparent.\$('#cart  .total-shopping-cart ').html(json['total'] );
\t\t\t\tparent.\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t\tparent.\$('.text-danger').remove();
\t\t\t\tparent.\$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');
\t\t\t\tparent.\$.magnificPopup.close();
\t\t\t}
\t\t\t
\t\t
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});


\$('#button-checkout').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=checkout/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product-quick input[type=\\'text\\'], #product-quick input[type=\\'hidden\\'], #product-quick input[type=\\'radio\\']:checked, #product-quick input[type=\\'checkbox\\']:checked, #product-quick select, #product-quick textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-checkout').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-checkout').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\tparent.\$('.alert').remove();
\t\t\t\$('.text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));
\t\t\t\t\t\t
\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t
\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}
\t\t\t\t
\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\tparent.\$('.text-danger').remove();
\t\t\t\tparent.\$('#cart  .total-shopping-cart ').html(json['total'] );
\t\t\t\tparent.window.location.href = \"index.php?route=checkout/checkout\";
\t\t\t}
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});

var wishlist = {
\t'add': function(product_id) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=extension/soconfig/wishlist/add',
\t\t\ttype: 'post',
\t\t\tdata: 'product_id=' + product_id,
\t\t\tdataType: 'json',
\t\t\t
\t\t\tsuccess: function(json) {
                parent.\$('.alert').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    parent.\$('#wrapper').before('<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"fa fa-close close\" data-dismiss=\"alert\"></button></div>');
                }
                if (json['info']) {
                    parent.\$('#wrapper').before('<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ' + json['info'] + '<button type=\"button\" class=\"fa fa-close close\"></button></div>');
                }
                parent.\$('#wishlist-total').html(json['total']);
\t\t\t\tparent.\$('#wishlist-total').attr('title', json['total']);
                timer = setTimeout(function() {
                    parent.\$('.alert').addClass('fadeOut');
                }, 4000);
            },
\t\t});
\t}
}

var compare = {
\t'add': function(product_id) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=extension/soconfig/compare/add',
\t\t\ttype: 'post',
\t\t\tdata: 'product_id=' + product_id,
\t\t\tdataType: 'json',
\t\t\t
\t\t\tsuccess: function(json) {
                parent.\$('.alert').remove();
                if (json['info']) {
                   parent. \$('#wrapper').before('<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i>  ' + json['info'] + '<button type=\"button\" class=\"fa fa-close close\"></button></div>');
                }
                if (json['success']) {
                    parent.\$('#wrapper').before('<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i>' + json['success'] + '<button type=\"button\" class=\"fa fa-close close\"></button></div>');
                    if (json['warning']) {
                        parent.\$('.alert').append('<div class=\"alert alert-warning\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['warning'] + '<button type=\"button\" class=\"fa fa-close close\"></button></div>');
                    }
                    parent.\$('#compare-total').attr('data-original-title', json['total']);
                    parent.\$('#compare-total').html('<span>' + json['total'] + '</span>');
                }
                timer = setTimeout(function() {
                    parent.\$('.alert').addClass('fadeOut');
                }, 4000);
            },
\t\t});
\t}
\t
}
//--></script> 
<script type=\"text/javascript\"><!--
var zoomCollection = '.large-image img';
\t\t\$( zoomCollection ).elevateZoom({
\t\t\tzoomType        :\"none\",
\t\t\tlensSize    : '200',
\t\t\teasing:false,
\t\t\tgallery:'thumb-slider',
\t\t\tcursor: 'pointer',
\t\t\tgalleryActiveClass: \"active\",
\t\t});
\t\t
\t\t
\t\t\$(\"#thumb-slider .image-additional\").each(function() {
\t\t\t\$(this).find(\"[data-index='0']\").addClass('active');
\t\t});
\$('.product-options li.radio').click(function(){
\t\$(this).addClass(function() {
\t\tif(\$(this).hasClass(\"active\")) return \"\";
\t\treturn \"active\";
\t});
\t
\t\$(this).siblings(\"li\").removeClass(\"active\");
\t\$(this).parent().find('.selected-option').html('<span class=\"label label-success\">'+ \$(this).find('img').data('original-title') +'</span>');
})

\$('.date').datetimepicker({
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;
\t
\t\$('#form-upload').remove();
\t
\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');
\t
\t\$('#form-upload input[name=\\'file\\']').trigger('click');
    if (typeof timer != 'undefined') {
        clearInterval(timer);
    }
\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);
\t\t\t
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();
\t\t\t\t\t
\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);
\t\t\t\t\t\t
\t\t\t\t\t\t\$(node).parent().find('input').attr('value', json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 

<script type=\"text/javascript\">
var ajax_price = function() {
\t\$.ajax({
\t\ttype: 'POST',
\t\turl: 'index.php?route=extension/soconfig/liveprice/index',
\t\tdata: \$('.product-info input[type=\\'text\\'], .product-info input[type=\\'hidden\\'], .product-info input[type=\\'radio\\']:checked, .product-info input[type=\\'checkbox\\']:checked, .product-info select, .product-info textarea'),
\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\tif (json.success) {
\t\t\t\tchange_price('#price-special', json.new_price.special);
\t\t\t\tchange_price('#price-tax', json.new_price.tax);
\t\t\t\tchange_price('#price-old', json.new_price.price);
\t\t\t}
\t\t}
\t});
}

var change_price = function(id, new_price) {
\t\$(id).html(new_price);
}
\$('.product-info input[type=\\'text\\'], .product-info input[type=\\'hidden\\'], .product-info input[type=\\'radio\\'], .product-info input[type=\\'checkbox\\'], .product-info select, .product-info textarea, .product-info input[name=\\'quantity\\']').on('change', function() {
\tajax_price();
});
</script>

";
    }

    public function getTemplateName()
    {
        return "so-claue/template/soconfig/quickview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1041 => 412,  944 => 317,  938 => 315,  936 => 314,  926 => 309,  918 => 306,  911 => 304,  908 => 303,  900 => 297,  896 => 296,  890 => 293,  885 => 290,  879 => 286,  868 => 284,  864 => 283,  860 => 282,  854 => 279,  851 => 278,  849 => 277,  846 => 276,  842 => 274,  834 => 271,  821 => 265,  814 => 263,  807 => 262,  805 => 261,  802 => 260,  789 => 254,  782 => 252,  775 => 251,  772 => 250,  759 => 244,  752 => 242,  745 => 241,  742 => 240,  734 => 237,  726 => 236,  722 => 235,  715 => 234,  712 => 233,  700 => 230,  694 => 229,  687 => 228,  684 => 227,  672 => 224,  666 => 223,  659 => 222,  657 => 221,  654 => 220,  647 => 215,  638 => 210,  632 => 207,  628 => 205,  617 => 200,  610 => 199,  606 => 198,  602 => 197,  598 => 195,  589 => 194,  585 => 193,  579 => 192,  573 => 191,  567 => 188,  562 => 186,  558 => 185,  552 => 184,  549 => 183,  546 => 182,  544 => 181,  540 => 180,  536 => 179,  529 => 178,  527 => 177,  524 => 176,  517 => 171,  508 => 166,  504 => 165,  498 => 162,  494 => 160,  476 => 155,  472 => 154,  469 => 153,  460 => 152,  456 => 151,  450 => 150,  444 => 149,  439 => 147,  434 => 145,  430 => 144,  424 => 143,  421 => 142,  418 => 141,  416 => 140,  412 => 139,  408 => 138,  401 => 137,  399 => 136,  396 => 135,  391 => 132,  384 => 130,  377 => 128,  375 => 127,  368 => 126,  364 => 125,  360 => 124,  354 => 123,  348 => 122,  341 => 121,  338 => 120,  334 => 119,  330 => 118,  325 => 116,  319 => 113,  315 => 112,  303 => 105,  296 => 103,  293 => 102,  286 => 101,  282 => 100,  278 => 98,  271 => 97,  267 => 96,  263 => 94,  254 => 93,  250 => 92,  244 => 88,  240 => 87,  228 => 85,  222 => 84,  217 => 82,  214 => 81,  207 => 77,  203 => 76,  198 => 75,  194 => 74,  189 => 73,  186 => 71,  179 => 70,  175 => 69,  171 => 67,  164 => 66,  161 => 65,  156 => 64,  152 => 63,  146 => 61,  142 => 58,  138 => 56,  132 => 54,  130 => 53,  126 => 52,  122 => 50,  116 => 49,  109 => 48,  105 => 47,  100 => 44,  98 => 43,  96 => 42,  91 => 40,  84 => 35,  78 => 30,  64 => 26,  56 => 25,  53 => 24,  49 => 23,  44 => 22,  32 => 18,  28 => 16,  19 => 9,);
    }
}
/* {#*/
/* ****************************************************** */
/*  * @package	SO Framework for Opencart 3.x*/
/*  * @author	http://www.opencartworks.com*/
/*  * @license	GNU General Public License*/
/*  * @copyright(C) 2008-2017 opencartworks.com. All rights reserved.*/
/*  *******************************************************/
/* #}*/
/* {{header}}*/
/* */
/* <div class="product-detail">*/
/* 	<div id="product-quick" class="product-info">*/
/* 		<div class="product-view row">*/
/* 			<div class="left-content-product ">*/
/* 				{#======Img Gallery Block=====#}*/
/* 				<div class="content-product-left class-honizol  col-sm-5">*/
/* 					<div class="large-image ">*/
/* 						<img class="product-image-zoom" src="{{popup}}" data-zoom-image="{{popup}}" title="{{ heading_title }}" alt="{{ heading_title }}" />*/
/* 					</div>*/
/* 					*/
/* 					{#==== Gallery - Bottom Thumbnails ==== #}*/
/* 					<div id="thumb-slider" class="full_slider contentslider" data-rtl="{{direction}}" data-autoplay="no"  data-pagination="no" data-delay="4" data-speed="0.6" data-margin="10"  data-items_column0="4" data-items_column1="3" data-items_column2="4" data-items_column3="3" data-items_column4="2" data-arrows="yes" data-lazyload="yes" data-loop="no" data-hoverpause="yes">*/
/* 						{% for key,image in images %}*/
/* 							<div class="image-additional">*/
/* 							<a data-index="{{key}}" class="img thumbnail " data-image="{{image.popup}}" title="{{ heading_title }}">*/
/* 								<img src="{{ image.thumb }}" title="{{ heading_title }}" alt="{{ heading_title }}" />*/
/* 							</a>*/
/* 							</div>*/
/* 						{% endfor %}*/
/* 					</div>*/
/* 					*/
/* 				</div>*/
/* 				*/
/* 				{#======Product info Block=====#}*/
/* 				<div class="content-product-right col-sm-7">*/
/* 					*/
/* 					<div class="row">*/
/* 						<div class="col-sm-7 col-xs-12">*/
/* 							<div class="title-product">*/
/* 								<h1>{{ heading_title }} </h1>*/
/* 							</div>*/
/* 							{% if review_status %}*/
/* 							{#======== Review - Rating ========== #}*/
/* 							<div class="box-review">*/
/* 								<div class="rating">*/
/* 									<div class="rating-box">*/
/* 									{% for i in 1..5 %}*/
/* 										{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 									{% endfor %}*/
/* 									</div>*/
/* 								</div>*/
/* 								<a class="reviews_button" href="#" >{{ reviews }}</a> */
/* 								{% if soconfig.get_settings('product_order') %}*/
/* 									<span class="order-num">{{orders}}</span>*/
/* 								{% endif %}*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							*/
/* 							*/
/* 							{# Product Price ------- #}*/
/* 							{% if price  %} */
/* 							<div class="product_page_price price" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">*/
/* 								{% if not special %} */
/* 									<span class="price-new"><span itemprop="price" id="price-old">{{ price }} </span></span>*/
/* 								{% else %}   */
/* 									<span class="price-new"><span itemprop="price" id="price-special">{{ special }} </span></span> <span class="price-old" id="price-old">{{ price }} </span>*/
/* 								{% endif %} */
/* 								*/
/* 								{% if tax %} */
/* 									<div class="price-tax"><span>{{ text_tax }} </span> {{ tax }} </div>*/
/* 								{% endif %} */
/* 								{#=======Discount Label======= #}*/
/* 								{% if soconfig.get_settings('discount_status')  %} */
/* 									{% if price  and  special  %} */
/* 										<span class="label-product label-sale"> {{ discount }}</span>*/
/* 									{% endif %} */
/* 								{% endif %} */
/* */
/* 								*/
/* 							</div>*/
/* 							{% endif %} */
/* 						 	{% if discounts %} */
/* 								<ul class="list-unstyled text-success">*/
/* 								{% for discount in discounts %} */
/* 									<li><strong>{{ discount.quantity }} {{ text_discount }} {{ discount.price }}</strong> </li>*/
/* 								{% endfor %}*/
/* 								</ul>*/
/* 							{% endif %} 	*/
/* 						</div>*/
/* 						<div class="col-sm-5 col-xs-12">*/
/* 							<div class="product-box-desc">*/
/* 								{% if manufacturer %} */
/* 										<div class="brand"><span>{{ text_manufacturer }} </span><a href="{{ manufacturers }} ">{{ manufacturer }} </a></div>*/
/* 								{% endif %} */
/* 								*/
/* 								{% if model %} */
/* 									<div class="model"><span>{{ text_model }} </span> {{ model }} </div>*/
/* 								{% endif %} */
/* 								*/
/* 								{% if points %} */
/* 									<div class="reward hidden"><span>{{ text_points }} </span> {{ points }} </div>*/
/* 								{% endif %} */
/* 								<div class="stock"><span>{{ text_stock }} </span> <i class="fa fa-check-square-o"></i> {{ stock }} </div>	*/
/* 								<div class="inner-box-viewed ">*/
/* 									<span>{{ text_viewed }}</span> <i class="fa fa-eye" ></i> {{ viewed }}*/
/* 								</div>									*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					*/
/* 					<div class="short_description form-group">*/
/* 						<h3>{{ objlang.get('text_overview') }}</h3>*/
/* 						<div class="form-group">{{ description_short }}</div>*/
/* 					</div>*/
/* 					*/
/* 					{% if options %} */
/* 					<div id="product">	*/
/* 						<h3>{{ text_option }} </h3>*/
/* 						{% for option in options %}*/
/* 							{% if option.type == 'select' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 								<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 								<select name="option[{{ option.product_option_id }}]" id="input-option{{ option.product_option_id }}" class="form-control">*/
/* 									<option value="">{{ text_select }}</option>*/
/* 								{% for option_value in option.product_option_value %}*/
/* 									<option value="{{ option_value.product_option_value_id }}">{{ option_value.name }}*/
/* 									{% if option_value.price %}*/
/* 										({{ option_value.price_prefix }}{{ option_value.price }})*/
/* 									{% endif %}*/
/* 									</option>*/
/* 								{% endfor %}*/
/* 							  </select>*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							*/
/* 							{% if option.type == 'radio' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  	<label class="control-label">{{ option.name }}</label>*/
/* 								<div id="input-option{{ option.product_option_id }}">*/
/* 									{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 									{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 									{% for option_value in option.product_option_value %} */
/* 									{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 									{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 									*/
/* 										<div class="radio {{ radio_image ~ radio_type }}">*/
/* 											<label>*/
/* 												<input type="radio" name="option[{{ option.product_option_id }}]" value="{{ option_value.product_option_value_id }}" />*/
/* 												<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 													{% if option_value.image %} */
/* 														<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 													{% endif %} */
/* 													<span class="option-name">{{ option_value.name }} </span>*/
/* 													{% if option_value.price  and  radio_style  != '1' %} ({{ option_value.price_prefix }} {{ option_value.price }} ){% endif %} */
/* 												  */
/* 												</span>*/
/* 											</label>*/
/* 										</div>*/
/* 									{% endfor %}	*/
/* 									 */
/* 									{% if radio_style %} */
/* 									<script type="text/javascript">*/
/* 										 $(document).ready(function(){*/
/* 											  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 												   $('#input-option{{ option.product_option_id }}  span').removeClass("active");*/
/* 												   $(this).toggleClass("active");*/
/* 											  });*/
/* 										 });*/
/* 									</script>*/
/* 									{% endif %} */
/* */
/* 								</div>*/
/* 							</div>*/
/* 							{% endif %}*/
/* */
/* 							{% if option.type == 'checkbox' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  	<label class="control-label">{{ option.name }}</label>*/
/* 							  	<div id="input-option{{ option.product_option_id }}">*/
/* 									{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 									{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 									{% for option_value in option.product_option_value %} */
/* 									{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 									{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 									*/
/* 										<div class="checkbox  {{ radio_image ~ radio_type }}">*/
/* 											<label>*/
/* 										*/
/* 												 <input type="checkbox" name="option[{{ option.product_option_id }}][]" value="{{ option_value.product_option_value_id }}" />*/
/* 												<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 													{% if option_value.image %} */
/* 														<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 													{% endif %} */
/* */
/* 													<span class="option-name">{{ option_value.name }} </span>*/
/* 													{% if option_value.price  and  radio_style  != '1' %} */
/* 														({{ option_value.price_prefix }} {{ option_value.price }} )*/
/* 													{% endif %} */
/* 												  */
/* 												</span>*/
/* 											</label>*/
/* 										</div>*/
/* 									{% endfor %}	*/
/* 									 */
/* 									{% if radio_style %} */
/* 									<script type="text/javascript">*/
/* 										 $(document).ready(function(){*/
/* 											  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 												   $(this).toggleClass("active");*/
/* 											  });*/
/* 										 });*/
/* 									</script>*/
/* 									{% endif %} */
/* */
/* 								</div>*/
/* 							</div>*/
/* 							{% endif %}*/
/* */
/* 							{% if option.type == 'text' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							  <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							{% if option.type == 'textarea' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							  <textarea name="option[{{ option.product_option_id }}]" rows="5" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control">{{ option.value }}</textarea>*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							{% if option.type == 'file' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  <label class="control-label">{{ option.name }}</label>*/
/* 							  <button type="button" id="button-upload{{ option.product_option_id }}" data-loading-text="{{ text_loading }}" class="btn btn-default btn-block"><i class="fa fa-upload"></i> {{ button_upload }}</button>*/
/* 							  <input type="hidden" name="option[{{ option.product_option_id }}]" value="" id="input-option{{ option.product_option_id }}" />*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							{% if option.type == 'date' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							  <div class="input-group date">*/
/* 								<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 								<span class="input-group-btn">*/
/* 								<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/* 								</span></div>*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							{% if option.type == 'datetime' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							  <div class="input-group datetime">*/
/* 								<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 								<span class="input-group-btn">*/
/* 								<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 								</span></div>*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							*/
/* 							{% if option.type == 'time' %}*/
/* 							<div class="form-group{% if option.required %} required {% endif %}">*/
/* 								<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 								<div class="input-group time">*/
/* 								<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 								<span class="input-group-btn">*/
/* 								<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 								</span></div>*/
/* 							</div>*/
/* 							{% endif %}*/
/* 							*/
/* 							*/
/* 						{% endfor %}*/
/* 					</div>*/
/* 					{% endif %}*/
/* 					*/
/* 					{% if recurrings %}*/
/* 					<hr>*/
/* 					<h3>{{ text_payment_recurring }}</h3>*/
/* 					<div class="form-group required">*/
/* 					  <select name="recurring_id" class="form-control">*/
/* 						<option value="">{{ text_select }}</option>*/
/* 						{% for recurring in recurrings %}*/
/* 						<option value="{{ recurring.recurring_id }}">{{ recurring.name }}</option>*/
/* 						{% endfor %}*/
/* 					  </select>*/
/* 					  <div class="help-block" id="recurring-description"></div>*/
/* 					</div>*/
/* 					{% endif %}*/
/* 				  */
/* 					<div class="form-group box-info-product">*/
/* 						<div class="option quantity">*/
/* 							<label class="control-label" for="input-quantity">{{ entry_qty }}</label>*/
/* 							<div class="input-group quantity-control">*/
/* 								  <span class="input-group-addon product_quantity_down fa fa-minus"></span>*/
/* 								  <input class="form-control" type="text" name="quantity" value="{{ minimum }}" />*/
/* 								  <input type="hidden" name="product_id" value="{{ product_id }}" />								  */
/* 								  <span class="input-group-addon product_quantity_up fa fa-plus"></span>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="detail-action">*/
/* 							{# =========button Cart ======#}*/
/* 							<div class="cart">*/
/* 								<input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega">*/
/* 								*/
/* 								<input type="button"  value="{{text_buynow}}" data-loading-text="{{ text_loading }}" id="button-checkout" class="btn btn-checkout " />*/
/* 							</div>*/
/* 							<div class="add-to-links wish_comp">*/
/* 								<a onclick="wishlist.add({{ product_id }});"><i class="fa fa-heart"></i> {{text_addwishlist}}</a>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					*/
/* 					{% if minimum > 1 %}*/
/* 						<div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_minimum }}</div>*/
/* 					{% endif %}*/
/* 					*/
/* 				</div>*/
/* 				*/
/* 			</div>*/
/* 		</div>*/
/* 		*/
/* 	</div>*/
/* </div>*/
/* */
/* <style type="text/css">*/
/* 	body{background:none;}*/
/* 	#wrapper{box-shadow:none;background:#fff;}*/
/* 	#wrapper > *:not(.product-detail){display: none;}*/
/* 	#wrapper .product-view{margin:0;}*/
/* </style>*/
/* */
/* */
/* <script type="text/javascript">*/
/* 	$(document).ready(function() {*/
/* 		$('.product-options li').click(function(){*/
/* 			$(this).addClass(function() {*/
/* 				if($(this).hasClass("active")) return "";*/
/* 				return "active";*/
/* 			});*/
/* 			*/
/* 			$(this).siblings("li").removeClass("active");*/
/* 			$('.product-options .selected-option').html('<span class="label label-success">'+ $(this).find('img').data('original-title') +'</span>');*/
/* 		})*/
/* 		*/
/* 	});*/
/* 			*/
/* </script>*/
/* */
/* <script type="text/javascript"><!--*/
/* $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/getRecurringDescription',*/
/* 		type: 'post',*/
/* 		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#recurring-description').html('');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert, .text-danger').remove();*/
/* 			*/
/* 			if (json['success']) {*/
/* 				$('#recurring-description').html(json['success']);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script> */
/* */
/* */
/* <script type="text/javascript"><!--*/
/* $('#button-cart').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=extension/soconfig/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product-quick input[type=\'text\'], #product-quick input[type=\'hidden\'], #product-quick input[type=\'radio\']:checked, #product-quick input[type=\'checkbox\']:checked, #product-quick select, #product-quick textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-cart').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-cart').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			*/
/* 			$('.text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/* 						*/
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* 				*/
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* 				*/
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* 			*/
/* 			if (json['success']) {*/
/* 				parent.$('#previewModal').modal('show'); */
/* 				parent.$('#previewModal .modal-body').load('index.php?route=extension/soconfig/cart/info&product_id='+ {{ product_id }});*/
/* 				parent.$('#cart  .total-shopping-cart ').html(json['total'] );*/
/* 				parent.$('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/* 				parent.$('.text-danger').remove();*/
/* 				parent.$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');*/
/* 				parent.$.magnificPopup.close();*/
/* 			}*/
/* 			*/
/* 		*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* */
/* */
/* $('#button-checkout').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product-quick input[type=\'text\'], #product-quick input[type=\'hidden\'], #product-quick input[type=\'radio\']:checked, #product-quick input[type=\'checkbox\']:checked, #product-quick select, #product-quick textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-checkout').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-checkout').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			parent.$('.alert').remove();*/
/* 			$('.text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* */
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/* 						*/
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* 				*/
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* 				*/
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				parent.$('.text-danger').remove();*/
/* 				parent.$('#cart  .total-shopping-cart ').html(json['total'] );*/
/* 				parent.window.location.href = "index.php?route=checkout/checkout";*/
/* 			}*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* */
/* var wishlist = {*/
/* 	'add': function(product_id) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=extension/soconfig/wishlist/add',*/
/* 			type: 'post',*/
/* 			data: 'product_id=' + product_id,*/
/* 			dataType: 'json',*/
/* 			*/
/* 			success: function(json) {*/
/*                 parent.$('.alert').remove();*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 }*/
/*                 if (json['success']) {*/
/*                     parent.$('#wrapper').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="fa fa-close close" data-dismiss="alert"></button></div>');*/
/*                 }*/
/*                 if (json['info']) {*/
/*                     parent.$('#wrapper').before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="fa fa-close close"></button></div>');*/
/*                 }*/
/*                 parent.$('#wishlist-total').html(json['total']);*/
/* 				parent.$('#wishlist-total').attr('title', json['total']);*/
/*                 timer = setTimeout(function() {*/
/*                     parent.$('.alert').addClass('fadeOut');*/
/*                 }, 4000);*/
/*             },*/
/* 		});*/
/* 	}*/
/* }*/
/* */
/* var compare = {*/
/* 	'add': function(product_id) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=extension/soconfig/compare/add',*/
/* 			type: 'post',*/
/* 			data: 'product_id=' + product_id,*/
/* 			dataType: 'json',*/
/* 			*/
/* 			success: function(json) {*/
/*                 parent.$('.alert').remove();*/
/*                 if (json['info']) {*/
/*                    parent. $('#wrapper').before('<div class="alert alert-info"><i class="fa fa-info-circle"></i>  ' + json['info'] + '<button type="button" class="fa fa-close close"></button></div>');*/
/*                 }*/
/*                 if (json['success']) {*/
/*                     parent.$('#wrapper').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i>' + json['success'] + '<button type="button" class="fa fa-close close"></button></div>');*/
/*                     if (json['warning']) {*/
/*                         parent.$('.alert').append('<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> ' + json['warning'] + '<button type="button" class="fa fa-close close"></button></div>');*/
/*                     }*/
/*                     parent.$('#compare-total').attr('data-original-title', json['total']);*/
/*                     parent.$('#compare-total').html('<span>' + json['total'] + '</span>');*/
/*                 }*/
/*                 timer = setTimeout(function() {*/
/*                     parent.$('.alert').addClass('fadeOut');*/
/*                 }, 4000);*/
/*             },*/
/* 		});*/
/* 	}*/
/* 	*/
/* }*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* var zoomCollection = '.large-image img';*/
/* 		$( zoomCollection ).elevateZoom({*/
/* 			zoomType        :"none",*/
/* 			lensSize    : '200',*/
/* 			easing:false,*/
/* 			gallery:'thumb-slider',*/
/* 			cursor: 'pointer',*/
/* 			galleryActiveClass: "active",*/
/* 		});*/
/* 		*/
/* 		*/
/* 		$("#thumb-slider .image-additional").each(function() {*/
/* 			$(this).find("[data-index='0']").addClass('active');*/
/* 		});*/
/* $('.product-options li.radio').click(function(){*/
/* 	$(this).addClass(function() {*/
/* 		if($(this).hasClass("active")) return "";*/
/* 		return "active";*/
/* 	});*/
/* 	*/
/* 	$(this).siblings("li").removeClass("active");*/
/* 	$(this).parent().find('.selected-option').html('<span class="label label-success">'+ $(this).find('img').data('original-title') +'</span>');*/
/* })*/
/* */
/* $('.date').datetimepicker({*/
/* 	pickTime: false*/
/* });*/
/* */
/* $('.datetime').datetimepicker({*/
/* 	pickDate: true,*/
/* 	pickTime: true*/
/* });*/
/* */
/* $('.time').datetimepicker({*/
/* 	pickDate: false*/
/* });*/
/* */
/* $('button[id^=\'button-upload\']').on('click', function() {*/
/* 	var node = this;*/
/* 	*/
/* 	$('#form-upload').remove();*/
/* 	*/
/* 	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* 	*/
/* 	$('#form-upload input[name=\'file\']').trigger('click');*/
/*     if (typeof timer != 'undefined') {*/
/*         clearInterval(timer);*/
/*     }*/
/* 	timer = setInterval(function() {*/
/* 		if ($('#form-upload input[name=\'file\']').val() != '') {*/
/* 			clearInterval(timer);*/
/* 			*/
/* 			$.ajax({*/
/* 				url: 'index.php?route=tool/upload',*/
/* 				type: 'post',*/
/* 				dataType: 'json',*/
/* 				data: new FormData($('#form-upload')[0]),*/
/* 				cache: false,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				beforeSend: function() {*/
/* 					$(node).button('loading');*/
/* 				},*/
/* 				complete: function() {*/
/* 					$(node).button('reset');*/
/* 				},*/
/* 				success: function(json) {*/
/* 					$('.text-danger').remove();*/
/* 					*/
/* 					if (json['error']) {*/
/* 						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');*/
/* 					}*/
/* 					*/
/* 					if (json['success']) {*/
/* 						alert(json['success']);*/
/* 						*/
/* 						$(node).parent().find('input').attr('value', json['code']);*/
/* 					}*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 		}*/
/* 	}, 500);*/
/* });*/
/* //--></script> */
/* */
/* <script type="text/javascript">*/
/* var ajax_price = function() {*/
/* 	$.ajax({*/
/* 		type: 'POST',*/
/* 		url: 'index.php?route=extension/soconfig/liveprice/index',*/
/* 		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),*/
/* 		dataType: 'json',*/
/* 			success: function(json) {*/
/* 			if (json.success) {*/
/* 				change_price('#price-special', json.new_price.special);*/
/* 				change_price('#price-tax', json.new_price.tax);*/
/* 				change_price('#price-old', json.new_price.price);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* }*/
/* */
/* var change_price = function(id, new_price) {*/
/* 	$(id).html(new_price);*/
/* }*/
/* $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\'], .product-info input[type=\'checkbox\'], .product-info select, .product-info textarea, .product-info input[name=\'quantity\']').on('change', function() {*/
/* 	ajax_price();*/
/* });*/
/* </script>*/
/* */
/* */
