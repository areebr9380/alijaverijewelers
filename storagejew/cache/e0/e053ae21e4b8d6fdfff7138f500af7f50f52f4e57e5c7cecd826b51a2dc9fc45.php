<?php

/* so-claue/template/common/cart.twig */
class __TwigTemplate_0404bf5e480d02f6af681a7b2ee97cbce9615651960385200821089002183bd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"cart\" class=\"btn-shopping-cart\">
  
  <a data-loading-text=\"";
        // line 3
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo " \" class=\"btn-group top_cart dropdown-toggle\" data-toggle=\"dropdown\">
    <div class=\"shopcart\">
      <span class=\"icon-c\">
\t 
        ";
        // line 7
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-1")) {
            echo " <svg width=\"20\" height=\"20\" class=\"icon-shopbag\"><use xlink:href=\"#icon-shopbag\"></use></svg>
\t\t\t";
        } elseif (($this->getAttribute(        // line 8
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-2")) {
            echo " <svg width=\"20\" height=\"20\" class=\"icon-shopping-basket\"><use xlink:href=\"#icon-shopping-basket\"></use></svg>
\t\t\t";
        } elseif (($this->getAttribute(        // line 9
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-3")) {
            echo " <svg width=\"20\" height=\"20\" class=\"icon-shopping-handbag\"><use xlink:href=\"#icon-shopping-handbag\"></use></svg>
\t\t\t";
        } elseif (($this->getAttribute(        // line 10
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-4")) {
            echo " <svg width=\"20\" height=\"20\" class=\"icon-shopping-briefcase\"><use xlink:href=\"#icon-shopping-briefcase\"></use></svg>
\t\t\t";
        } elseif (($this->getAttribute(        // line 11
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-5")) {
            echo " <svg width=\"18\" height=\"18\" class=\"icon-shopping-bag2\"><use xlink:href=\"#icon-shopping-bag2\"></use></svg>
\t\t\t";
        } elseif (($this->getAttribute(        // line 12
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "iconcart"), "method") == "cart-6")) {
            echo " <svg width=\"20\" height=\"20\" class=\"icon-shopping-basket2\"><use xlink:href=\"#icon-shopping-basket2\"></use></svg>
\t\t";
        }
        // line 14
        echo "      </span>
      <div class=\"shopcart-inner\">
        <p class=\"text-shopping-cart\">
         ";
        // line 17
        echo (isset($context["text_shop_cart"]) ? $context["text_shop_cart"] : null);
        echo "
        </p>
   
        <span class=\"total-shopping-cart cart-total-full\">
           ";
        // line 21
        echo (isset($context["text_items"]) ? $context["text_items"] : null);
        echo "
        </span>
      </div>
    </div>
  </a>
  
  <ul class=\"dropdown-menu pull-right shoppingcart-box\">
    ";
        // line 28
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 29
            echo "    <li class=\"content-item\">
      <table class=\"table table-striped\" style=\"margin-bottom:10px;\">
        ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 32
                echo "        <tr>
          <td class=\"text-center size-img-cart\">";
                // line 33
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    echo " <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img class=\"img-thumbnail lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\"  /></a> ";
                }
                echo "</td>
          <td class=\"text-left\"><a href=\"";
                // line 34
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a> ";
                if ($this->getAttribute($context["product"], "option", array())) {
                    // line 35
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        echo " <br />
            - <small>";
                        // line 36
                        echo $this->getAttribute($context["option"], "name", array());
                        echo " ";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</small> ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 37
                    echo "            ";
                }
                // line 38
                echo "            ";
                if ($this->getAttribute($context["product"], "recurring", array())) {
                    echo " <br />
            - <small>";
                    // line 39
                    echo (isset($context["text_recurring"]) ? $context["text_recurring"] : null);
                    echo " ";
                    echo $this->getAttribute($context["product"], "recurring", array());
                    echo "</small> ";
                }
                echo "</td>
          <td class=\"text-right\">x ";
                // line 40
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "</td>
          <td class=\"text-right\">";
                // line 41
                echo $this->getAttribute($context["product"], "total", array());
                echo "</td>
          <td class=\"text-center\"><button type=\"button\" onclick=\"cart.remove('";
                // line 42
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i></button></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 46
                echo "        <tr>
          <td class=\"text-center\"></td>
          <td class=\"text-left\">";
                // line 48
                echo $this->getAttribute($context["voucher"], "description", array());
                echo "</td>
          <td class=\"text-right\">x&nbsp;1</td>
          <td class=\"text-right\">";
                // line 50
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</td>
          <td class=\"text-center text-danger\"><button type=\"button\" onclick=\"voucher.remove('";
                // line 51
                echo $this->getAttribute($context["voucher"], "key", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-times\"></i></button></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "      </table>
    </li>
  
\t<li>
\t\t<div class=\"checkout clearfix\">
\t\t<a href=\"";
            // line 59
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "\" class=\"btn btn-view-cart inverse\"> ";
            echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
            echo "</a>
\t\t<a href=\"";
            // line 60
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\" class=\"btn btn-checkout pull-right\">";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</a>
\t\t</div>
\t</li>
    ";
        } else {
            // line 64
            echo "    <li>
      <p class=\"text-center empty\">";
            // line 65
            echo (isset($context["text_empty_cart"]) ? $context["text_empty_cart"] : null);
            echo "</p>
    </li>
    ";
        }
        // line 68
        echo "  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "so-claue/template/common/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 68,  215 => 65,  212 => 64,  203 => 60,  197 => 59,  190 => 54,  179 => 51,  175 => 50,  170 => 48,  166 => 46,  161 => 45,  150 => 42,  146 => 41,  142 => 40,  134 => 39,  129 => 38,  126 => 37,  117 => 36,  110 => 35,  104 => 34,  90 => 33,  87 => 32,  83 => 31,  79 => 29,  77 => 28,  67 => 21,  60 => 17,  55 => 14,  50 => 12,  46 => 11,  42 => 10,  38 => 9,  34 => 8,  30 => 7,  23 => 3,  19 => 1,);
    }
}
/* <div id="cart" class="btn-shopping-cart">*/
/*   */
/*   <a data-loading-text="{{ text_loading }} " class="btn-group top_cart dropdown-toggle" data-toggle="dropdown">*/
/*     <div class="shopcart">*/
/*       <span class="icon-c">*/
/* 	 */
/*         {% if  soconfig.get_settings('iconcart') =='cart-1' %} <svg width="20" height="20" class="icon-shopbag"><use xlink:href="#icon-shopbag"></use></svg>*/
/* 			{% elseif soconfig.get_settings('iconcart') =='cart-2' %} <svg width="20" height="20" class="icon-shopping-basket"><use xlink:href="#icon-shopping-basket"></use></svg>*/
/* 			{% elseif soconfig.get_settings('iconcart') =='cart-3' %} <svg width="20" height="20" class="icon-shopping-handbag"><use xlink:href="#icon-shopping-handbag"></use></svg>*/
/* 			{% elseif soconfig.get_settings('iconcart') =='cart-4' %} <svg width="20" height="20" class="icon-shopping-briefcase"><use xlink:href="#icon-shopping-briefcase"></use></svg>*/
/* 			{% elseif soconfig.get_settings('iconcart') =='cart-5' %} <svg width="18" height="18" class="icon-shopping-bag2"><use xlink:href="#icon-shopping-bag2"></use></svg>*/
/* 			{% elseif soconfig.get_settings('iconcart') =='cart-6' %} <svg width="20" height="20" class="icon-shopping-basket2"><use xlink:href="#icon-shopping-basket2"></use></svg>*/
/* 		{% endif %}*/
/*       </span>*/
/*       <div class="shopcart-inner">*/
/*         <p class="text-shopping-cart">*/
/*          {{ text_shop_cart}}*/
/*         </p>*/
/*    */
/*         <span class="total-shopping-cart cart-total-full">*/
/*            {{text_items}}*/
/*         </span>*/
/*       </div>*/
/*     </div>*/
/*   </a>*/
/*   */
/*   <ul class="dropdown-menu pull-right shoppingcart-box">*/
/*     {% if products or vouchers %}*/
/*     <li class="content-item">*/
/*       <table class="table table-striped" style="margin-bottom:10px;">*/
/*         {% for product in products %}*/
/*         <tr>*/
/*           <td class="text-center size-img-cart">{% if product.thumb %} <a href="{{ product.href }}"><img class="img-thumbnail lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}"  /></a> {% endif %}</td>*/
/*           <td class="text-left"><a href="{{ product.href }}">{{ product.name }}</a> {% if product.option %}*/
/*             {% for option in product.option %} <br />*/
/*             - <small>{{ option.name }} {{ option.value }}</small> {% endfor %}*/
/*             {% endif %}*/
/*             {% if product.recurring %} <br />*/
/*             - <small>{{ text_recurring }} {{ product.recurring }}</small> {% endif %}</td>*/
/*           <td class="text-right">x {{ product.quantity }}</td>*/
/*           <td class="text-right">{{ product.total }}</td>*/
/*           <td class="text-center"><button type="button" onclick="cart.remove('{{ product.cart_id }}');" title="{{ button_remove }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*         {% for voucher in vouchers %}*/
/*         <tr>*/
/*           <td class="text-center"></td>*/
/*           <td class="text-left">{{ voucher.description }}</td>*/
/*           <td class="text-right">x&nbsp;1</td>*/
/*           <td class="text-right">{{ voucher.amount }}</td>*/
/*           <td class="text-center text-danger"><button type="button" onclick="voucher.remove('{{ voucher.key }}');" title="{{ button_remove }}" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*       </table>*/
/*     </li>*/
/*   */
/* 	<li>*/
/* 		<div class="checkout clearfix">*/
/* 		<a href="{{ cart }}" class="btn btn-view-cart inverse"> {{ text_cart }}</a>*/
/* 		<a href="{{ checkout }}" class="btn btn-checkout pull-right">{{ text_checkout }}</a>*/
/* 		</div>*/
/* 	</li>*/
/*     {% else %}*/
/*     <li>*/
/*       <p class="text-center empty">{{  text_empty_cart  }}</p>*/
/*     </li>*/
/*     {% endif %}*/
/*   </ul>*/
/* </div>*/
/* */
