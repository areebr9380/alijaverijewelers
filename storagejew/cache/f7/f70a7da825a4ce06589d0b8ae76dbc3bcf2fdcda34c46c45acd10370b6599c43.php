<?php

/* so-claue/template/extension/module/so_listing_tabs/default/default_items.twig */
class __TwigTemplate_579af46abfd16316146d6f665035841f4c973af9e8eb9becf15ac604a0fc8406 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["type_show"]) ? $context["type_show"] : null) == "slider")) {
            // line 2
            echo "\t\t<div class=\"ltabs-items-inner owl2-carousel  ltabs-slider \">
";
        } else {
            // line 4
            echo "\t\t<div class=\"ltabs-items-inner ";
            echo ((((isset($context["type_show"]) ? $context["type_show"] : null) == "loadmore")) ? ((((isset($context["class_ltabs"]) ? $context["class_ltabs"] : null) . " ") . (isset($context["effect"]) ? $context["effect"] : null))) : (" "));
            echo "\">
";
        }
        // line 6
        if ( !twig_test_empty((isset($context["child_items"]) ? $context["child_items"] : null))) {
            // line 7
            echo "\t";
            $context["i"] = 0;
            // line 8
            echo "\t";
            $context["k"] = ((array_key_exists("rl_loaded", $context)) ? ((isset($context["rl_loaded"]) ? $context["rl_loaded"] : null)) : (0));
            // line 9
            echo "\t";
            $context["count"] = twig_length_filter($this->env, (isset($context["child_items"]) ? $context["child_items"] : null));
            // line 10
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["child_items"]) ? $context["child_items"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 11
                echo "\t\t\t";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 12
                echo "\t\t\t";
                $context["k"] = ((isset($context["k"]) ? $context["k"] : null) + 1);
                // line 13
                echo "\t\t\t
\t\t\t";
                // line 14
                if ((((isset($context["type_show"]) ? $context["type_show"] : null) == "slider") && ((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 1) || ((isset($context["nb_rows"]) ? $context["nb_rows"] : null) == 1)))) {
                    // line 15
                    echo "\t\t\t\t<div class=\"ltabs-item \">
\t\t\t";
                }
                // line 17
                echo "\t\t\t";
                if (((isset($context["type_show"]) ? $context["type_show"] : null) == "loadmore")) {
                    // line 18
                    echo "\t\t\t\t<div class=\"ltabs-item new-ltabs-item\" >
\t\t\t";
                }
                // line 19
                echo "\t\t\t
\t\t\t<div class=\"item-inner product-layout transition product-grid\">

\t\t\t\t<div class=\"product-item-container\">
\t\t\t\t\t<div class=\"left-block\">\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"product-image-container ";
                // line 24
                if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                    echo " ";
                    echo "second_img";
                    echo " ";
                }
                echo "\t\">
\t\t\t\t\t\t\t<a href=\"";
                // line 25
                echo $this->getAttribute($context["product"], "href", array());
                echo "\" target=\"";
                echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "nameFull", array());
                echo " \"  >
\t\t\t\t\t\t\t\t";
                // line 26
                if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                    // line 27
                    echo "\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"image/catalog/productLoading.svg\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" class=\"img-1 lazyload\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" />
\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"image/catalog/productLoading.svg\" data-src=\"";
                    // line 28
                    echo $this->getAttribute($context["product"], "thumb2", array());
                    echo "\" class=\"img-2 lazyload\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\">
\t\t\t\t\t\t\t\t";
                } else {
                    // line 30
                    echo "\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"image/catalog/productLoading.svg\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" class=\"img-1 lazyload\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" />
\t\t\t\t\t\t\t\t";
                }
                // line 31
                echo "\t
\t\t\t\t\t\t\t</a>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"box-label\">
\t\t\t\t\t\t\t";
                // line 35
                if (($this->getAttribute($context["product"], "special", array()) && (isset($context["display_sale"]) ? $context["display_sale"] : null))) {
                    echo " 
\t\t\t\t\t\t\t\t<span class=\"label-product label-sale\">";
                    // line 36
                    echo " ";
                    echo $this->getAttribute($context["product"], "discount", array());
                    echo "  </span>
\t\t\t\t\t\t\t";
                }
                // line 37
                echo " 

\t\t\t\t\t\t\t";
                // line 39
                if (($this->getAttribute($context["product"], "productNew", array()) && (isset($context["display_new"]) ? $context["display_new"] : null))) {
                    echo " 
\t\t\t\t\t\t\t\t<span class=\"label-product label-new\">";
                    // line 40
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_new"), "method");
                    echo " </span>
\t\t\t\t\t\t\t";
                }
                // line 41
                echo " \t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                // line 43
                if ((isset($context["display_add_to_cart"]) ? $context["display_add_to_cart"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<button type=\"button\" class=\"addToCart btn-button\" title=\"";
                    // line 44
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo "\" onclick=\"cart.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><span>";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo " </span></button>
\t\t\t\t\t\t";
                }
                // line 45
                echo " 
\t\t\t\t\t\t<div class=\"button-group cartinfo--left\">
\t\t\t\t\t\t\t<div class=\"so-quickview\">
\t\t\t\t\t\t\t\t<a class=\"hidden\" data-product='";
                // line 48
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "' href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                // line 50
                if ((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<button type=\"button\" class=\"wishlist btn-button\" title=\"";
                    // line 51
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_wishlist"), "method");
                    echo "\" onclick=\"wishlist.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"pe-7s-like\"></i></button>
\t\t\t\t\t\t\t";
                }
                // line 52
                echo " \t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 53
                if ((isset($context["display_compare"]) ? $context["display_compare"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<button type=\"button\" class=\"compare btn-button\" title=\"";
                    // line 54
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_compare"), "method");
                    echo "\" onclick=\"compare.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"pe-7s-graph3\"></i></button>
\t\t\t\t\t\t\t";
                }
                // line 55
                echo " 

\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"right-block\">
\t\t\t\t\t\t

\t\t\t\t\t\t";
                // line 66
                if ((isset($context["display_title"]) ? $context["display_title"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t<a href=\"";
                    // line 68
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "nameFull", array());
                    echo " \"  >
\t\t\t\t\t\t\t\t\t";
                    // line 69
                    echo $this->getAttribute($context["product"], "name", array());
                    echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t";
                }
                // line 73
                echo "\t\t\t\t\t\t";
                if ((isset($context["display_rating"]) ? $context["display_rating"] : null)) {
                    // line 74
                    echo "\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "rating", array())) {
                        // line 75
                        echo "\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t";
                        // line 76
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(1, 5));
                        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
                            // line 77
                            echo "\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t";
                            // line 78
                            if (($this->getAttribute($context["product"], "rating", array()) < $context["k"])) {
                                echo " 
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t \t";
                            } else {
                                // line 80
                                echo "   
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 82
                            echo " 
\t\t\t\t\t\t\t\t\t\t";
                            // line 83
                            $context["k"] = ($context["k"] + 1);
                            // line 84
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                    } else {
                        // line 86
                        echo "  
\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t";
                        // line 88
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(1, 5));
                        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                            // line 89
                            echo "\t\t\t\t\t\t\t\t\t";
                            $context["j"] = ($context["j"] + 1);
                            // line 90
                            echo "\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 91
                        echo " \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                    }
                    // line 93
                    echo "\t
\t\t\t\t\t\t";
                }
                // line 94
                echo "\t
\t\t\t\t\t\t";
                // line 95
                if ((isset($context["display_price"]) ? $context["display_price"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<div  class=\"price\">
\t\t\t\t\t\t\t\t";
                    // line 97
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        echo " 
\t\t\t\t\t\t\t\t\t<span class=\"price-new\">
\t\t\t\t\t\t\t\t\t\t";
                        // line 99
                        echo $this->getAttribute($context["product"], "price", array());
                        echo " 
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 101
                        echo "   
\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        // line 102
                        echo $this->getAttribute($context["product"], "special", array());
                        echo " </span>&nbsp;&nbsp;
\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 103
                        echo $this->getAttribute($context["product"], "price", array());
                        echo " </span>&nbsp;
\t\t\t\t\t\t\t\t";
                    }
                    // line 104
                    echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 107
                echo " \t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 109
                if ((isset($context["display_description"]) ? $context["display_description"] : null)) {
                    echo " 
\t\t\t\t\t\t\t<div class=\"item-des\">
\t\t\t\t\t\t\t\t";
                    // line 111
                    echo $this->getAttribute($context["product"], "description", array());
                    echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 113
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>

\t\t\t\t\t
\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t";
                // line 124
                if ((((isset($context["type_show"]) ? $context["type_show"] : null) == "slider") && ((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 0) || ((isset($context["i"]) ? $context["i"] : null) == (isset($context["count"]) ? $context["count"] : null))))) {
                    // line 125
                    echo "\t\t\t</div>
\t\t\t";
                }
                // line 127
                echo "\t\t\t
\t\t\t";
                // line 128
                if (((isset($context["type_show"]) ? $context["type_show"] : null) == "loadmore")) {
                    // line 129
                    echo "\t\t\t</div>
\t\t\t";
                }
                // line 131
                echo "
\t\t\t";
                // line 132
                if (((isset($context["type_show"]) ? $context["type_show"] : null) == "loadmore")) {
                    // line 133
                    echo "\t\t\t\t";
                    $context["clear"] = "clr1";
                    // line 134
                    echo "\t\t\t\t";
                    if ((((isset($context["k"]) ? $context["k"] : null) % 2) == 0)) {
                        echo " ";
                        $context["clear"] = ((isset($context["clear"]) ? $context["clear"] : null) . " clr2");
                        echo " ";
                    }
                    // line 135
                    echo "\t\t\t\t";
                    if ((((isset($context["k"]) ? $context["k"] : null) % 3) == 0)) {
                        echo " ";
                        $context["clear"] = ((isset($context["clear"]) ? $context["clear"] : null) . " clr3");
                        echo " ";
                    }
                    // line 136
                    echo "\t\t\t\t";
                    if ((((isset($context["k"]) ? $context["k"] : null) % 4) == 0)) {
                        echo " ";
                        $context["clear"] = ((isset($context["clear"]) ? $context["clear"] : null) . " clr4");
                        echo " ";
                    }
                    // line 137
                    echo "\t\t\t\t";
                    if ((((isset($context["k"]) ? $context["k"] : null) % 5) == 0)) {
                        echo " ";
                        $context["clear"] = ((isset($context["clear"]) ? $context["clear"] : null) . " clr5");
                        echo " ";
                    }
                    // line 138
                    echo "\t\t\t\t";
                    if ((((isset($context["k"]) ? $context["k"] : null) % 6) == 0)) {
                        echo " ";
                        $context["clear"] = ((isset($context["clear"]) ? $context["clear"] : null) . " clr6");
                        echo " ";
                    }
                    // line 139
                    echo "\t\t\t\t<div class=\"";
                    echo (isset($context["clear"]) ? $context["clear"] : null);
                    echo "\"></div>
\t\t\t";
                }
                // line 141
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 142
            echo "\t";
        }
        // line 143
        echo "</div>

";
        // line 145
        if (((isset($context["type_show"]) ? $context["type_show"] : null) == "slider")) {
            // line 146
            echo "<script type=\"text/javascript\">
\tjQuery(document).ready(function(\$){
\t\tvar \$tag_id = \$('#";
            // line 148
            echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
            echo "'), 
\t\tparent_active = \t\$('.items-category-";
            // line 149
            echo (isset($context["tab_id"]) ? $context["tab_id"] : null);
            echo "', \$tag_id),
\t\ttotal_product = parent_active.data('total'),
\t\ttab_active = \$('.ltabs-items-inner',parent_active),
\t\tnb_column0 = ";
            // line 152
            echo (isset($context["nb_column0"]) ? $context["nb_column0"] : null);
            echo ",
\t\tnb_column1 = ";
            // line 153
            echo (isset($context["nb_column1"]) ? $context["nb_column1"] : null);
            echo ",
\t\tnb_column2 = ";
            // line 154
            echo (isset($context["nb_column2"]) ? $context["nb_column2"] : null);
            echo ",
\t\tnb_column3 = ";
            // line 155
            echo (isset($context["nb_column3"]) ? $context["nb_column3"] : null);
            echo ",
\t\tnb_column4 = ";
            // line 156
            echo (isset($context["nb_column4"]) ? $context["nb_column4"] : null);
            echo ";
\t\ttab_active.owlCarousel2({
\t\t\trtl: ";
            // line 158
            echo (isset($context["direction"]) ? $context["direction"] : null);
            echo ",
\t\t\tnav: ";
            // line 159
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ",
\t\t\tdots: false,\t
\t\t\tmargin: 30,
\t\t\tloop:  ";
            // line 162
            echo (isset($context["display_loop"]) ? $context["display_loop"] : null);
            echo ",
\t\t\tautoplay: ";
            // line 163
            echo (isset($context["autoplay"]) ? $context["autoplay"] : null);
            echo ",
\t\t\tautoplayHoverPause: ";
            // line 164
            echo (isset($context["pausehover"]) ? $context["pausehover"] : null);
            echo ",
\t\t\tautoplayTimeout: ";
            // line 165
            echo (isset($context["autoplayTimeout"]) ? $context["autoplayTimeout"] : null);
            echo ",
\t\t\tautoplaySpeed: ";
            // line 166
            echo (isset($context["autoplaySpeed"]) ? $context["autoplaySpeed"] : null);
            echo ",
\t\t\tmouseDrag: ";
            // line 167
            echo (isset($context["mousedrag"]) ? $context["mousedrag"] : null);
            echo ",
\t\t\ttouchDrag: ";
            // line 168
            echo (isset($context["touchdrag"]) ? $context["touchdrag"] : null);
            echo ",
\t\t\tnavRewind: true,
\t\t\tnavText: [ '', '' ],
\t\t\tresponsive: {
\t\t\t\t0: {
\t\t\t\t\titems: nb_column4,
\t\t\t\t\tnav: total_product <= nb_column0 ? false : ((";
            // line 174
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ") ? true: false),
\t\t\t\t},
\t\t\t\t480: {
\t\t\t\t\titems: nb_column3,
\t\t\t\t\tnav: total_product <= nb_column0 ? false : ((";
            // line 178
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ") ? true: false),
\t\t\t\t},
\t\t\t\t768: {
\t\t\t\t\titems: nb_column2,
\t\t\t\t\tnav: total_product <= nb_column0 ? false : ((";
            // line 182
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ") ? true: false),
\t\t\t\t},
\t\t\t\t992: {
\t\t\t\t\titems: nb_column1,
\t\t\t\t\tnav: total_product <= nb_column0 ? false : ((";
            // line 186
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ") ? true: false),
\t\t\t\t},
\t\t\t\t1200: {
\t\t\t\t\titems: nb_column0,
\t\t\t\t\t
\t\t\t\t\tnav: total_product <= nb_column0 ? false : ((";
            // line 191
            echo (isset($context["display_nav"]) ? $context["display_nav"] : null);
            echo ") ? true: false),
\t\t\t\t}
\t\t\t}
\t\t});
\t});
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "so-claue/template/extension/module/so_listing_tabs/default/default_items.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  552 => 191,  544 => 186,  537 => 182,  530 => 178,  523 => 174,  514 => 168,  510 => 167,  506 => 166,  502 => 165,  498 => 164,  494 => 163,  490 => 162,  484 => 159,  480 => 158,  475 => 156,  471 => 155,  467 => 154,  463 => 153,  459 => 152,  453 => 149,  449 => 148,  445 => 146,  443 => 145,  439 => 143,  436 => 142,  430 => 141,  424 => 139,  417 => 138,  410 => 137,  403 => 136,  396 => 135,  389 => 134,  386 => 133,  384 => 132,  381 => 131,  377 => 129,  375 => 128,  372 => 127,  368 => 125,  366 => 124,  353 => 113,  347 => 111,  342 => 109,  338 => 107,  332 => 104,  327 => 103,  323 => 102,  320 => 101,  314 => 99,  309 => 97,  304 => 95,  301 => 94,  297 => 93,  292 => 91,  285 => 90,  282 => 89,  278 => 88,  274 => 86,  264 => 84,  262 => 83,  259 => 82,  254 => 80,  248 => 78,  245 => 77,  241 => 76,  238 => 75,  235 => 74,  232 => 73,  225 => 69,  217 => 68,  212 => 66,  199 => 55,  192 => 54,  188 => 53,  185 => 52,  178 => 51,  174 => 50,  167 => 48,  162 => 45,  153 => 44,  149 => 43,  145 => 41,  140 => 40,  136 => 39,  132 => 37,  126 => 36,  122 => 35,  116 => 31,  108 => 30,  101 => 28,  94 => 27,  92 => 26,  84 => 25,  76 => 24,  69 => 19,  65 => 18,  62 => 17,  58 => 15,  56 => 14,  53 => 13,  50 => 12,  47 => 11,  42 => 10,  39 => 9,  36 => 8,  33 => 7,  31 => 6,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if type_show == 'slider' %}*/
/* 		<div class="ltabs-items-inner owl2-carousel  ltabs-slider ">*/
/* {% else %}*/
/* 		<div class="ltabs-items-inner {{ type_show == 'loadmore' ? class_ltabs ~ ' '~ effect : ' ' }}">*/
/* {% endif %}*/
/* {% if child_items is not empty %}*/
/* 	{% set i = 0 %}*/
/* 	{% set k = rl_loaded is defined ? rl_loaded : 0 %}*/
/* 	{% set count = child_items|length %}*/
/* 		{% for product in child_items %}*/
/* 			{% set i = i + 1 %}*/
/* 			{% set k = k + 1 %}*/
/* 			*/
/* 			{% if type_show == 'slider' and (i % nb_rows == 1 or nb_rows == 1) %}*/
/* 				<div class="ltabs-item ">*/
/* 			{% endif %}*/
/* 			{% if type_show == 'loadmore' %}*/
/* 				<div class="ltabs-item new-ltabs-item" >*/
/* 			{% endif %}			*/
/* 			<div class="item-inner product-layout transition product-grid">*/
/* */
/* 				<div class="product-item-container">*/
/* 					<div class="left-block">									*/
/* 						<div class="product-image-container {% if product_image_num  == 2 %} {{ 'second_img' }} {% endif %}	">*/
/* 							<a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.nameFull }} "  >*/
/* 								{% if product_image_num ==2 %}*/
/* 									<img data-sizes="auto" src="image/catalog/productLoading.svg" data-src="{{ product.thumb }}" class="img-1 lazyload" alt="{{ product.name }}" />*/
/* 									<img data-sizes="auto" src="image/catalog/productLoading.svg" data-src="{{ product.thumb2 }}" class="img-2 lazyload" alt="{{ product.name }}">*/
/* 								{% else %}*/
/* 									<img data-sizes="auto" src="image/catalog/productLoading.svg" data-src="{{ product.thumb }}" class="img-1 lazyload" alt="{{ product.name }}" />*/
/* 								{% endif %}	*/
/* 							</a>						*/
/* 						</div>*/
/* 						<div class="box-label">*/
/* 							{% if product.special  and  display_sale  %} */
/* 								<span class="label-product label-sale">{# {{ objlang.get('text_sale') }} #} {{ product.discount }}  </span>*/
/* 							{% endif %} */
/* */
/* 							{% if product.productNew  and  display_new  %} */
/* 								<span class="label-product label-new">{{ objlang.get('text_new') }} </span>*/
/* 							{% endif %} 											*/
/* 						</div>*/
/* 						{% if display_add_to_cart  %} */
/* 							<button type="button" class="addToCart btn-button" title="{{ objlang.get('button_cart') }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ objlang.get('button_cart') }} </span></button>*/
/* 						{% endif %} */
/* 						<div class="button-group cartinfo--left">*/
/* 							<div class="so-quickview">*/
/* 								<a class="hidden" data-product='{{ product.product_id }}' href="{{ product.href }}"></a>*/
/* 							</div>*/
/* 							{% if display_wishlist  %} */
/* 							<button type="button" class="wishlist btn-button" title="{{ objlang.get('button_wishlist') }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="pe-7s-like"></i></button>*/
/* 							{% endif %} 							*/
/* 							{% if display_compare %} */
/* 							<button type="button" class="compare btn-button" title="{{ objlang.get('button_compare') }}" onclick="compare.add('{{ product.product_id }}');"><i class="pe-7s-graph3"></i></button>*/
/* 							{% endif %} */
/* */
/* 						</div>*/
/* 				*/
/* 						*/
/* 													*/
/* 					</div>*/
/* */
/* 					<div class="right-block">*/
/* 						*/
/* */
/* 						{% if display_title %} */
/* 							<h4>*/
/* 								<a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.nameFull }} "  >*/
/* 									{{ product.name }} */
/* 								</a>*/
/* 							</h4>*/
/* 						{% endif %}*/
/* 						{% if display_rating %}*/
/* 							{% if product.rating %}*/
/* 								<div class="rating">*/
/* 									{% for k in 1..5 %}*/
/* 										*/
/* 										{% if product.rating < k %} */
/* 											<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 									 	{% else %}   */
/* 											<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>*/
/* 										{% endif %} */
/* 										{% set k = k + 1 %}*/
/* 									{% endfor %} */
/* 								</div>*/
/* 							{% else %}  */
/* 							<div class="rating">*/
/* 								{% for j in 1..5 %}*/
/* 									{% set j = j + 1 %}*/
/* 									<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 								{% endfor %} 													*/
/* 							</div>*/
/* 							{% endif %}	*/
/* 						{% endif %}	*/
/* 						{% if display_price %} */
/* 							<div  class="price">*/
/* 								{% if not product.special %} */
/* 									<span class="price-new">*/
/* 										{{ product.price }} */
/* 									</span>*/
/* 								{% else %}   */
/* 									<span class="price-new">{{ product.special }} </span>&nbsp;&nbsp;*/
/* 									<span class="price-old">{{ product.price }} </span>&nbsp;*/
/* 								{% endif %} */
/* 								*/
/* 							</div>*/
/* 						{% endif %} 						*/
/* 						*/
/* 						{% if display_description %} */
/* 							<div class="item-des">*/
/* 								{{ product.description }} */
/* 							</div>*/
/* 						{% endif %} */
/* 																	*/
/* 						*/
/* 						*/
/* 					</div>*/
/* */
/* 					*/
/* 	*/
/* 				</div>*/
/* 				*/
/* 			</div>*/
/* 			{% if type_show == 'slider' and (i % nb_rows == 0 or i == count) %}*/
/* 			</div>*/
/* 			{% endif %}*/
/* 			*/
/* 			{% if type_show == 'loadmore' %}*/
/* 			</div>*/
/* 			{% endif %}*/
/* */
/* 			{% if type_show == 'loadmore' %}*/
/* 				{% set clear = 'clr1' %}*/
/* 				{% if k % 2 == 0 %} {% set clear = clear ~' clr2' %} {% endif %}*/
/* 				{% if k % 3 == 0 %} {% set clear = clear ~' clr3' %} {% endif %}*/
/* 				{% if k % 4 == 0 %} {% set clear = clear ~' clr4' %} {% endif %}*/
/* 				{% if k % 5 == 0 %} {% set clear = clear ~' clr5' %} {% endif %}*/
/* 				{% if k % 6 == 0 %} {% set clear = clear ~' clr6' %} {% endif %}*/
/* 				<div class="{{ clear }}"></div>*/
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 	{% endif %}*/
/* </div>*/
/* */
/* {% if type_show == 'slider' %}*/
/* <script type="text/javascript">*/
/* 	jQuery(document).ready(function($){*/
/* 		var $tag_id = $('#{{ tag_id }}'), */
/* 		parent_active = 	$('.items-category-{{ tab_id }}', $tag_id),*/
/* 		total_product = parent_active.data('total'),*/
/* 		tab_active = $('.ltabs-items-inner',parent_active),*/
/* 		nb_column0 = {{ nb_column0 }},*/
/* 		nb_column1 = {{ nb_column1 }},*/
/* 		nb_column2 = {{ nb_column2 }},*/
/* 		nb_column3 = {{ nb_column3 }},*/
/* 		nb_column4 = {{ nb_column4 }};*/
/* 		tab_active.owlCarousel2({*/
/* 			rtl: {{ direction }},*/
/* 			nav: {{ display_nav }},*/
/* 			dots: false,	*/
/* 			margin: 30,*/
/* 			loop:  {{ display_loop }},*/
/* 			autoplay: {{ autoplay }},*/
/* 			autoplayHoverPause: {{ pausehover }},*/
/* 			autoplayTimeout: {{ autoplayTimeout }},*/
/* 			autoplaySpeed: {{ autoplaySpeed }},*/
/* 			mouseDrag: {{ mousedrag }},*/
/* 			touchDrag: {{ touchdrag }},*/
/* 			navRewind: true,*/
/* 			navText: [ '', '' ],*/
/* 			responsive: {*/
/* 				0: {*/
/* 					items: nb_column4,*/
/* 					nav: total_product <= nb_column0 ? false : (({{display_nav}}) ? true: false),*/
/* 				},*/
/* 				480: {*/
/* 					items: nb_column3,*/
/* 					nav: total_product <= nb_column0 ? false : (({{display_nav}}) ? true: false),*/
/* 				},*/
/* 				768: {*/
/* 					items: nb_column2,*/
/* 					nav: total_product <= nb_column0 ? false : (({{display_nav}}) ? true: false),*/
/* 				},*/
/* 				992: {*/
/* 					items: nb_column1,*/
/* 					nav: total_product <= nb_column0 ? false : (({{display_nav}}) ? true: false),*/
/* 				},*/
/* 				1200: {*/
/* 					items: nb_column0,*/
/* 					*/
/* 					nav: total_product <= nb_column0 ? false : (({{display_nav}}) ? true: false),*/
/* 				}*/
/* 			}*/
/* 		});*/
/* 	});*/
/* </script>*/
/* {% endif %}*/
